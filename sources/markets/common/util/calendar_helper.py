import datetime

class calendar_helper():

    #region Constructors

    def __init__(self):
        pass
    #endregion

    #region Static Methods

    @staticmethod
    def eval_new_day(curr_date, prev_date):
        return  (curr_date.day != prev_date.day)


    @staticmethod
    def eval_new_hour(curr_hour,prev_hour):
        return (curr_hour.day == prev_hour.day and curr_hour.hour != prev_hour.hour)

    @staticmethod
    def eval_new_minute(curr_minute, prev_minute):
        return ( curr_minute.minute != prev_minute.minute)

    @staticmethod
    def implement_first_hour(date):
        return datetime.datetime(year=date.year, month=date.month, day=date.day, hour=0, minute=0, second=0)

    @staticmethod
    def implement_last_hour(date):
        return datetime.datetime(year=date.year, month=date.month, day=date.day, hour=23, minute=59, second=59)

    @staticmethod
    def implement_first_minute(date):
        return datetime.datetime(year=date.year, month=date.month,day=date.day, hour=date.hour,minute=0, second=0)


    @staticmethod
    def implement_last_minute(date):
        return datetime.datetime(year=date.year, month=date.month,day=date.day, hour=date.hour,minute=59, second=59)

    @staticmethod
    def implement_first_second(date):
        return datetime.datetime(year=date.year, month=date.month,day=date.day, hour=date.hour,minute=date.minute, second=0)


    @staticmethod
    def implement_last_second(date):
        return datetime.datetime(year=date.year, month=date.month,day=date.day, hour=date.hour,minute=date.minute, second=59)

    #endregion