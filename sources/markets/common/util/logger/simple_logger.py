import logging
import os
from logging.handlers import TimedRotatingFileHandler


class simple_logger():

    #region Constructors

    def __init__(self,name,enabled=True):
        self.prepare_logs(name)
        self.enabled=enabled

    #endregion

    #region Private Methods

    def prepare_logs(self,name):
        self.logger = logging.getLogger(name)

        log_path = os.path.join("logs", name)

        main_formatter = logging.Formatter(
            fmt='%(asctime)s [%(module)s %(levelname)s] %(message)s',
            datefmt='%Y-%m-%d %H:%M:%S')

        console_handler = logging.StreamHandler()
        file_handler = TimedRotatingFileHandler(
            filename=log_path, when='d', backupCount=20)

        for handler in [console_handler, file_handler]:
            handler.setFormatter(main_formatter)
            self.logger.addHandler(handler)
        self.logger.setLevel(20)  # 20 = normal

    #endregion

    #region Public Methods

    def log(self,msg):
        #print(msg)
        if self.enabled:
            self.logger.info(msg)
    def cond_log(self,msg,cond):
        if cond and self.enabled:
            self.logger.info(msg)

    #endregion