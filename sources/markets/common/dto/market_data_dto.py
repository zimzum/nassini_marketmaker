class market_data_dto:
    def __init__(self,p_tradecontract=None,p_datetime=None,p_last_px=None,p_last_qty=None,p_bid_px=None,p_offer_px=None,
                 p_bid_qty=1,p_offer_qty=1,p_bid_px_2=0,p_offer_px_2=0):

        self.datetime=[]
        self.tradecontract=p_tradecontract
        self.last_px=[]
        self.last_qty = []
        self.bid_px=[]
        self.offer_px=[]
        self.bid_px_2 = None
        self.offer_px_2 = None
        self.bid_qty=[]
        self.offer_qty=[]


        if p_datetime is not None:
            self.datetime.append(p_datetime)

        if p_last_px is not None:
            self.last_px.append(p_last_px)

        if p_last_qty is not None:
            self.last_qty.append(p_last_qty)

        if p_bid_px is not None:
            self.bid_px.append(p_bid_px)

        if p_offer_px is not None:
            self.offer_px.append(p_offer_px)

        self.bid_px_2=p_bid_px_2
        self.offer_px_2=p_offer_px_2

        if p_bid_qty is not None:
            self.bid_qty.append(p_bid_qty)

        if p_offer_qty is not None:
            self.offer_qty.append(p_offer_qty)


    def get_last_date_time(self):

        return self.datetime[0] if len (self.datetime)>0 else None

    def get_last_last_px(self):

        return self.last_px[0] if len(self.last_px) > 0 else None

    def get_last_last_qty(self):

        return self.last_qty[0] if len(self.last_qty) > 0 else None

    def get_last_bid_px(self):

        return self.bid_px[0] if len(self.bid_px) > 0 else None

    def get_last_offer_px(self):

        return self.offer_px[0] if len(self.offer_px) > 0 else None

    def get_last_bid_qty(self):

        return self.bid_qty[0] if len(self.bid_qty) > 0 else None

    def get_last_p_offer_qty(self):

        return self.offer_qty[0] if len(self.offer_qty) > 0 else None

    def get_last_bid_px_2(self):

        return self.bid_px_2

    def get_last_offer_px_2(self):

        return self.offer_px_2


    def different_tif(self,md):

        if md is not None:
            return (self.get_last_offer_px()!=md.get_last_offer_px()
                   or self.get_last_last_px()!=md.get_last_last_px()
                   or self.get_last_bid_px()!=md.get_last_bid_px()
                   or self.get_last_offer_px()!=md.get_last_offer_px())
        else:
            return True