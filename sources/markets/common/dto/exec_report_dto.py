from sources.framework.common.enums.Side import *
class executed:
    def __init__(self,p_size,p_price):
        self.size=p_size
        self.price=p_price

class exec_report_dto:
    def __init__(self,p_order,p_status,p_cum_qty,p_avg_px):

        self.order=p_order

        self.m_orderId=p_order.OrderId
        self.m_serverOrderId=p_order.ClOrdId
        self.size=p_order.OrderQty if p_order.Side==Side.Buy else -1*p_order.OrderQty
        self.price=p_order.Price
        self.status=p_status

        if p_order.Side == Side.Sell:
            p_cum_qty=-1*p_cum_qty


        self.executed=executed(p_size=p_cum_qty,p_price=p_avg_px)