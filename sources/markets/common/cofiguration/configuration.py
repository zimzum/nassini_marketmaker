import configparser
import datetime

class configuration:
    def __init__(self, configFile):
        config = configparser.ConfigParser()
        config.read(configFile)

        self.vendor_server = config['DEFAULT']['VENDOR_SERVER']
        self.vendor_port = config['DEFAULT']['VENDOR_PORT']

        self.connection_string = config['DEFAULT']['CONNECTION_STRING']

        self.data_source_mode = config['DEFAULT']['DATA_SOURCE_MODE']

        self.symbol = config['DEFAULT']['SYMBOL']
        self.name = config['DEFAULT']['NAME']

        self.vendor_start_date = config['DEFAULT']['VENDOR_START_DATE']
        self.vendor_end_date = config['DEFAULT']['VENDOR_END_DATE']


        self.start_date=  datetime.datetime.strptime(str(config['DEFAULT']['START_DATE']), '%d/%m/%Y %H:%M:%S')
        self.end_date =  datetime.datetime.strptime(str(config['DEFAULT']['END_DATE']), '%d/%m/%Y %H:%M:%S')

        self.persist_incoming_market_data = config['DEFAULT']['PERSIST_INCOMING_MARKET_DATA']=="True"
        self.sleep_time_between_executions = int( config['DEFAULT']['SLEEP_TIME_BETWEEN_EXECUTIONS'])
        self.sleep_time_between_exec_reports = int(config['DEFAULT']['SLEEP_TIME_BETWEEN_EXEC_REPORTS'])
        self.periodic_persist = config['DEFAULT']['PERIODIC_PERSIST']
        self.md_as_float = config['DEFAULT']['MD_AS_FLOAT']=="True"
        self.allow_multiple_orders = config['DEFAULT']['ALLOW_MULTIPLE_ORDERS'] == "True"

        self.beep_on_delays = config['DEFAULT']['BEEP_ON_DELAYS'] == "True"
        self.log_buy_sell_replacements = config['DEFAULT']['LOG_BUY_SELL_REPLACEMENTS'] == "True"
        self.dummy_url = config['DEFAULT']['DUMMY_URL']

        self.market_start_time = config['DEFAULT']['MARKET_START_TIME']
        self.market_end_time = config['DEFAULT']['MARKET_END_TIME']




