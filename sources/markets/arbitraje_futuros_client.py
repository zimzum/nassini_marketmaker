from sources.client.strategies.dummy_router.dummy_router import *
from sources.client.strategies.arbitraje_futuro.arbitraje_futuro import *
from sources.markets.fake_market.fake_market import *
from sources.markets.common.cofiguration.configuration import *
import datetime
import  decimal
from decimal import Decimal, ROUND_HALF_UP, getcontext

getcontext().prec = 51

def round_as_decimal(num, decimal_places=2):
    """Round a number to a given precision and return as a Decimal

    Arguments:
    :param num: number
    :type num: int, float, decimal, or str
    :returns: Rounded Decimal
    :rtype: decimal.Decimal
    """
    precision = '1.{places}'.format(places='0' * decimal_places)
    return Decimal(str(num)).quantize(Decimal(precision), rounding=ROUND_HALF_UP)

if __name__ == '__main__':


    configuration =  configuration("../../configs/backtester_arbitraje_futuros.ini")
    arbitraje_futuro_backtester_proxy.arbitraje_futuro_backtester_proxy_pre_initialization(configuration.symbol)
    arbitraje_futuro = ArbitrajeMultiFuturos()
    arbitraje_futuro.impl_backtest = True


    fake_market = fake_market(arbitraje_futuro,p_configuration=configuration)

    fake_market.start()

    input()
