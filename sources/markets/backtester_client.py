from sources.client.strategies.dummy_router.dummy_router import *
from sources.client.strategies.market_maker.market_maker import *
from sources.markets.fake_market.fake_market import *
from sources.markets.common.cofiguration.configuration import *
import datetime
import  decimal
from decimal import Decimal, ROUND_HALF_UP, getcontext

getcontext().prec = 51

def round_as_decimal(num, decimal_places=2):
    """Round a number to a given precision and return as a Decimal

    Arguments:
    :param num: number
    :type num: int, float, decimal, or str
    :returns: Rounded Decimal
    :rtype: decimal.Decimal
    """
    precision = '1.{places}'.format(places='0' * decimal_places)
    return Decimal(str(num)).quantize(Decimal(precision), rounding=ROUND_HALF_UP)

if __name__ == '__main__':


    configuration =  configuration("../../configs/backtester.ini")
    market_maker = market_maker()
    market_maker.impl_backtest = True

    # imbalance
    market_maker.imbalance_min_threshold = decimal.Decimal(0.75)
    market_maker.imbalance_exit_threshold = decimal.Decimal(0.60)
    market_maker.imbalance_price_delta = decimal.Decimal(500)
    market_maker.trade_unit = decimal.Decimal(5)

    # rope setting
    market_maker.elastic_rope_pct = 0.5

    # inventory
    market_maker.default_spread = decimal.Decimal(50)  # min price size
    market_maker.inventory_spread_unit = decimal.Decimal(300)
    market_maker.inv_threshold = decimal.Decimal(1)

    # mid price strategy
    market_maker.mid_price_calculation = int(4)

    # aggressiveness
    market_maker.impl_opposite_inventory = 0
    market_maker.impl_strong_direction =0

    #Rope on regular price
    market_maker.impl_rope_price_on_regular_markets = 1

    #ATR imbalance exit
    market_maker.impl_ATR_imbalance_exit=0
    market_maker.ATR_imbalance_safety_mult = 6

    market_maker.max_long_qty = 20
    market_maker.max_short_qty = -20

    # momentum
    market_maker.new_maxmin_start_count = 40
    market_maker.new_max_min_end_count = 20

    market_maker.elastic_rope_pct = decimal.Decimal(0.5)

    fake_market = fake_market(market_maker,p_configuration=configuration)

    fake_market.start()

    input()
