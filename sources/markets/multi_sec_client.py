from sources.client.strategies.dummy_router.dummy_router import *
from sources.client.strategies.multi_sec_test.multi_sec_test import *
from sources.markets.fake_market.fake_market import *
from sources.markets.common.cofiguration.configuration import *
import datetime
import  decimal
from decimal import Decimal, ROUND_HALF_UP, getcontext

getcontext().prec = 51

def round_as_decimal(num, decimal_places=2):
    """Round a number to a given precision and return as a Decimal

    Arguments:
    :param num: number
    :type num: int, float, decimal, or str
    :returns: Rounded Decimal
    :rtype: decimal.Decimal
    """
    precision = '1.{places}'.format(places='0' * decimal_places)
    return Decimal(str(num)).quantize(Decimal(precision), rounding=ROUND_HALF_UP)

if __name__ == '__main__':


    configuration =  configuration("../../configs/backtester.ini")
    market_maker = market_maker()
    market_maker.impl_backtest = True
    market_maker.periodic_persist=backtest_period.Daily.value

    # inventory
    market_maker.default_spread = round_as_decimal(100,2)
    market_maker.inventory_spread_unit = round_as_decimal(300,2)
    market_maker.inv_threshold = round_as_decimal(1,2)

    fake_market = fake_market(market_maker,p_configuration=configuration)

    fake_market.start()

    input()
