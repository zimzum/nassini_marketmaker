import sys
import traceback
import winsound

from sources.framework.util.generic.log_util import log_util
from sources.framework.util.generic.time_util import time_util
from sources.markets.common.dto.exec_report_dto import *
from sources.client.framework.data_access_layer.backtest_manager import *
from sources.client.framework.data_access_layer.backtest_parameter_manager import *
from sources.markets.fake_market.data_vendor import *
from sources.client.framework.business_entities.backtest import *
from sources.markets.data_access_layer.market_data_manager import *
from sources.framework.common.enums.OrdType import *
from sources.markets.common.util.calendar_helper import *
from sources.markets.common.util.logger.simple_logger import *
from sources.framework.business_entities.orders.order import *
from sources.framework.business_entities.securities.security import *
import threading
import time
import datetime
import backtrader as bt
import uuid

_VENDOR_DATA_SOURCE = "VENDOR"
_DB_DATA_SOURCE = "DATABASE"


class fake_market:

    def __init__(self, p_trading_algo, p_configuration):
        self.trading_algo = p_trading_algo
        self.trading_algo.FakeMarket = self

        self.symbols = p_configuration.symbol.split(",")

        self.start_date = p_configuration.start_date
        self.end_date = p_configuration.end_date
        self.vendor_start_date = p_configuration.vendor_start_date
        self.vendor_end_date = p_configuration.vendor_end_date

        self.configuration = p_configuration

        self.data_source_mode = p_configuration.data_source_mode

        self.load_managers(p_configuration)

        self.init_backtest(p_configuration)

        self.init_backtest_paramters(p_configuration)

        if p_configuration.data_source_mode == _VENDOR_DATA_SOURCE:
            self.data_vendor = data_vendor(p_server=p_configuration.vendor_server, p_port=p_configuration.vendor_port)
        elif p_configuration.data_source_mode == _DB_DATA_SOURCE:
            self.data_vendor = None
        else:
            raise Exception("Unknown market data source {}".format(p_configuration.data_source_mode))

        self.market_data = {}
        self.inc_orders = {}

        self.do_lock = True
        self.lock = threading.Lock()

        for symbol in self.symbols:
            self.inc_orders[symbol] = []
            self.market_data[symbol] = []

        # region Internal Variables

        self.pending_action = False
        self.last_best_bid_on_trade = {}
        self.last_best_ask_on_trade = {}
        self.last_px_on_trade = {}

        self.reset_memory()

        # endregion

        # region Calcualted Stats

        self.cum_profit = 0
        self.day_profit = 0
        self.net_sells = 0
        self.net_buys = 0
        self.acum_net_long_contracts = {}
        self.acum_net_short_contracts = {}

        self.market_start_time= datetime.datetime.strptime(self.configuration.market_start_time, '%H:%M:%S')
        self.market_end_time = datetime.datetime.strptime(self.configuration.market_end_time, '%H:%M:%S')

        i = 0
        for symbol in self.symbols:
            self.acum_net_long_contracts[symbol] = 0
            self.acum_net_short_contracts[symbol] = 0
            setattr(self, "data" + str(i), None)

        # endregion

    # region Threads

    def send_execution_reports(self, exec_reports):
        clOrdId=None
        orderId=None
        try:
            to_sleep=self.configuration.sleep_time_between_exec_reports / 1000
            time.sleep(to_sleep)  # 50ms should be ok
            for exec_report in exec_reports:

                self.trading_algo.notify_order(exec_report)

                self.simple_logger.cond_log("<exec_reports_sent> @{} for ClOrdId={} OrderId={} Status={}"
                                       .format(time_util.full_now(),exec_report.m_serverOrderId,
                                               exec_report.m_orderId,exec_report.status),self.configuration.log_buy_sell_replacements)


                time.sleep(to_sleep)  # 50ms should be ok
        except Exception as e:
            self.handle_trading_error(e,"ERROR @send_execution_reports:{}".format(str(e)))
        finally:
            self.pending_action = False


    def eval_new_day(self, curr_md, prev_md):

        if prev_md != None and calendar_helper.eval_new_day(curr_md.get_last_date_time(), prev_md.get_last_date_time()):

            start = calendar_helper.implement_first_hour(prev_md.get_last_date_time())

            end = calendar_helper.implement_last_hour(prev_md.get_last_date_time())

            self.trading_algo.on_daily_change(self.backtest, start, end)

            self.day_profit = 0
            self.cum_profit += self.trading_algo.get_cum_profit()
            self.net_sells += self.trading_algo.get_net_sells()
            self.net_buys += self.trading_algo.get_net_buys()

            for symbol in self.symbols:
                self.acum_net_long_contracts[symbol] += self.trading_algo.get_net_long_contracts_for_symbol(symbol)
                self.acum_net_short_contracts[symbol] += self.trading_algo.get_net_short_contracts_for_symbol(symbol)

            self.reset_memory()

            self.trading_algo.start()

            self.trading_algo.backtest_prestart(curr_md.get_last_date_time())

        txt_summary=self.trading_algo.get_summary()
        if txt_summary!="" and txt_summary is not None:
            self.simple_logger.log(txt_summary)

    def eval_executions(self,data):
        i=0
        if len(self.symbols) > 1:
            for symbol in self.symbols:
                md = getattr(self.trading_algo, "data" + str(i))
                if md is not None:
                    self.eval_execution(md)
                i +=1

        else:
            self.eval_execution(data)

    def eval_market_time(self,md_time):
        start_date_time=datetime.datetime(year=md_time.year,month=md_time.month,day=md_time.day,hour=self.market_start_time.hour,
                                          minute=self.market_start_time.minute,second=self.market_start_time.second)

        end_date_time = datetime.datetime(year=md_time.year, month=md_time.month, day=md_time.day,
                                   hour=self.market_end_time.hour,
                                   minute=self.market_end_time.minute, second=self.market_end_time.second)

        return start_date_time<=md_time and md_time<=end_date_time


    def eval_new_hour(self, curr_md, prev_md):

        if prev_md != None and calendar_helper.eval_new_hour(curr_md.get_last_date_time(),
                                                             prev_md.get_last_date_time()):
            start = calendar_helper.implement_first_minute(prev_md.get_last_date_time())
            end = calendar_helper.implement_last_minute(prev_md.get_last_date_time())

            self.trading_algo.on_hourly_change(self.backtest, start, end)

            self.simple_logger.log(
                "Persisting new hour from={} to={} for backtest {}".format(start, end, self.backtest.name))

    def eval_new_minute(self, curr_md, prev_md):

        if prev_md != None:
            if calendar_helper.eval_new_minute(curr_md.get_last_date_time(), prev_md.get_last_date_time()):
                start = calendar_helper.implement_first_second(prev_md.get_last_date_time())
                end = calendar_helper.implement_last_second(prev_md.get_last_date_time())

                self.trading_algo.on_minute_change(self.backtest, start, end)

                self.simple_logger.log(
                    "Persisting new minute from={} to={} for backtest {}".format(start, end, self.backtest.name))

    def send_market_data(self):

        pivot_symbol = self.symbols[0]

        prev_data = None
        for data in self.market_data[pivot_symbol]:
            skipped_md=False
            try:

                self.eval_new_day(data, prev_data)

                if not self.eval_market_time(data.get_last_date_time()):
                    skipped_md=True
                    prev_data = data
                    continue

                if not data.different_tif(prev_data):
                    skipped_md = True
                    self.eval_new_hour(data, prev_data)
                    self.eval_new_minute(data, prev_data)
                    prev_data = data
                    continue

                self.wait_for_pending_action()

                self.lock.acquire(blocking=self.do_lock)

                self.simple_logger.log("{}-Processing market data for timestmap={}".format(time_util.full_now(),data.get_last_date_time()))

                self.trading_algo.now = data.get_last_date_time()
                self.trading_algo.data = data

                self.lock.release()
                if len(self.symbols) > 1:
                    i = 0
                    for symbol in self.symbols:
                        sorted_md=sorted(self.market_data[symbol],key=lambda x: x.get_last_date_time(),reverse=True)
                        closest_md = next((x for x in sorted_md if x.get_last_date_time() <= data.get_last_date_time()), None)
                        if closest_md is not None:
                            self.simple_logger.log("Found closest md for symbol {}: {} (pivot md={})".format(symbol,closest_md.get_last_date_time(),data.get_last_date_time()))
                            setattr(self.trading_algo, "data" + str(i), closest_md)
                            self.eval_execution(closest_md)
                        i += 1
                else:
                    self.eval_execution(data)

                # the idea is to leave some time for the ER to arrive
                # so by the time the market data arrives, we won't replace orders that are being filled
                time.sleep(self.configuration.sleep_time_between_exec_reports / 1000)

                self.trading_algo.next()

                self.eval_new_hour(data, prev_data)
                self.eval_new_minute(data, prev_data)

                prev_data = data


            except Exception as e:
                #traceback.print_exc()
                #self.simple_logger.log(traceback.print_exc())
                msg = "ERROR @send_market_data:{}".format(str(e))
                self.simple_logger.log(msg)

            finally:
                if self.lock.locked():
                    self.lock.release()
                if not skipped_md:
                    to_sleep = self.configuration.sleep_time_between_executions / 1000
                    time.sleep(to_sleep)

    # endregion

    # region Private Methods

    # region Initial Loads

    def load_managers(self, p_configuration):
        self.market_data_manager = market_data_manager(p_configuration.connection_string)

        self.backtest_manager = backtest_manager(p_configuration.connection_string)

        self.backtest_parameter_manager = backtest_parameter_manager(p_configuration.connection_string)

    def init_backtest(self, p_configuration):

        self.simple_logger = simple_logger("FakeMarket")

        self.simple_logger.log("Persisting strategy {}".format(p_configuration.name))

        self.backtest = backtest(p_configuration.name, datetime.datetime.now())

        self.backtest_manager.parsist_backtest(self.backtest)

        self.backtest.id = self.backtest_manager.get_backtest_id(self.backtest.datetime, self.backtest.name)

        self.simple_logger.log("Strategy persisted. Id= {}".format(self.backtest))

        self.trading_algo.on_init_backtester(self.configuration)

    def init_backtest_paramters(self, p_configuration):

        parms = self.trading_algo.get_config_params(self.backtest)

        for parameter in parms:
            self.simple_logger.log("Persisting parameter {}".format(parameter.key))
            self.backtest_parameter_manager.parsist_backtest_paramter(parameter)
        self.simple_logger.log("All parameters persisted")

    def reset_memory(self):
        for symbol in self.symbols:
            self.last_best_bid_on_trade[symbol] = None
            self.last_best_ask_on_trade[symbol] = None
            self.last_px_on_trade[symbol] = None
            self.inc_orders[symbol].clear()

    def load_data_from_vendor(self):

        for symbol in self.symbols:
            md_tick_array = self.data_vendor.get_data(security_id=symbol,
                                                      start_date=self.vendor_start_date,
                                                      end_date=self.vendor_end_date)

            for index, tick in md_tick_array.iterrows():
                md = market_data_dto(p_tradecontract=symbol,
                                     p_datetime=tick['fecha_insercion'],
                                     p_last_px=tick['ultimo_precio'],
                                     p_last_qty=tick['ultimo_size'],
                                     p_bid_px=tick['bi_1_precio'],
                                     p_offer_px=tick['of_1_precio'],
                                     p_bid_qty=tick['bi_1_size'],
                                     p_offer_qty=tick['of_1_size'],
                                     p_bid_px_2=tick['bi_2_precio'],
                                     p_offer_px_2=tick['of_2_precio'], )

                if self.configuration.persist_incoming_market_data:
                    self.market_data_manager.parsist_market_data(md)

                self.market_data[symbol].append(md)

    def load_data_from_DB(self):
        i = 0
        pivot_symbol = self.symbols[0]
        for symbol in self.symbols:
            md_list = self.market_data_manager.get_market_data(symbol, self.start_date, self.end_date,
                                                               self.configuration.md_as_float)

            do_reverse = False if i == 0 or pivot_symbol == symbol else True

            md_list = sorted(filter(lambda x: x.get_last_date_time() is not None, md_list),
                             key=lambda x: x.get_last_date_time(),
                             reverse=do_reverse)

            self.market_data[symbol] = md_list
            i += 1

    # endregion

    # region

    def wait_for_pending_action(self):
        while self.pending_action:
            if self.configuration.beep_on_delays:
                winsound.Beep(2500, 1000)
            self.simple_logger.log("WARNING-Waiting for Pending Action @send_market_data")
            to_sleep = (2 * self.configuration.sleep_time_between_exec_reports) / 1000
            time.sleep(to_sleep)

    def handle_trading_error(self,e,msg):
        #self.simple_logger.log(traceback.print_exc())
        self.simple_logger.log(msg)
        if self.pending_action:
            self.pending_action = False

    def find_symbol_by_order_id(self, orderId):
        for symbol in self.inc_orders.keys():
            for order in self.inc_orders[symbol]:
                if order.OrderId == orderId:
                    return symbol

        raise Exception("Could not find order with order Id {}".format(orderId))

    def validate_new_buy_order(self, symbol, price, tradeid):
        curr_order = next((b for b in self.inc_orders[symbol] if b.Side == Side.Buy), None)

        if curr_order is not None and not self.configuration.allow_multiple_orders:
            winsound.Beep(2500, 1000)
            raise Exception("Error-Fake Market cant handle more than one order. client_order_id={} vs old_order_id {}"
                            .format(tradeid,curr_order.ClOrdId))

        if (price is None):
            winsound.Beep(2500, 1000)
            raise Exception("Error-Market Orders have to be routed through buy_market method")

    def validate_new_sell_order(self, symbol, price, tradeid):
        curr_order = next((b for b in self.inc_orders[symbol] if b.Side == Side.Sell), None)

        if curr_order is not None and not self.configuration.allow_multiple_orders:
            winsound.Beep(2500, 1000)
            raise Exception("Error-Fake Market cant handle more than one order. client_order_id={} vs old_order_id {}"
                            .format(tradeid, curr_order.ClOrdId))

        if (price is None):
            winsound.Beep(2500, 1000)
            raise Exception("Error-Market Orders have to be routed through sell_market method")

    # endregion

    # region Execution Logic

    def run_trade(self, market_data, order, trade_size, avg_px):

        order.CumQty += trade_size

        if order.CumQty > order.OrderQty: #it must have been an order book shift
            order.CumQty=order.OrderQty
            trade_size = order.LvsQty
            order.LvsQty=0
        else:
            order.LvsQty = order.OrderQty-order.CumQty

        order.AvgPx = avg_px

        if order.LvsQty <0:
            raise Exception("Critical error running fake market. Lvs Qty es lower than 0!: LvsQty={} OrdQty={}"
                            .format(order.LvsQty, order.OrderQty))

        if (order.CumQty == order.OrderQty):
            self.inc_orders[market_data.tradecontract].remove(
                order)  # For the moment we consider it is a full trade for bid an ask

        trade_report = exec_report_dto(p_order=order,
                                       p_status=bt.Order.Completed if trade_size >= order.OrderQty else bt.Order.Partial,
                                       p_cum_qty=order.CumQty, p_avg_px=avg_px)

        threading.Thread(target=self.send_execution_reports, args=([trade_report],)).start()

    def eval_sell_execution(self, market_data):

        sell_order = next((s for s in self.inc_orders[market_data.tradecontract] if s.Side == Side.Sell), None)

        if sell_order is not None:
            if (market_data.get_last_bid_px() >= sell_order.Price
                    and (self.last_best_bid_on_trade[market_data.tradecontract] is None or self.last_best_bid_on_trade[
                        market_data.tradecontract] != market_data.get_last_bid_px())
            ):
                # AGGRESSIVE ORDER: SELL ORDER LOWER THAN BID. The execution is the bid
                self.last_best_bid_on_trade[market_data.tradecontract] = market_data.get_last_bid_px()
                exec_price = market_data.get_last_bid_px()
                trade_size=sell_order.LvsQty

                #trade size is the whole order qty
                self.run_trade(market_data, sell_order, trade_size,exec_price)
                return

            if (round(market_data.get_last_last_px(),2) >= round(sell_order.Price,2)
                    and (self.last_px_on_trade[market_data.tradecontract] is None or self.last_px_on_trade[
                        market_data.tradecontract] != market_data.get_last_last_px())
            ):
                # PASSIVE ORDER> SELL ORDER NOT AGGRESSIVE. There is a trade with higher price, we assume our order would have been executed
                self.last_px_on_trade[market_data.tradecontract] = market_data.get_last_last_px()
                trade_size = sell_order.LvsQty if market_data.get_last_last_qty() > sell_order.LvsQty else market_data.get_last_last_qty()
                exec_price = sell_order.Price

                self.simple_logger.log(
                                        "<BACKTESTER.SELL.ONTRADE> Last_px={} exe_price={} trade_size={} (LvsQty={}) my_ask={} market_ask={}"
                                        .format(market_data.get_last_last_px(),exec_price,trade_size,sell_order.LvsQty,
                                               sell_order.Price,market_data.get_last_offer_px()))
                self.run_trade(market_data, sell_order, trade_size, exec_price)

                return

    def eval_buy_execution(self, market_data):

        buy_order = next((b for b in self.inc_orders[market_data.tradecontract] if b.Side == Side.Buy), None)

        if buy_order is not None:
            if (market_data.get_last_offer_px() <= buy_order.Price
                    and (self.last_best_ask_on_trade[market_data.tradecontract] is None
                     or self.last_best_ask_on_trade[market_data.tradecontract] != market_data.get_last_offer_px())
            ):
                # AGGRESSIVE ORDER: BUY ORDER BIGGER THAN ASK. The execution is the ask

                self.last_best_ask_on_trade[market_data.tradecontract] = market_data.get_last_offer_px()
                exec_price = market_data.get_last_offer_px()
                trade_size=buy_order.LvsQty

                # trade size is the whole order qty
                self.run_trade(market_data, buy_order, trade_size, exec_price)
                return

            if (round(market_data.get_last_last_px(),2) <= round(buy_order.Price,2)
                    and (self.last_px_on_trade[market_data.tradecontract] is None
                         or self.last_px_on_trade[market_data.tradecontract] != market_data.get_last_last_px())
            ):
                # PASSIVE ORDER: BUY ORDER NOT AGGRESSIVE. There is a trade with lower price, we assume our order would have been executed

                self.last_px_on_trade[market_data.tradecontract] = market_data.get_last_last_px()
                trade_size = buy_order.LvsQty if market_data.get_last_last_qty() > buy_order.LvsQty else market_data.get_last_last_qty()
                exec_price = buy_order.Price

                self.simple_logger.log("<BACKTESTER.BUY.ONTRADE> Last_px={}  exec_price={} trade_size={} (LvsQty={}) my_bid={} market_bid={}"
                                        .format(market_data.get_last_last_px(),exec_price,trade_size,buy_order.LvsQty,
                                         buy_order.Price,market_data.get_last_bid_px()))
                self.run_trade(market_data, buy_order, trade_size, exec_price)
                return

    def eval_execution(self, market_data):

        try:

            buy_order = next((b for b in self.inc_orders[market_data.tradecontract] if b.Side == Side.Buy), None)
            sell_order = next((s for s in self.inc_orders[market_data.tradecontract] if s.Side == Side.Sell), None)

            self.simple_logger.log(
                "best_bid={}-order_buy={} LastPx={} order_sell={}-best_ask={}".format(market_data.get_last_bid_px(),
                                                                                      buy_order.Price if buy_order is not None else "-",
                                                                                      market_data.get_last_last_px(),
                                                                                      sell_order.Price if sell_order is not None else "",
                                                                                      market_data.get_last_offer_px()))

            self.eval_sell_execution(market_data)

            self.eval_buy_execution(market_data)
        except Exception as e:
            self.handle_trading_error(e, "ERROR @eval_execution:{}".format(str(e)))
            #self.simple_logger.log(traceback.print_exc())

    # endregion

    # endregion

    # region Public Methods

    def start(self):
        self.load_data()
        self.start_trading()

    def load_data(self):

        if self.data_source_mode == _VENDOR_DATA_SOURCE:
            self.data_vendor = self.load_data_from_vendor()
        elif self.data_source_mode == _DB_DATA_SOURCE:
            self.data_vendor = self.load_data_from_DB()

    def start_trading(self):

        self.trading_algo.start()

        datetimes = []
        for symbol in self.symbols:
            datetimes.append(self.market_data[symbol][0].get_last_date_time())

        start_time = min(datetimes)
        self.trading_algo.backtest_prestart(start_time)

        threading.Thread(target=self.send_market_data, args=()).start()

    def buy(self, data, size, price, exectype, tradeid):

        try:
            self.lock.acquire(blocking=self.do_lock)

            self.pending_action = True
            self.simple_logger.cond_log("Validating new order creation @buy",self.configuration.log_buy_sell_replacements)
            self.validate_new_buy_order(data.tradecontract, price, tradeid)

            buy_order = Order(ClOrdId=tradeid, OrigClOrdId=None, Security=Security(Symbol=data.tradecontract),
                              SettlType=SettlType.Regular,
                              Side=Side.Buy, Exchange=None, OrdType=OrdType.Limit,
                              QuantityType=QuantityType.SHARES, OrderQty=size, PriceType=PriceType.FixedAmount,
                              Price=price)

            order_id = str(uuid.uuid4())
            buy_order.OrderId = order_id

            self.inc_orders[data.tradecontract].append(buy_order)
            er = exec_report_dto(p_order=buy_order, p_status=bt.Order.Submitted, p_cum_qty=buy_order.CumQty,
                                 p_avg_px=buy_order.AvgPx)

            exec_reports = []
            exec_reports.append(
                exec_report_dto(p_order=buy_order, p_status=bt.Order.Accepted, p_cum_qty=buy_order.CumQty,
                                p_avg_px=buy_order.AvgPx))

            threading.Thread(target=self.send_execution_reports, args=(exec_reports,)).start()

            return er

        except Exception as e:
            self.handle_trading_error(e,"ERROR @buy:{}".format(str(e)))
            log_util.log_line()
            raise e
        finally:
            if self.lock.locked():
                self.lock.release()

    def sell(self, data, size, price, exectype, tradeid=None):

        try:

            self.lock.acquire(blocking=self.do_lock)

            self.pending_action = True
            self.simple_logger.cond_log("Validating new order creation @sell",self.configuration.log_buy_sell_replacements)
            self.validate_new_sell_order(data.tradecontract, price, tradeid)

            sell_order = Order(ClOrdId=tradeid, OrigClOrdId=None, Security=Security(Symbol=data.tradecontract),
                               SettlType=SettlType.Regular,
                               Side=Side.Sell, Exchange=None, OrdType=OrdType.Limit,
                               QuantityType=QuantityType.SHARES, OrderQty=size, PriceType=PriceType.FixedAmount,
                               Price=price)

            order_id = str(uuid.uuid4())
            sell_order.OrderId = order_id

            self.inc_orders[data.tradecontract].append(sell_order)
            er = exec_report_dto(p_order=sell_order, p_status=bt.Order.Submitted, p_cum_qty=sell_order.CumQty,
                                 p_avg_px=sell_order.AvgPx)

            exec_reports = []
            exec_reports.append(
                exec_report_dto(p_order=sell_order, p_status=bt.Order.Accepted, p_cum_qty=sell_order.CumQty,
                                p_avg_px=sell_order.AvgPx))

            threading.Thread(target=self.send_execution_reports, args=(exec_reports,)).start()

            return er

        except Exception as e:
            self.handle_trading_error(e,"ERROR @sell:{}".format(str(e)))
            log_util.log_line()
            raise e
        finally:
            if self.lock.locked():
                self.lock.release()

    def replace(self, order, size, price, tradeid=None):

        try:

            self.lock.acquire(blocking=self.do_lock)

            symbol = self.find_symbol_by_order_id(order.m_orderId)
            curr_order = next((b for b in self.inc_orders[symbol] if b.OrderId == order.m_orderId), None)

            if curr_order is not None:

                self.pending_action = True
                self.inc_orders[symbol].remove(curr_order)

                repl_order = Order(ClOrdId=curr_order.ClOrdId if tradeid is None else tradeid,
                                   OrigClOrdId=curr_order.ClOrdId,
                                   Security=Security(Symbol=symbol), SettlType=SettlType.Regular,
                                   Side=curr_order.Side, Exchange=None, OrdType=OrdType.Limit,
                                   QuantityType=QuantityType.SHARES, OrderQty=abs(size),
                                   PriceType=PriceType.FixedAmount, Price=price)

                repl_order.OrderId = str(uuid.uuid4())

                self.inc_orders[symbol].append(repl_order)

                self.simple_logger.cond_log(
                                            "{}-Replacing order {} with new price={} and size={} (len={})".format(time_util.full_now(),order.m_orderId, price, size,
                                                                                                               len(self.inc_orders[symbol])),
                                            self.configuration.log_buy_sell_replacements)


                exec_reports = []
                exec_reports.append(
                    exec_report_dto(p_order=curr_order, p_status=bt.Order.Canceled, p_cum_qty=curr_order.CumQty,
                                    p_avg_px=curr_order.AvgPx))
                exec_reports.append(
                    exec_report_dto(p_order=repl_order, p_status=bt.Order.Accepted, p_cum_qty=0, p_avg_px=None))

                # self.send_execution_reports(exec_reports)
                threading.Thread(target=self.send_execution_reports, args=(exec_reports,)).start()
                return exec_report_dto(p_order=repl_order, p_status=bt.Order.Submitted, p_cum_qty=0, p_avg_px=None)

            else:
                raise Exception("Error-Could not find active order for OrderId={}".format(order.m_orderId))
        except Exception as e:
            self.handle_trading_error(e,"ERROR @replace:{}".format(str(e)))
            log_util.log_line()
            raise e
        finally:
            if self.lock.locked():
                self.lock.release()

    def cancel(self, order):

        try:

            self.lock.acquire(blocking=self.do_lock)

            symbol = self.find_symbol_by_order_id(order.m_orderId)
            curr_order = next((b for b in self.inc_orders[symbol] if b.OrderId == order.m_orderId), None)

            if curr_order is not None:

                self.pending_action = True
                self.inc_orders[symbol].remove(curr_order)

                self.simple_logger.cond_log("Cancelling order {} (len={})".format(order.m_orderId, len(self.inc_orders[symbol])),
                                            self.configuration.log_buy_sell_replacements)

                exec_reports = []
                exec_reports.append(
                    exec_report_dto(p_order=curr_order, p_status=bt.Order.Canceled, p_cum_qty=curr_order.CumQty,
                                    p_avg_px=curr_order.AvgPx))
                # self.send_execution_reports(exec_reports)
                threading.Thread(target=self.send_execution_reports, args=(exec_reports,)).start()
                return exec_reports[0]

            else:
                raise Exception("Error-Could not find active order for OrderId=[]".format(order.m_orderId))

        except Exception as e:
            self.handle_trading_error(e,"ERROR @cancel:{}".format(str(e)))
            log_util.log_line()
            raise e
        finally:
            if self.lock.locked():
                self.lock.release()

    # endregion
