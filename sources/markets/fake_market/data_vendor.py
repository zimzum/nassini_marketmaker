from datetime import datetime
import requests
import pandas as pd
import urllib.parse


class data_vendor:

    def __init__(self,p_server,p_port):
        self.server = p_server
        self.port = p_port
        self.url_base = "http://" + self.server + ":" + self.port + "/marketdata_OMS/"

    def get_data(self, security_id: str, start_date: str, end_date: str):
        '''
        SECURITYID : Es el securityId que llega de byma, y el mismo que se puede consultar en la api de simbolos
        INICIO : 'dd/mm/aaaa'
        FIN : 'dd/mm/aaaa'
        '''
        start_date = str(int(datetime.timestamp(datetime.strptime(start_date, "%d/%m/%Y"))))
        end_date = str(int(datetime.timestamp(datetime.strptime(end_date, "%d/%m/%Y"))))

        safe_security = urllib.parse.quote(security_id,safe='')

        url = self.url_base + safe_security + '/' + start_date + '/' + end_date


        data = requests.get(url).json()
        data = pd.DataFrame(data['data'], columns=data['columnas'])
        return data
        # return self.filter_repeated_data(data)
