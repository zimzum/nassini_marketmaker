import pyodbc
import uuid
import datetime
from sources.markets.common.dto.market_data_dto import *

_symbol=0
_date_time=1
_last_px=2
_last_qty=3
_best_bid_px=4
_best_ask_px=5
_bid_qty=6
_offer_qty=7
_bid_px_2=8
_offer_px_2=9



class market_data_manager():

    #region Constructors

    def __init__(self, connString):
        self.i = 0
        self.connection = pyodbc.connect(connString)
    #endregion

    #region Private Methods

    def build_market_data_dto(self, row,numbers_as_float):

        timestamp=None
        try:
            timestamp = datetime.datetime.strptime(str(row[_date_time]), '%Y-%m-%d %H:%M:%S.%f')
        except Exception as e:
            timestamp = datetime.datetime.strptime(str(row[_date_time]), '%Y-%m-%d %H:%M:%S')

        if numbers_as_float:
            md = market_data_dto(p_tradecontract=row[_symbol],
                                 p_datetime=timestamp,
                                 p_last_px=float(row[_last_px]),
                                 p_last_qty=float(row[_last_qty]),
                                 p_bid_px=float(row[_best_bid_px]),
                                 p_offer_px=float(row[_best_ask_px]),
                                 p_bid_qty=float(row[_bid_qty]),
                                 p_offer_qty=float(row[_offer_qty]),
                                 p_bid_px_2=float(row[_bid_px_2]),
                                 p_offer_px_2=float(row[_offer_px_2]),


                                 )
        else:
                md = market_data_dto(p_tradecontract= row[_symbol],
                                    p_datetime=timestamp,
                                    p_last_px=row[_last_px],
                                    p_last_qty=row[_last_qty],
                                    p_bid_px=row[_best_bid_px],
                                    p_offer_px=row[_best_ask_px],
                                    p_bid_qty = (row[_bid_qty]),
                                    p_offer_qty = (row[_offer_qty]),
                                    p_bid_px_2 = (row[_bid_px_2]),
                                    p_offer_px_2 = (row[_offer_px_2])
                                    )

        return md

    #endregion

    #region Public Methods

    def get_market_data(self,symbol, from_date,to_date,numbers_as_float=False):
        market_data=[]
        with self.connection.cursor() as cursor:
            params = (symbol, from_date,to_date)
            cursor.execute("{CALL GetMarketData (?,?,?)}", params)

            for row in cursor:

                md = self.build_market_data_dto(row,numbers_as_float)
                market_data.append(md)

        return market_data

    def parsist_market_data(self,market_data):

        with self.connection.cursor() as cursor:
            params = (market_data.tradecontract,
                      market_data.get_last_date_time(),
                      market_data.get_last_last_px(),
                      market_data.get_last_last_qty(),
                      market_data.get_last_bid_px(),
                      market_data.get_last_offer_px(),
                      market_data.get_last_bid_qty(),
                      market_data.get_last_p_offer_qty(),
                      market_data.get_last_bid_px_2(),
                      market_data.get_last_offer_px_2(),
                      )
            cursor.execute("{CALL PersistMarketData (?,?,?,?,?,?,?,?,?,?)}", params)
            self.connection.commit()



    #endregion

