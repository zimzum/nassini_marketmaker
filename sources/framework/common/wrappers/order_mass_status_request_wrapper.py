from sources.framework.common.enums.Actions import Actions
from sources.framework.common.wrappers.wrapper import Wrapper


class OrderMassStatusRequestWrapper(Wrapper):

    def __init__(self):
        pass

    def GetAction(self):
        return Actions.ORDER_MASS_STATUS_REQUEST

    def GetField(self, field):

        if field == None:
            return None