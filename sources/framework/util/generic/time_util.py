import datetime


class time_util:
    def __init__(self):
        pass

    @staticmethod
    def full_now():
        return datetime.datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S.%f')[:-3]