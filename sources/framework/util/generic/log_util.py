import datetime
import os
import sys


class log_util:
    def __init__(self):
        pass

    @staticmethod
    def log_line():
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        return"ERROR - type={} name={} line={}".format(exc_type, fname, exc_tb.tb_lineno)

