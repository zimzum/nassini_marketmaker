from sources.framework.business_entities.securities.security import Security
from sources.framework.common.enums.SettlType import SettlType
from sources.framework.common.enums.Side import Side
from sources.framework.common.enums.QuantityType import QuantityType
from sources.framework.common.enums.PriceType import PriceType
from sources.framework.common.enums.OrdStatus import OrdStatus
import datetime

class Order:

    def __init__(self, ClOrdId=None,OrigClOrdId=None,Security=None,
                 SettlType=None,Side=None,Exchange=None,OrdType=None,QuantityType=None,OrderQty=None,
                 PriceType=None,Price=None,StopPx=None,Currency=None,
                 TimeInForce=None,Account=None,OrdStatus=None,Broker=None,Strategy=None,MarketArrivalTime=None):

        #region Attributes
        self.ClOrdId=ClOrdId
        self.OrigClOrdId=OrigClOrdId
        self.OrderId=""
        self.Security=Security
        self.SettlType=SettlType
        self.Side=Side
        self.Exchange=Exchange
        self.OrdType=OrdType
        self.QuantityType=QuantityType
        self.OrderQty=OrderQty
        self.PriceType=PriceType
        self.Price=Price
        self.StopPx = StopPx
        self.Currency=Currency
        self.TimeInForce=TimeInForce
        self.Account=Account
        self.OrdStatus=OrdStatus
        self.RejReason=None
        self.Broker = Broker
        self.Strategy = Strategy
        self.MarketArrivalTime=MarketArrivalTime
        self.CreationTime= datetime.datetime.now()

        self.CumQty=0
        self.LvsQty=self.OrderQty
        self.AvgPx = None

        #endregion

    def IsOpenOrder(self):
        return ( self.OrdStatus==OrdStatus.New or self.OrdStatus==OrdStatus.PartiallyFilled
               or self.OrdStatus==OrdStatus.PendingNew or self.OrdStatus==OrdStatus.Replaced
               or self.OrdStatus==OrdStatus.PendingReplace or self.OrdStatus==OrdStatus.AcceptedForBidding)

    def Clone(self):
        new_order=Order()

        new_order.ClOrdId=self.ClOrdId
        new_order.OrigClOrdId=self.OrigClOrdId
        new_order.OrderId=self.OrderId
        new_order.Security=self.Security
        new_order.SettlType=self.SettlType
        new_order.Side=self.Side
        new_order.Exchange=self.Exchange
        new_order.OrdType=self.OrdType
        new_order.QuantityType=self.QuantityType
        new_order.OrderQty=self.LvsQty
        new_order.PriceType=self.PriceType
        new_order.Price=self.Price
        new_order.StopPx = self.StopPx
        new_order.Currency=self.Currency
        new_order.TimeInForce=self.TimeInForce
        new_order.Account=self.Account
        new_order.OrdStatus=self.OrdStatus
        new_order.RejReason=self.RejReason
        new_order.Broker = self.Broker
        new_order.Strategy = self.Strategy
        new_order.MarketArrivalTime=self.MarketArrivalTime
        new_order.CreationTime= datetime.datetime.now()

        new_order.CumQty=0
        new_order.LvsQty=self.LvsQty
        new_order.AvgPx = None

        return new_order

