import configparser
import decimal


class Configuration:
    def __init__(self, configFile):
        config = configparser.ConfigParser()
        config.read(configFile)

        self.PauseBeforeExecutionInSeconds = int( config['DEFAULT']['PAUSE_BEFORE_EXECUTION_IN_SECONDS'])
        self.CancelActiveOrdersOnStart = config['DEFAULT']['CANCEL_ACTIVE_ORDERS_ON_START']=="True"
        self.WaitUntilPreviousOrdersCancelSec = int(config['DEFAULT']['WAIT_UNTIL_PREVIOUS_ORDERS_CANCEL_SEC'])

        self.IncomingModule = config['DEFAULT']['INCOMING_MODULE']
        self.IncomingModuleConfigFile = config['DEFAULT']['INCOMING_CONFIG_FILE']

        self.OutgoingModule = config['DEFAULT']['OUTGOING_MODULE']
        self.OutgoingModuleConfigFile = config['DEFAULT']['OUTGOING_CONFIG_FILE']

        self.Symbol = config['DEFAULT']['SYMBOL']
        self.SecPosfixMDReq = config['DEFAULT']['SEC_POSFIX_MD_REQ'] if config['DEFAULT']['SEC_POSFIX_MD_REQ']!="" else None

        self.Account = config['DEFAULT']['ACCOUNT']

        self.Currency = config['DEFAULT']['CURRENCY']
        self.PeriodicPersist = config['DEFAULT']['PERIODIC_PERSIST']

        self.ConnectionString =  config['DEFAULT']['CONNECTION_STRING']

        self.MAX_LONG_QTY = float( config['DEFAULT']['MAX_LONG_QTY'])
        self.MAX_SHORT_QTY = float(config['DEFAULT']['MAX_SHORT_QTY'])
        self.DEFAULT_SPREAD = float( config['DEFAULT']['DEFAULT_SPREAD'])
        self.INVENTORY_SPREAD_UNIT = float( config['DEFAULT']['INVENTORY_SPREAD_UNIT'])
        self.TRADE_UNIT = float( config['DEFAULT']['TRADE_UNIT'])
        self.INV_THRESHOLD = float( config['DEFAULT']['INV_THRESHOLD'])
        self.IMPL_IMBALANCE = int( config['DEFAULT']['IMPL_IMBALANCE'])

        self.IMBALANCE_ENTRY_THRESHOLD =float( config['DEFAULT']['IMBALANCE_ENTRY_THRESHOLD'])
        self.IMBALANCE_EXIT_THRESHOLD =float( config['DEFAULT']['IMBALANCE_EXIT_THRESHOLD'])
        self.IMBALANCE_UNIT = float(config['DEFAULT']['IMBALANCE_UNIT'])
        self.IMBALANCE_PRICE_DELTA =float( config['DEFAULT']['IMBALANCE_PRICE_DELTA'])
        self.IMBALANCE_MIN_OPX_TIMESPAN = int( config['DEFAULT']['IMBALANCE_MIN_OPX_TIMESPAN'])
        self.IMBALANCE_DEPURATION_THRESHOLD =int( config['DEFAULT']['IMBALANCE_DEPURATION_THRESHOLD'])
        self.IMBALANCE_MIN_ENTRY_SEC = int(config['DEFAULT']['IMBALANCE_MIN_ENTRY_SEC'])
        self.SWITCHED_OFF_IMBALANCE_TIME = config['DEFAULT']['SWITCHED_OFF_IMBALANCE_TIME']

        self.ATR_IMBALANCE_SAFETY_MULT = int( config['DEFAULT']['ATR_IMBALANCE_SAFETY_MULT'])
        self.ATR_IMBALANCE_EXIT_LENGTH = int(config['DEFAULT']['ATR_IMBALANCE_EXIT_LENGTH'])

        self.IMPL_ROPE_PRICE_ON_REGULAR_MARKETS =int( config['DEFAULT']['IMPL_ROPE_PRICE_ON_REGULAR_MARKETS'])
        self.IMPL_CLOSING_TIME =int( config['DEFAULT']['IMPL_CLOSING_TIME'])
        self.IMPL_OPPOSITE_INVENTORY =int( config['DEFAULT']['IMPL_OPPOSITE_INVENTORY'])
        self.IMPL_STRONG_DIRECTION =int( config['DEFAULT']['IMPL_STRONG_DIRECTION'])
        self.IMPL_ATR_IMBALANCE_EXIT =int( config['DEFAULT']['IMPL_ATR_IMBALANCE_EXIT'])
        self.IMPL_BACKTEST = int(config['DEFAULT']['IMPL_BACKTEST'])
        self.EVALUATE_HIGH_LOWS =int( config['DEFAULT']['EVALUATE_HIGH_LOWS'])

        self.CLOSING_TIME = str(config['DEFAULT']['CLOSING_TIME'])
        self.MID_PRICE_CALCULATION = int( config['DEFAULT']['MID_PRICE_CALCULATION'])
        self.TICK_SIZE = float(config['DEFAULT']['TICK_SIZE'])
        self.NEW_MAX_MIN_START_COUNT = int( config['DEFAULT']['NEW_MAX_MIN_START_COUNT'])
        self.NEW_MAX_MIN_END_COUNT = int(config['DEFAULT']['NEW_MAX_MIN_END_COUNT'])

        self.ELASTIC_ROPE_PCT = float( config['DEFAULT']['ELASTIC_ROPE_PCT'])


