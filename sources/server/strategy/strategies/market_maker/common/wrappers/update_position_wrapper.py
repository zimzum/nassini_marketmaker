from sources.framework.common.enums.Actions import Actions
from sources.framework.common.enums.fields.order_field import OrderField
from sources.framework.common.enums.fields.position_field import PositionField
from sources.framework.common.wrappers.wrapper import Wrapper


class UpdatePositionWrapper(Wrapper):
    def __init__(self, pOrigPositionId,pPositionId,pPrice):
        self.OrigPositionId = pOrigPositionId
        self.PositionId = pPositionId
        self.Price=pPrice

    def GetAction(self):
        return Actions.UPDATE_POSITION

    def GetField(self, field):
        if field is None:
            return None

        if field == PositionField.OrigPosId:
            return self.OrigPositionId
        elif field == PositionField.PosId:
            return self.PositionId
        elif field == PositionField.OrderPrice:
            return self.Price
        else:
            return None