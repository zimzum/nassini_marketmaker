from datetime import datetime


class CancelReplaceDTO:

    def __init__(self,OrigClOrdId,ClOrdId,spotTimespan):
        self.OrigClOrdId=OrigClOrdId
        self.ClOrdId=ClOrdId
        self.SpotTimespan=spotTimespan#used by backtesters
        self.RealTimespan=datetime.now()
