import datetime

from sources.server.strategy.strategies.market_maker.common.enums.backtest_period import backtest_period


class calendar_helper():

    #region Constructors

    def __init__(self):
        pass
    #endregion

    #region Static Methods

    @staticmethod
    def eval_new_period(configuration,curr_date,prev_date):

        if(prev_date is None):
            return False

        if configuration.PeriodicPersist == backtest_period.Minute.value:
            return calendar_helper.eval_new_minute(curr_date, prev_date)
        elif configuration.PeriodicPersist == backtest_period.Hourly.value:
            return calendar_helper.eval_new_hour(curr_date, prev_date)
        elif configuration.PeriodicPersist == backtest_period.Daily.value:
            return calendar_helper.eval_new_day(curr_date, prev_date)
        else:
            raise Exception("ERROR: Could not process period unit @eval_new_period :{}".format( configuration.PeriodicPersist))

    @staticmethod
    def get_start(configuration,date):
        if configuration.PeriodicPersist == backtest_period.Minute.value:
            return calendar_helper.implement_first_second(date)
        elif configuration.PeriodicPersist == backtest_period.Hourly.value:
            return calendar_helper.implement_first_minute(date)
        elif configuration.PeriodicPersist == backtest_period.Daily.value:
            return calendar_helper.implement_first_hour(date)
        else:
            raise Exception("ERROR: Could not process period unit @get_start :{}".format(configuration.PeriodicPersist))

    @staticmethod
    def get_end(configuration,date):
        if configuration.PeriodicPersist == backtest_period.Minute.value:
            return calendar_helper.implement_last_second(date)
        elif configuration.PeriodicPersist == backtest_period.Hourly.value:
            return calendar_helper.implement_last_minute(date)
        elif configuration.PeriodicPersist == backtest_period.Daily.value:
            return calendar_helper.implement_last_hour(date)
        else:
            raise Exception("ERROR: Could not process period unit @get_end :{}".format(configuration.PeriodicPersist))

    @staticmethod
    def eval_new_day(curr_date, prev_date):
        if prev_date is not None and curr_date is not None:
            return  (curr_date.day != prev_date.day)
        else:
            return False


    @staticmethod
    def eval_new_hour(curr_hour,prev_hour):
        return (curr_hour.day == prev_hour.day and curr_hour.hour != prev_hour.hour)

    @staticmethod
    def eval_new_minute(curr_minute, prev_minute):
        return ( curr_minute.minute != prev_minute.minute)

    @staticmethod
    def implement_first_hour(date):
        return datetime.datetime(year=date.year, month=date.month, day=date.day, hour=0, minute=0, second=0)

    @staticmethod
    def implement_last_hour(date):
        return datetime.datetime(year=date.year, month=date.month, day=date.day, hour=23, minute=59, second=59)

    @staticmethod
    def implement_first_minute(date):
        return datetime.datetime(year=date.year, month=date.month,day=date.day, hour=date.hour,minute=0, second=0)


    @staticmethod
    def implement_last_minute(date):
        return datetime.datetime(year=date.year, month=date.month,day=date.day, hour=date.hour,minute=59, second=59)

    @staticmethod
    def implement_first_second(date):
        return datetime.datetime(year=date.year, month=date.month,day=date.day, hour=date.hour,minute=date.minute, second=0)


    @staticmethod
    def implement_last_second(date):
        return datetime.datetime(year=date.year, month=date.month,day=date.day, hour=date.hour,minute=date.minute, second=59)

    #endregion