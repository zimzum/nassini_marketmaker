from sources.framework.common.logger.message_type import MessageType
from sources.server.strategy.strategies.market_maker.common.util.calendar_helper import calendar_helper


class NewDayHelper():

    #region Constructors

    def __init__(self,configuration,logger):
        self.Configuration=configuration
        self.SimpleLogger=logger

    #endregion

    #region Public Methods

    def eval_new_day(self, curr_md, prev_md):

        if prev_md != None and calendar_helper.eval_new_day(curr_md.MDEntryDate, prev_md.MDEntryDate):
            return True

        else:
            return False

    def eval_new_hour(self, curr_md, prev_md):

        if prev_md != None and calendar_helper.eval_new_hour(curr_md.MDEntryDate, prev_md.MDEntryDate):
            return True
        else:
            return False


    def eval_new_minute(self, curr_md, prev_md):

        if prev_md != None and calendar_helper.eval_new_minute(curr_md.MDEntryDate, prev_md.MDEntryDate):
           return True
        else:
            return False



    #endregion