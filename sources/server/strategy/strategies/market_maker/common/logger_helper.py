from datetime import datetime

from sources.framework.common.logger.message_type import MessageType


class LoggerHelper:

    def __init__(self):
        pass
        #TODO: implement logging initialization


    #region Public Methods

    def log(self,msg):
        pass
        #TODO: implement logging

    #endregion


    #region Public Static Methods

    @staticmethod
    def LogPublishMarketDataOnSecurity(sender,logger,symbol,md):
        logger.DoLog("At {}: {}-Publishing market data for symbol {}: MDEntryDate={} Trade={} LastTradeSize={} BestBid={} BestBidSize={} BestAsk={} BestAskSize={} ".format(
            sender,
            datetime.now(),
            symbol,
            md.MDEntryDate if md.MDEntryDate is not None else "?",
            md.Trade if md.Trade is not None else "-",
            md.MDTradeSize if md.MDTradeSize is not None else "-",
            md.BestBidPrice if md.BestBidPrice is not None else "-",
            md.BestBidSize if md.BestBidSize is not None else "-",
            md.BestAskPrice if md.BestAskPrice is not None else "-",
            md.BestAskSize if md.BestAskSize is not None else "-",
            ),
            MessageType.DEBUG)

    #endregion