import pyodbc
import datetime

class MarketMakingSummaryManager():

    #region Constructors

    def __init__(self, connString):
        self.i = 0
        self.connection = pyodbc.connect(connString)
    #endregion

    #region Public Methods

    def parsist_market_making_summary(self,summary):

        #print("before:{}".format(datetime.datetime.now()))

        with self.connection.cursor() as cursor:
            params = (summary.date,
                      str(summary.type),
                      summary.backtest.id if summary.backtest is not None else None,
                      summary.start,
                      summary.end,
                      summary.profit,
                      summary.net_buys,
                      summary.net_sells,
                      summary.net_shares,
                      summary.imbalance_activated,
                      summary.imbalance_start,
                      summary.imbalance_end,
                      summary.nominal_volatility,
                      summary.imbalance_bid,
                      summary.imbalance_ask,
                      summary.market_price,
                      summary.new_high_lows_start,
                      summary.new_high_lows_end,
                      summary.new_high_lows_type,
                      summary.reggr_slope,
                      summary.candle_breadth
                      )
            cursor.execute("{CALL PersistMarketMakerExecutionSummary (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}", params)
            self.connection.commit()

        #print("after:{}".format(datetime.datetime.now()))

    #endregion
