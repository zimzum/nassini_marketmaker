import queue
import threading
import time
import traceback
import uuid
from datetime import datetime
from winsound import Beep

from sources.framework.business_entities.positions.execution_summary import ExecutionSummary, Side
from sources.framework.business_entities.positions.position import Position, OrdType
from sources.framework.common.abstract.base_communication_module import BaseCommunicationModule
from sources.framework.common.converters.execution_report_converter import ExecutionReportConverter, \
    ExecutionReportField
from sources.framework.common.dto.cm_state import CMState
from sources.framework.common.enums.Actions import Actions
from sources.framework.common.enums.OrdStatus import OrdStatus
from sources.framework.common.enums.PriceType import PriceType
from sources.framework.common.enums.QuantityType import QuantityType
from sources.framework.common.interfaces.icommunication_module import ICommunicationModule
from sources.framework.common.logger.message_type import MessageType
from sources.framework.common.wrappers.cancel_order_wrapper import CancelOrderWrapper
from sources.framework.common.wrappers.error_wrapper import ErrorWrapper
from sources.framework.common.wrappers.market_data_request_wrapper import MarketDataRequestWrapper, \
    SubscriptionRequestType
from sources.framework.common.wrappers.order_mass_status_request_wrapper import OrderMassStatusRequestWrapper
from sources.server.strategy.common.wrappers.position_wrapper import PositionWrapper
from sources.server.strategy.strategies.day_trader.common.converters.market_data_converter import MarketDataConverter, \
    Security, SecurityType
from sources.server.strategy.strategies.day_trader.common.util.log_helper import LogHelper
from sources.server.strategy.strategies.market_maker.business_entities.market_making_position import \
    MarketMakingPosition
from sources.server.strategy.strategies.market_maker.common.configuration.configuration import Configuration
from sources.server.strategy.strategies.market_maker.common.logger_helper import LoggerHelper
from sources.server.strategy.strategies.market_maker.common.util.calendar_helper import calendar_helper
from sources.server.strategy.strategies.market_maker.common.wrappers.update_position_wrapper import \
    UpdatePositionWrapper
from sources.server.strategy.strategies.market_maker.data_access_layer.market_making_summary_manager import \
    MarketMakingSummaryManager


class MarketMaker(BaseCommunicationModule, ICommunicationModule):

    def __init__(self):

        self.RoutingLock = threading.Lock()
        self.MarketDataLock = threading.Lock()

        self.Configuration = None

        self.Positions = {}

        self.InvokingModule = None
        self.OutgoingModule = None

        self.FailureException = None
        self.ServiceFailure = False
        self.PositionsSynchronization = True

        self.MarketMakingSummaryManager=None

        self.MarketMakingPositions={}
        self.PositionSecurities={}

        self.SummariesQueue = queue.Queue(maxsize=1000000)
        self.StartTime = None

    # region Util

    def ProcessCriticalError(self, exception, msg):
        self.FailureException = exception
        self.ServiceFailure = True
        self.DoLog(msg, MessageType.ERROR)

    def ProcessErrorInMethod(self, method, e, symbol=None):
        try:
            msg = "Error @{} for security {}:{} ".format(method, symbol if symbol is not None else "-", str(e))
            #error = ErrorWrapper(Exception(msg))
            #self.ProcessError(error)
            self.DoLog(msg, MessageType.ERROR)
        except Exception as e:
            self.DoLog("Critical error @MarketMaker.ProcessErrorInMethod: " + str(e), MessageType.ERROR)

    def LoadConfig(self):
        self.Configuration = Configuration(self.ModuleConfigFile)
        return True

    def EvalOrderMassStatusRequest(self):

        if(self.Configuration.CancelActiveOrdersOnStart):
            wrapper = OrderMassStatusRequestWrapper()
            self.OrderRoutingModule.ProcessMessage(wrapper)

    def LoadMarketMakingPositions(self):

        marketPos = MarketMakingPosition(self.GetOutgoingSymbol(),
                                         max_long_qty=self.Configuration.MAX_LONG_QTY,
                                         max_short_qty=self.Configuration.MAX_SHORT_QTY,
                                         default_spread=self.Configuration.DEFAULT_SPREAD,
                                         inventory_spread_unit=self.Configuration.INVENTORY_SPREAD_UNIT,
                                         trade_unit=self.Configuration.TRADE_UNIT,
                                         inv_threshold=self.Configuration.INV_THRESHOLD,
                                         impl_imbalance=self.Configuration.IMPL_IMBALANCE,
                                         imbalance_min_threshold=self.Configuration.IMBALANCE_ENTRY_THRESHOLD,
                                         imbalance_exit_threshold=self.Configuration.IMBALANCE_EXIT_THRESHOLD,
                                         imbalance_unit=self.Configuration.IMBALANCE_UNIT,
                                         imbalance_price_delta=self.Configuration.IMBALANCE_PRICE_DELTA,
                                         imbalance_min_opx_timespan=self.Configuration.IMBALANCE_MIN_OPX_TIMESPAN,
                                         imbalance_depuration_threshold=self.Configuration.IMBALANCE_DEPURATION_THRESHOLD,
                                         imbalance_min_entry_sec=self.Configuration.IMBALANCE_MIN_ENTRY_SEC,
                                         switched_off_imbalance_time=self.Configuration.SWITCHED_OFF_IMBALANCE_TIME,
                                         impl_rope_price_on_regular_markets=self.Configuration.IMPL_ROPE_PRICE_ON_REGULAR_MARKETS,
                                         impl_closing_time=self.Configuration.IMPL_CLOSING_TIME,
                                         impl_opposite_inventory=self.Configuration.IMPL_OPPOSITE_INVENTORY,
                                         impl_strong_direction=self.Configuration.IMPL_STRONG_DIRECTION,
                                         closing_time=self.Configuration.CLOSING_TIME,
                                         mid_price_calculation=self.Configuration.MID_PRICE_CALCULATION,
                                         tick_size=self.Configuration.TICK_SIZE,
                                         new_maxmin_start_count=self.Configuration.NEW_MAX_MIN_START_COUNT,
                                         new_max_min_end_count=self.Configuration.NEW_MAX_MIN_END_COUNT,
                                         elastic_rope_pct=self.Configuration.ELASTIC_ROPE_PCT,
                                         impl_ATR_imbalance_exit=self.Configuration.IMPL_ATR_IMBALANCE_EXIT,
                                         ATR_imbalance_exit_length=self.Configuration.ATR_IMBALANCE_EXIT_LENGTH,
                                         ATR_imbalance_safety_mult=self.Configuration.ATR_IMBALANCE_SAFETY_MULT,
                                         evaluate_high_lows=self.Configuration.EVALUATE_HIGH_LOWS,
                                         impl_backtest=self.Configuration.IMPL_BACKTEST)


        self.MarketMakingPositions[self.Configuration.Symbol]= marketPos
        self.MarketMakingPositions[self.Configuration.Symbol].start(self)

    def GetOutgoingSymbol(self):
        sent_symbol = self.Configuration.Symbol

        if self.Configuration.SecPosfixMDReq is not None:
            sent_symbol += "." + self.Configuration.SecPosfixMDReq

        return sent_symbol



    def SubscribeMarketDataThread(self):
        try:

            self.DoLog("Sending Market Data Request for symbol {}".format(self.Configuration.Symbol),MessageType.INFO)

            sec = Security(Symbol=self.GetOutgoingSymbol(),
                           Exchange=None,
                           SecType=SecurityType.OTH,
                           Currency=self.Configuration.Currency)

            mdrw = MarketDataRequestWrapper(sec,SubscriptionRequestType.SnapshotAndUpdates)

            self.OrderRoutingModule.ProcessMessage(mdrw)

        except Exception as e:
            self.DoLog("Critical error @MarketMaker.SubscribeMarketDataThread: " + str(e), MessageType.ERROR)

    def LoadManagers(self):
        self.MarketMakingSummaryManager = MarketMakingSummaryManager(self.Configuration.ConnectionString)

    #endregion

    #region Thread Methods

    def SummaryPersistanceThread(self):
        while True:

            while not self.SummariesQueue.empty():
                try:

                    summary = self.SummariesQueue.get()
                    self.MarketMakingSummaryManager.parsist_market_making_summary(summary)

                except Exception as e:
                    self.DoLog("Error summaries Trades to DB: {}".format(e), MessageType.ERROR)

            time.sleep(2)

    def OnBuy(self,portPos,posId,date,symbol,qty,price):
        try:
            self.DoRoute(portPos, posId, date, symbol, Side.Buy, qty, price)
        except Exception as e:
            traceback.print_exc()
            self.ProcessErrorInMethod("@MarketMaker.OnBuy", e,symbol)

    def OnSell(self,portPos,posId,date,symbol,qty,price):
        try:
            self.DoRoute(portPos, posId, date, symbol, Side.Sell, qty, price)
        except Exception as e:
            traceback.print_exc()
            self.ProcessErrorInMethod("@MarketMaker.OnSell", e, symbol)

    def OnCancelReplace(self,portPos,date,symbol, origPosId,posId,side,qty,refPrice):
        try:

            self.DoLog("Updating Position {} for symbol {} with Price {} (new PosId {})".format(origPosId,symbol,refPrice,posId),MessageType.INFO)

            oldPos = self.PositionSecurities[origPosId]

            if(oldPos==None):
                raise Exception("Could not find position security for posId {}".format(origPosId))

            routePos = Position(PosId=posId,
                                Security=Security(Symbol=symbol, Exchange=None, SecType=SecurityType.CS),
                                Side=side, PriceType=PriceType.FixedAmount, Qty=qty,
                                QuantityType=QuantityType.SHARES,
                                Account=self.Configuration.Account, Broker=None, Strategy=None,
                                OrderType=OrdType.Market if refPrice is None else OrdType.Limit,
                                OrderPrice=refPrice)

            routePos.ValidateNewPosition()
            portPos.ExecutionSummaries[routePos.PosId] = ExecutionSummary(date, routePos)

            self.PositionSecurities[posId]=portPos

            wrapper = UpdatePositionWrapper(origPosId,posId,refPrice)
            self.OrderRoutingModule.ProcessMessage(wrapper)
        except Exception as e:
            traceback.print_exc()
            self.ProcessErrorInMethod("@MarketMaker.OnCancelReplace", e, symbol)

    def UpdateManagedPosExecutionSummary(self,potPos, summary, execReport):

        summary.UpdateStatus(execReport)

        if summary.Position.IsFinishedPosition():
            LogHelper.LogPositionUpdate(self, "Managed Position Finished", summary, execReport)
        else:
            LogHelper.LogPositionUpdate(self, "Managed Position Updated", summary, execReport)

        potPos.on_execution_report(execReport, summary)
        #self.SummariesQueue.put(summary)

    def EvalCancellingOrdersOnStartup(self,execReport=None):
        if self.Configuration.CancelActiveOrdersOnStart \
                and (execReport is None or execReport.IsOpenOrder() ):
            span = datetime.now() - self.StartTime

            if span.seconds<self.Configuration.WaitUntilPreviousOrdersCancelSec:
                if(execReport is not None):
                    self.DoLog("Cancelling order {} on startup".format(execReport.Order.ClOrdId),MessageType.INFO)
                    cxlOrder = CancelOrderWrapper(execReport.Order.ClOrdId,execReport.Order.OrderId)
                    self.OrderRoutingModule.ProcessMessage(cxlOrder)
                #We wait until elapsed is long enough

            else:
                self.Configuration.CancelActiveOrdersOnStart=False


    def ProcessExecutionReport(self, wrapper):
        try:

            try:
                execReport = ExecutionReportConverter.ConvertExecutionReport(wrapper)
            except Exception as e:
                self.DoLog("Discarding execution report with bad data: " + str(e), MessageType.INFO)
                return

            self.EvalCancellingOrdersOnStartup(execReport)

            pos_id = wrapper.GetField(ExecutionReportField.PosId)

            if pos_id is not None:

                self.RoutingLock.acquire(blocking=True)

                if pos_id in self.PositionSecurities:
                    potPos = self.PositionSecurities[pos_id]
                    summary = potPos.ExecutionSummaries[pos_id]
                    self.UpdateManagedPosExecutionSummary(potPos,summary,execReport)
                    return CMState.BuildSuccess(self)
                else:
                    self.DoLog("Received execution report for unknown PosId (ClOrdId) {}".format(pos_id), MessageType.INFO)
            else:
                raise Exception("Received execution report without PosId (ClOrdId) ")
        except Exception as e:
            traceback.print_exc()
            self.DoLog("Critical error @MarketMaker.ProcessExecutionReport: " + str(e), MessageType.ERROR)
            return CMState.BuildFailure(self, Exception=e)
        finally:
            if self.RoutingLock.locked():
                self.RoutingLock.release()

    def ProcessMarketData(self,wrapper):

        md=None
        try:
            self.EvalCancellingOrdersOnStartup()
            if self.Configuration.CancelActiveOrdersOnStart:
                self.DoLog("MD Received - Waiting from previous orders to be cancelled",MessageType.INFO)
                return

            self.MarketDataLock.acquire(blocking=True)
            md = MarketDataConverter.ConvertMarketData(wrapper)
            LoggerHelper.LogPublishMarketDataOnSecurity("Market Maker Recv @MarketData", self, md.Security.Symbol, md)

            if md.Security.Symbol in self.MarketMakingPositions:

                if calendar_helper.eval_new_period(self.Configuration, md.MDEntryDate, self.MarketMakingPositions[ md.Security.Symbol].get_last_period_date()):
                    summary = self.MarketMakingPositions[md.Security.Symbol].get_strategy_summary(self.Configuration)
                    self.SummariesQueue.put(summary)
                    new_period = calendar_helper.get_start(self.Configuration, md.MDEntryDate)
                    self.MarketMakingPositions[md.Security.Symbol].set_last_period_date(new_period)

                if(calendar_helper.eval_new_day(md.MDEntryDate,self.MarketMakingPositions[md.Security.Symbol].get_now())):
                    self.MarketMakingPositions[md.Security.Symbol].start(self)

                self.MarketMakingPositions[md.Security.Symbol].set_now(md.MDEntryDate)
                self.MarketMakingPositions[md.Security.Symbol].on_market_data(md)

            else:
                self.DoLog("Ignoring market data for not tracked position {}".format(md.Security.Symbol),MessageType.INFO)
        except Exception as e:
            traceback.print_exc()
            self.ProcessErrorInMethod("@MarketData.ProcessMarketData", e,md.Security.Symbol if md is not None else None)
        finally:
            if self.MarketDataLock.locked():
                self.MarketDataLock.release()

    #endregion

    #region Private Methods

    def DoRoute(self,portfPos,posId,date,symbol,side,qty,price):
        routePos = Position(PosId=posId,
                            Security=Security(Symbol=symbol, Exchange=None,SecType=SecurityType.CS),
                            Side=side, PriceType=PriceType.FixedAmount, Qty=qty,
                            QuantityType=QuantityType.SHARES,
                            Account=self.Configuration.Account, Broker=None, Strategy=None,
                            OrderType=OrdType.Market if price is None else OrdType.Limit,
                            OrderPrice=price)

        routePos.ValidateNewPosition()
        portfPos.ExecutionSummaries[routePos.PosId] = ExecutionSummary(date, routePos)

        self.PositionSecurities[routePos.PosId] = portfPos

        self.DoLog("Routing position for Symbol {} date {} side {} and price {}".format(symbol, date, side, price),
                   MessageType.INFO)
        posWrapper = PositionWrapper(routePos)
        self.OrderRoutingModule.ProcessMessage(posWrapper)

        self.DoLog("Persisting position for Symbol {} date {} side {} and price {}".format(symbol, date, side, price),
                   MessageType.INFO)

    #endregion

    #region Public Methods

    def ProcessMessage(self, wrapper):
        try:
            raise Exception("MarketMaker.ProcessMessage: Not prepared for routing message {}".format(wrapper.GetAction()))
        except Exception as e:
            self.DoLog("Critical error @MarketMaker.ProcessMessage: " + str(e), MessageType.ERROR)
            return CMState.BuildFailure(self, Exception=e)

    def ProcessIncoming(self, wrapper):
        try:

            if wrapper.GetAction() == Actions.MARKET_DATA:
                threading.Thread(target=self.ProcessMarketData, args=(wrapper,)).start()
                return CMState.BuildSuccess(self)
            elif wrapper.GetAction() == Actions.ERROR:
                threading.Thread(target=self.ProcessError, args=(wrapper,)).start()
            else:
                raise Exception("ProcessIncoming: Not prepared for routing message {}".format(wrapper.GetAction()))
        except Exception as e:
            self.DoLog("Critical error @DayTrader.ProcessIncoming: " + str(e), MessageType.ERROR)
            return CMState.BuildFailure(self, Exception=e)

    def ProcessOutgoing(self, wrapper):
        try:
            if wrapper.GetAction() == Actions.EXECUTION_REPORT:

                threading.Thread(target=self.ProcessExecutionReport, args=(wrapper,)).start()
                return CMState.BuildSuccess(self)
            elif wrapper.GetAction() == Actions.POSITION_LIST:
                #threading.Thread(target=self.ProcessPositionList, args=(wrapper,)).start()
                return CMState.BuildSuccess(self)
            elif wrapper.GetAction() == Actions.ORDER_CANCEL_REJECT:
                #threading.Thread(target=self.ProcessOrderCancelReject, args=(wrapper,)).start()
                return CMState.BuildSuccess(self)
            elif wrapper.GetAction() == Actions.ERROR:
                #threading.Thread(target=self.ProcessError, args=(wrapper,)).start()
                return CMState.BuildSuccess(self)
            else:
                raise Exception("ProcessOutgoing: Not prepared for routing message {}".format(wrapper.GetAction()))
        except Exception as e:
            self.DoLog("Critical error @MarketMaker.ProcessOutgoing: " + str(e), MessageType.ERROR)
            return CMState.BuildFailure(self, Exception=e)

    def Initialize(self, pInvokingModule, pConfigFile):
        self.ModuleConfigFile = pConfigFile
        self.InvokingModule = pInvokingModule
        self.DoLog("MarketMaker  Initializing", MessageType.INFO)

        if self.LoadConfig():

            self.PositionsSynchronization = False

            self.StartTime=datetime.now()

            self.LoadManagers()

            self.LoadMarketMakingPositions()

            self.MarketDataModule = self.InitializeModule(self.Configuration.IncomingModule,
                                                          self.Configuration.IncomingModuleConfigFile)

            self.OrderRoutingModule = self.InitializeModule(self.Configuration.OutgoingModule,
                                                            self.Configuration.OutgoingModuleConfigFile)

            time.sleep(self.Configuration.PauseBeforeExecutionInSeconds)

            threading.Thread(target=self.SummaryPersistanceThread, args=()).start()

            threading.Thread(target=self.SubscribeMarketDataThread, args=()).start()

            self.EvalOrderMassStatusRequest()

            self.DoLog("MarketMaker Successfully initialized", MessageType.INFO)

            return CMState.BuildSuccess(self)

        else:
            msg = "Error initializing MarketMaker"
            self.DoLog(msg, MessageType.ERROR)
            return CMState.BuildFailure(self, errorMsg=msg)

    #endregion



