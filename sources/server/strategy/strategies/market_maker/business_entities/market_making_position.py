import datetime
import math
import threading
import traceback
import uuid
import winsound
from time import time

from sources.framework.common.enums.PositionsStatus import PositionStatus
from sources.framework.common.enums.Side import Side
from sources.framework.common.logger.message_type import MessageType
from sources.framework.util.generic.time_util import time_util
from sources.server.strategy.common.dto.strategy_summary import strategy_summary
from sources.server.strategy.strategies.market_maker.business_entities.imbalance import Imbalance
from sources.server.strategy.strategies.market_maker.business_entities.mid_price_logic import MidPriceLogic
from sources.server.strategy.strategies.market_maker.business_entities.order_price_logic import OrderPriceLogic
from sources.server.strategy.strategies.market_maker.common.dto.cancel_replace_dto import CancelReplaceDTO
from sources.server.strategy.strategies.market_maker.common.logger_helper import LoggerHelper
from sources.server.strategy.strategies.market_maker.common.util.calendar_helper import calendar_helper

_SIDE_BID="BID"
_SIDE_ASK="ASK"
class MarketMakingPosition():

    # region Constructor

    def do_buy(self,symbol ,qty, price):
        self.routing_buy_order_id = str(uuid.uuid4())
        self.last_buy_order_sent = self.get_now()
        self.parent.OnBuy(self, self.routing_buy_order_id, self.get_now(), symbol, qty, price)

        self.orders[self.routing_buy_order_id] = self.routing_buy_order_id
        self.sent_orders[self.routing_buy_order_id] = self.routing_buy_order_id

        self.routing_long += qty
        self.log(
            "<buy>-{}-Sending buy order {} for qty {} price {}".format(self.get_now(), self.routing_buy_order_id, qty,
                                                                       round(price, 2)))

    # endregion

    # region Routing Methods

    def do_sell(self,symbol, qty, price):
        self.routing_sell_order_id = str(uuid.uuid4())
        self.last_sell_order_sent = self.get_now()
        self.parent.OnSell(self, self.routing_sell_order_id, self.get_now(), symbol, qty, price)

        self.orders[self.routing_sell_order_id] = self.routing_sell_order_id
        self.sent_orders[self.routing_sell_order_id] = self.routing_sell_order_id

        self.routing_short += qty * -1
        self.log(
            "<sell>-{}-Sending sell order {} for qty {} price {}".format(self.get_now(), self.routing_sell_order_id,
                                                                         qty, round(price, 2)))

    def __init__(self, symbol=None, max_long_qty=20, max_short_qty=-20, default_spread=100, inventory_spread_unit=300,
                 trade_unit=5,
                 inv_threshold=1, impl_imbalance=1, imbalance_min_threshold=0.75, imbalance_exit_threshold=0.6,
                 imbalance_unit=0.05,
                 imbalance_price_delta=500, imbalance_min_opx_timespan=15, imbalance_depuration_threshold=30,
                 imbalance_min_entry_sec=600,switched_off_imbalance_time="07:40 PM",
                 impl_rope_price_on_regular_markets=0,
                 impl_closing_time=1, impl_opposite_inventory=1, impl_strong_direction=1, closing_time="11:30 PM",
                 mid_price_calculation=4, tick_size=5,
                 new_maxmin_start_count=20, new_max_min_end_count=10, elastic_rope_pct=0.5,
                 strong_dir_pacing_sec=3,
                 impl_ATR_imbalance_exit=0,
                 ATR_imbalance_exit_length=20, ATR_imbalance_safety_mult=4, evaluate_high_lows=0,impl_backtest=0):
        self.name = "Market Maker3"
        self.symbol = symbol

        # region Config Parameters
        self.max_long_qty = max_long_qty
        self.max_short_qty = max_short_qty
        self.trade_unit = trade_unit

        self.impl_imbalance = impl_imbalance
        self.impl_backtest = impl_backtest==1

        self.ExecutionSummaries = {}

        imbalance = Imbalance(imbalance_min_threshold, imbalance_exit_threshold, imbalance_unit,
                                   imbalance_price_delta,
                                   imbalance_min_opx_timespan, imbalance_depuration_threshold,
                                   imbalance_min_entry_sec,switched_off_imbalance_time,
                                   ATR_imbalance_exit_length,
                                   ATR_imbalance_safety_mult, impl_ATR_imbalance_exit)

        self.price_logic = OrderPriceLogic(imbalance,tick_size, self, impl_opposite_inventory,
                                           impl_strong_direction,
                                           impl_rope_price_on_regular_markets, default_spread, inv_threshold,
                                           inventory_spread_unit,
                                           elastic_rope_pct,self)

        # TODO - low priority: Encapsulate New High/Low logic
        self.new_maxmin_start_count = new_maxmin_start_count
        self.new_maxmin_end_count = new_max_min_end_count
        self.evaluate_high_lows = evaluate_high_lows==1

        self.impl_closing_time = impl_closing_time
        self.closing_time = closing_time

        self.strong_dir_pacing_sec=strong_dir_pacing_sec
        self.midPriceLogic = MidPriceLogic(mid_price_calculation, default_spread, tick_size, self.price_logic.imbalance)

        # self.logger= LoggerHelper()

    def do_cancel_replace(self, new_price, order_id, isBuy=True):

        if order_id in self.sent_orders:
            return

        replacing_order = next((x for x in self.replaced_orders.values() if x.OrigClOrdId == order_id), None)

        if replacing_order is not None:
            return  # order_id is being replaced

        if order_id in self.replaced_orders:
            self.log("Trying to replace order id {} which hasn't yet arrived to the exchange".format(order_id))
            return  # order id not yet arrived to the exchange

        if order_id in self.orders:
            order = self.orders[order_id]

            new_order_id = str(uuid.uuid4())  # ClOrdId

            self.log("<replace> {} - Replace {} order with OrigClOrdId {} with new price {} (ClOrdId={})"
                    .format(time_util.full_now(),"buy" if isBuy else "sell", order_id, round(new_price, 2),
                            new_order_id))

            # We dont update the routing_buy/sell_order id until it has been accepted
            self.orders[new_order_id] = new_order_id
            self.sent_orders[new_order_id] = new_order_id
            self.replaced_orders[new_order_id] = CancelReplaceDTO(order_id, new_order_id, self.get_now())

            qty=self.routing_long if isBuy else self.routing_short

            if isBuy:
                self.last_buy_order_sent = self.get_now()
            else:
                self.last_sell_order_sent = self.get_now()


            self.parent.OnCancelReplace(self,self.get_now(), self.symbol, order_id, new_order_id,
                                        Side.Buy if isBuy else Side.Sell,qty, new_price)

        else:
            self.do_halt=True
            winsound.Beep(2500, 1000)
            self.log("ERROR!: new replace order with client_order_id={} did not receive a server id!".format(order_id))

    def eval_coming_replacement(self, exec_report):
        replacing_order = next((x for x in self.replaced_orders.values() if x.OrigClOrdId == exec_report.Order.ClOrdId),
                               None)
        return replacing_order

    def eval_order_is_replacement(self, exec_report, summary):
        if exec_report.Order.ClOrdId in self.replaced_orders:
            del self.replaced_orders[exec_report.Order.ClOrdId]

            if(summary.Position.IsRejectedPosition()):
                self.log("DB-REJ: Rejecteed Order {}".format(exec_report.Order.ClOrdId))

            if (summary.Position.IsOpenPosition() or summary.Position.IsFullyFilled()):
                self.update_control_vars(exec_report,summary)
                self.clear_replaced_order_on_replacement_arrival(exec_report, summary.Position.IsOpenPosition())

            if (summary.Position.IsRejectedPosition() or summary.Position.IsCancelledPosition()):
                #Replcement OrderA-->OrderB, we are with OrderB and was cancelled/rejected
                #then if routing_buy_order_id/routing_sell_order_id is the replaced ord,
                if (exec_report.Order.Side == Side.Buy and self.routing_buy_order_id==exec_report.Order.ClOrdId):
                    self.routing_buy_order_id = None
                    self.routing_long = 0
                elif (exec_report.Order.Side == Side.Sell and self.routing_sell_order_id == exec_report.Order.ClOrdId):
                    self.routing_sell_order_id = None
                    self.routing_short = 0
                else:
                    self.log("DB-E/F Recv canceleld/rejected but  active is OrigClOrdId={}".format(exec_report.Order.ClOrdId))

            #If the replacement order is not open (rejected, cancelled)
            #we have to wait for other execution reports to know what happened with the original one
            # else:
                # if (exec_report.Order.Side == Side.Buy):
                #     self.routing_buy_order_id = None
                #     self.routing_long = 0
                #     self.log("DB-E routing upd with={}".format(exec_report.Order.ClOrdId))
                # elif (exec_report.Order.Side == Side.Sell):
                #     self.routing_sell_order_id = None
                #     self.routing_short = 0
                #     self.log("DB-F routing upd with={}".format(exec_report.Order.ClOrdId))

            return True
        else:
            return False

    # a not tracked order could be external, or it could just be a lost order
    def eval_process_potential_external(self, exec_report, summary):

        if exec_report.Order.ClOrdId in self.orders:
            self.log(
                "WARNING-Ignoring unknown order with status {} for ClOrdId {} because the order has already been accepted"
                .format(summary.Position.PosStatus, exec_report.ClOrdId))
        else:
            self.log("EXTERNAL NEW detected for ClOrdId {}".format(exec_report.ClOrdId))

    def eval_order_finished(self,exec_report):
        if exec_report.Order.Side == Side.Buy and exec_report.Order.ClOrdId == self.routing_buy_order_id:
            if self.eval_coming_replacement(exec_report) is None:  # If we DONT have to wait for the replaced order
                self.routing_buy_order_id = None
                self.routing_long = 0
                self.log("DB-no coming replacement cancelling clOrdId {}".format(exec_report.Order.ClOrdId))
            else:  # if we have to, we replace the routing
                repl_order = self.eval_coming_replacement(exec_report)
                self.routing_buy_order_id = repl_order.ClOrdId
                self.log("DB-buy replaced with new order coming {}->{}".format(repl_order.OrigClOrdId,
                                                                                repl_order.ClOrdId))

        elif exec_report.Order.Side == Side.Sell and exec_report.Order.ClOrdId == self.routing_sell_order_id:
            if self.eval_coming_replacement(exec_report) is None:  # If we DONT have to wait for the replaced order
                self.routing_sell_order_id = None
                self.routing_short = 0
                self.log("DB-no coming replacement cancelling clOrdId {}".format(exec_report.Order.ClOrdId))
            else:  # if we have to, we replace the routing
                repl_order = self.eval_coming_replacement(exec_report)
                self.routing_sell_order_id = repl_order.ClOrdId
                self.log("DB-sell replaced with new order coming {}->{}".format(repl_order.OrigClOrdId,
                                                                                repl_order.ClOrdId))
        else:
            self.do_halt = True
            winsound.Beep(2500, 1000)
            self.log("ERROR-order {} <Side={}) not found on routing_sell={} or routing_buy={}".format(
                exec_report.Order.ClOrdId, exec_report.Order.Side, self.routing_sell_order_id,
                self.routing_buy_order_id))

    def process_order_started(self, summary, exec_report):

        if exec_report.Order.ClOrdId in self.sent_orders:

            if summary.Position.PosStatus == PositionStatus.New:
                self.log(
                    "New order for ClOrdId={} OrderId={}".format(exec_report.Order.ClOrdId, exec_report.Order.OrderId))

                self.orders[exec_report.Order.ClOrdId] = exec_report.Order
                del self.sent_orders[exec_report.Order.ClOrdId]

                self.eval_order_is_replacement(exec_report,summary)

            else:
                self.eval_process_potential_external(exec_report, summary)

        else:
            self.eval_process_potential_external(exec_report, summary)

    def process_order_finished(self, summary, exec_report):

        if exec_report.Order.ClOrdId in self.sent_orders:
            del self.sent_orders[exec_report.Order.ClOrdId]

        if exec_report.Order.ClOrdId in self.orders:
            del self.orders[exec_report.Order.ClOrdId]
            self.finished_orders[exec_report.Order.ClOrdId] = exec_report.Order
            if not self.eval_order_is_replacement(exec_report,summary):
                self.eval_order_finished(exec_report)
        else:
            self.eval_process_potential_external(exec_report, summary)

    def process_order_traded(self, summary, exec_report):

        if exec_report.Order.ClOrdId in self.sent_orders:
            del self.sent_orders[exec_report.Order.ClOrdId]

        if exec_report.Order.ClOrdId in self.orders:

            if not self.eval_order_is_replacement(exec_report, summary):
                self.update_control_vars(exec_report,summary)

            self.update_order_dictionaries(exec_report, summary)

            self.eval_trading_status_on_replacements(exec_report, summary)

            self.eval_halting_on_inconsistency(exec_report)

        else:
            self.eval_process_potential_external(exec_report, summary)

    # endregion

    # region Private Methods

    # region Util Methods


    def set_last_period_date(self,date):
        self.last_period_date=date

    def get_last_period_date(self):
        return self.last_period_date

    def is_new_day(self):
        pass

    def set_now(self, datetime):
        if self.process_start is None:
            self.process_start = datetime
            self.last_period_date=datetime
        self.price_logic.imbalance.set_now(datetime)
        self.price_logic.set_now(datetime)

        self.now = datetime

    def get_now(self):

        if not self.impl_backtest:
            return datetime.datetime.now()
        else:
            return self.now

    def get_timestamp(self, md):
        return md.MDEntryDate

    def is_closing_time(self):
        pass
        # TODO: Implement
        # if not self.trading_closed and self.evaluate_time():  # comparar fechas para ver si corresponde cierre
        #     self.log("Executing closing procedure at {}".format(self.get_now()))
        #     threading.Thread(target=self.run_final_opx_thread, args=()).start()
        #     self.trading_closed = True
        #
        # return self.trading_closed

    def log_engine_status(self, method, data, action):

        self.profit = 0
        if self.net_sells is not None and self.net_buys is not None and self.net_shares is not None and self.last_market_price is not None:
            self.profit = self.net_sells - self.net_buys + (self.net_shares * self.last_market_price)

        self.log(
            "{}- @ {} for action {} --> timestamp={} net_shares={} routing_long={} routing_short={} routing_buy_order_id={} routing_sell_order_id={} "
            "net_buys={} net_sells={} last_market_price={} profit={}".format(self.get_now(),
                                                                             method, action,
                                                                             self.get_timestamp(data),
                                                                             self.net_shares, self.routing_long,
                                                                             self.routing_short,
                                                                             self.routing_buy_order_id,
                                                                             self.routing_sell_order_id,
                                                                             round(self.net_buys, 2),
                                                                             round(self.net_sells, 2),
                                                                             self.last_market_price,
                                                                             round(self.profit, 2)))

    def log(self, msg):
        self.parent.DoLog(msg, MessageType.INFO)

    # endregion

    # region Profitability Methods

    def eval_halting_on_inconsistency(self, exec_report):

        if (exec_report.Order.Side == Side.Sell):
            self.do_halt = self.routing_short > 0
            if self.do_halt:
                self.log("ERROR-Halting on POSITIVE routing_short number={}".format(self.routing_short))
                winsound.Beep(2500, 1000)

        elif (exec_report.Order.Side == Side.Buy):
            self.do_halt = self.routing_long < 0
            if self.do_halt:
                self.log("ERROR-Haling on NEGATIVE routing_long number={}".format(self.routing_long))
                winsound.Beep(2500, 1000)

    def eval_trading_status_on_replacements(self, exec_report, summary):
        if (exec_report.Order.Side == Side.Sell):

            # We have a trade, but a new order is coming
            # we set the routing_buy_order_id to the coming order so that no routing is made
            if self.eval_coming_replacement(exec_report) is not None and summary.Position.IsFinishedPosition():
                ord_repl = self.eval_coming_replacement(exec_report)
                self.routing_sell_order_id = ord_repl.ClOrdId
                self.log("DB-B routing upd with={}".format(ord_repl.ClOrdId))

        elif (exec_report.Order.Side == Side.Buy):

            # We have a trade, but a new order is coming
            # we set the routing_buy_order_id to the coming order so that no routing is made
            if self.eval_coming_replacement(exec_report) is not None and summary.Position.IsFinishedPosition():
                ord_repl = self.eval_coming_replacement(exec_report)
                self.routing_buy_order_id = ord_repl.ClOrdId
                self.log("DB-A routing upd with={}".format(ord_repl.ClOrdId))

    def update_order_dictionaries(self, exec_report, summary):

        if (exec_report.Order.Side == Side.Sell):

            if self.routing_sell_order_id is None:

                if summary.Position.IsFinishedPosition():
                    self.log("DB-delete {}".format(exec_report.Order.ClOrdId))
                    del self.orders[exec_report.Order.ClOrdId]
                    self.finished_orders[exec_report.Order.ClOrdId] = exec_report.Order
                else:
                    #self.routing_sell_order_id=exec_report.Order.ClOrdId
                    self.log(
                        "WARNING- no buy routing order id but the position is not finished:ClOrdId={} Status={}".format(
                            exec_report.Order.ClOrdId, summary.Position.PosStatus))

        elif (exec_report.Order.Side == Side.Buy):

            if self.routing_buy_order_id is None:

                if summary.Position.IsFinishedPosition():
                    self.log("DB-delete {}".format(exec_report.Order.ClOrdId))
                    del self.orders[exec_report.Order.ClOrdId]
                    self.finished_orders[exec_report.Order.ClOrdId] = exec_report.Order
                else:
                    #self.routing_buy_order_id=exec_report.Order.ClOrdId
                    self.log(
                        "ERROR- no sell routing order id but the position is not finished:ClOrdId={} Status={}".format(
                            exec_report.Order.ClOrdId, summary.Position.PosStatus))

    def update_control_vars(self, exec_report,summary):

        if (exec_report.Order.Side == Side.Sell):
            self.net_shares -= exec_report.LastQty
            self.net_short_contracts += abs(exec_report.LastQty)
            self.log("Reducing Routing Short: routing_short_prev={} last_qty={} (ClOrdId={})".format(self.routing_short,
                                                                                                     exec_report.LastQty,
                                                                                                     exec_report.Order.ClOrdId))
            self.routing_short = exec_report.LeavesQty*-1 if summary.Position.IsOpenPosition()  else 0  # routing short is negative
            self.log("Updated Routing Short: routing_short_after={} ".format(self.routing_short))
            self.routing_sell_order_id = None if summary.Position.IsFinishedPosition()  else self.routing_sell_order_id
            self.net_sells += exec_report.LastQty * (exec_report.AvgPx if exec_report.AvgPx is not None else 0)

        elif (exec_report.Order.Side == Side.Buy):

            self.net_shares += exec_report.LastQty
            self.net_long_contracts += exec_report.LastQty
            self.log("Reducing Routing Long: routing_long_prev={} last_qty={} (ClOrdId={})".format(self.routing_long,
                                                                                                   exec_report.LastQty,
                                                                                                   exec_report.Order.ClOrdId))
            self.routing_long = exec_report.LeavesQty if summary.Position.IsOpenPosition() else 0
            self.log("Updated Routing Long: routing_long_after={} ".format(self.routing_long))
            self.routing_buy_order_id = None if summary.Position.IsFinishedPosition() else self.routing_buy_order_id
            self.net_buys += exec_report.LastQty * (exec_report.AvgPx if exec_report.AvgPx is not None else 0)

        self.calcualte_net_profitability()

    def clear_replaced_order_on_replacement_arrival(self,exec_report,active):
        if exec_report.Order.OrigClOrdId is not None and exec_report.Order.OrigClOrdId in self.orders:
            # the new order arrived BEFORE the cancelled order
            del self.orders[exec_report.Order.OrigClOrdId]
            self.finished_orders[exec_report.Order.OrigClOrdId] = exec_report.Order
            if (exec_report.Order.Side == Side.Buy):
                self.routing_buy_order_id = exec_report.Order.ClOrdId if active else None
            elif (exec_report.Order.Side == Side.Sell):
                self.routing_sell_order_id = exec_report.Order.ClOrdId if active else None

    def calcualte_net_profitability(self):

        profit = self.net_sells - self.net_buys + (self.net_shares * self.last_market_price)

        self.log("{}-Profit:{} Net Shares={} Market Price={} NetBuys={} NetSells={}".format(
            self.get_now(), profit, self.net_shares, round(self.last_market_price, 2), round(self.net_buys, 2),
            round(self.net_sells, 2)))

    # endregion

    # region Trading Methods

    def eval_trading_conditions(self, md):
        if self.is_closing_time():
            return False

        if self.pending_replaces():
            return False  # we have to update to the replaced orders to arrive to continue

        if self.do_halt:
            self.log("SYSTEM HALTED AT {}".format(md.MDEntryDate))
            return False

        return True

    def get_buy_qty(self):

        if (self.trade_unit - self.routing_long) > 0:
            return self.trade_unit - self.routing_long
        else:
            raise Exception("ERROR:Could not route {} units to market when we already have {} units routed".format(
                self.trade_unit, self.routing_long))

    def get_sell_qty(self):

        if (self.trade_unit - abs(self.routing_short)) > 0:
            return self.trade_unit - abs(self.routing_short)
        else:
            raise Exception("ERROR:Could not route {} units to market when we already have {} units routed".format(
                self.trade_unit, self.routing_short))

    def pending_replaces(self):

        toDel = []
        for replace_order in self.replaced_orders.values():
            now=datetime.datetime.now()
            span = now - replace_order.RealTimespan #We validate real time span (for backtesters and live exec)
            #span = self.get_now() - replace_order.SpotTimespan
            if (span.seconds > 50):
                #self.do_halt = True

                toDel.append(replace_order.ClOrdId)
                self.log(
                    "WARNING- Replaced order {} took more than 50 seconds to arrive!. {} vs {}".format(replace_order.ClOrdId,now,replace_order.RealTimespan))
                #winsound.Beep(2500, 1000)

        for clOrdId in toDel:
            del self.replaced_orders[clOrdId]
            del self.orders[clOrdId]
            del self.sent_orders[clOrdId]
            self.finished_orders[clOrdId]=clOrdId
            if self.routing_buy_order_id == clOrdId:
                self.routing_buy_order_id = None
                self.routing_long=0
            if self.routing_sell_order_id == clOrdId:
                self.routing_sell_order_id = None
                self.routing_short = 0

        return len(self.replaced_orders) != 0

    def validadte_going_long_inv(self):
        # No llegamos al limite de tenencias long y no estamos ruteando ya toda la trade_unit al
        # mercado
        return ((self.net_shares + self.routing_long) < (self.max_long_qty)) and (
                self.trade_unit > self.routing_long)

    def validate_going_long(self):

        if self.pending_replaces():
            return False

        if  (self.price_logic.is_strong_direction(_SIDE_BID) or self.price_logic.is_opposite_inventory(_SIDE_BID)) and  self.last_buy_order_sent is not None:

            span = self.get_now() - self.last_buy_order_sent

            self.log(
                "<pacing> Long- Evaluating that {} seconds elapsed is bigger than req_pacing (sec)= {}".format(span.seconds,
                                                                                                               self.strong_dir_pacing_sec))
            if (span.seconds > self.strong_dir_pacing_sec):
                self.log("<pacing>-good to go long")

                # No llegamos al limite de tenencias long y no estamos ruteando ya toda la trade_unit al mercado
                return self.validadte_going_long_inv()
            else:
                return False
        else:
            return  self.validadte_going_long_inv()

    def validate_going_short_inv(self):
        # No llegamos al limite de tenencias long y no estamos ruteando ya toda la trade_unit al mercado
        # max_short_qty,routing_short están expresados como negativos asi que hay que hacer las consideraciones de signo
        # necesarias
        return (self.net_shares + self.routing_short) > (self.max_short_qty) and (
                self.trade_unit > abs(self.routing_short))

    def validate_going_short(self):
        if self.pending_replaces():
            return False

        if (self.price_logic.is_strong_direction(_SIDE_ASK) or self.price_logic.is_opposite_inventory(_SIDE_ASK)) and self.last_sell_order_sent is not None:

            span = self.get_now() - self.last_sell_order_sent
            self.log("<pacing> Short- Evaluating that {} seconds elapsed is bigger than req_pacing (sec)= {}".format(
                span.seconds, self.strong_dir_pacing_sec))

            if (span.seconds > self.strong_dir_pacing_sec):
                self.log("<pacing>-good to go short")

                # No llegamos al limite de tenencias long y no estamos ruteando ya toda la trade_unit al mercado
                # max_short_qty,routing_short están expresados como negativos asi que hay que hacer las consideraciones de signo
                # necesarias
                return self.validate_going_short_inv()
            else:
                return False
        else:
            return self.validate_going_short_inv()

    # endregion

    # endregion

    # region Public Methods

    def get_strategy_summary(self,conf):
        summary = strategy_summary(start=calendar_helper.get_start(conf, self.get_last_period_date()),
                                   end=calendar_helper.get_end(conf, self.get_last_period_date()),
                                   date=datetime.datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S.%f')[:-3],
                                   type=conf.PeriodicPersist,
                                   profit=round(self.profit,2),
                                   net_buys=round(self.net_buys,2) if self.net_buys is not None else 0,
                                   net_sells=round(self.net_sells,2) if self.net_sells is not None else 0,
                                   net_shares=round(self.net_shares, 2) if self.net_shares is not None else 0,
                                   imbalance_start=self.price_logic.imbalance.imbalance_start,
                                   imbalance_end=self.price_logic.imbalance.imbalance_end,
                                   imbalance_bid=self.price_logic.imbalance.get_imbalance_bid(),
                                   imbalance_ask=self.price_logic.imbalance.get_imbalance_ask(),
                                   imbalance_activated=self.price_logic.imbalance.imbalance_activated,
                                   market_price=self.last_market_price,
                                   nominal_volatiliy=None,
                                   new_high_lows_start=None, new_high_lows_end=None, new_high_lows_type=None,
                                   reggr_slope=None, candle_breadth=None)
        return summary

    def start(self, parent):
        self.parent = parent
        self.log("Starting Nassini - Market Maker")

        # region Administrative
        self.routing_buy_order_id = None
        self.routing_sell_order_id = None

        self.impl_replace = True
        # endregion

        # region Calculated
        self.last_market_price = 0
        self.routing_long = 0
        self.routing_short = 0
        self.net_shares = 0
        self.net_sells = 0
        self.net_buys = 0
        self.net_long_contracts = 0
        self.net_short_contracts = 0
        self.profit = 0

        self.price_logic.imbalance.start()
        self.trading_closed = False

        self.new_high_lows_activated = None
        self.new_high_lows_start = None
        self.new_high_lows_end = None

        self.CumQtyDict = {}

        self.now = None
        self.process_start = None
        self.last_period_date = None
        self.price_logic.imbalance.last_imbalance_depuration = None
        self.last_buy_order_sent = None
        self.last_sell_order_sent = None
        self.do_halt = False

        # endregion

        self._routing_lock = threading.Lock()

        self.orders = {}  # All the sent, live until traded/cancelled in the market
        self.finished_orders = {}  # all the traded/cancelled/rejected orderds
        self.sent_orders = {}  # All the sent but not yet accepted
        self.replaced_orders = {}  # Orders replaced but not yet accepted at the exchagne

        self.last_buy_order_sent=self.get_now()
        self.last_sell_order_sent=self.get_now()

    def on_market_data(self, md):
        try:

            self.log_engine_status("next", md, "{}-Recv market data".format(time_util.full_now()))

            self._routing_lock.acquire(blocking=True)

            if not self.eval_trading_conditions(md):
                return

            self.price_logic.imbalance.calculate_imbalance(md)

            self.price_logic.imbalance.eval_depurate_imbalance()

            ref_price = self.midPriceLogic.get_market_price(md,self.price_logic.imbalance.mon_volume_bid > self.price_logic.imbalance.mon_volume_ask)

            prev_market_price = self.last_market_price
            self.last_market_price = ref_price

            if ref_price is not None and not math.isnan(ref_price):

                # self.log_engine_status("next",self.data, "New market price")
                if self.routing_buy_order_id is not None and prev_market_price != self.last_market_price:
                    new_buy_price = self.price_logic.get_bid_reference_price(ref_price,md)
                    self.do_cancel_replace(new_buy_price, self.routing_buy_order_id, isBuy=True)

                elif (self.routing_buy_order_id is None and self.validate_going_long()):
                    buy_price = self.price_logic.get_bid_reference_price(ref_price, md)
                    self.do_buy(self.symbol,self.get_buy_qty(), buy_price)

                if self.routing_sell_order_id is not None and prev_market_price != self.last_market_price:
                    new_sell_price = self.price_logic.get_ask_reference_price(ref_price,md)
                    self.do_cancel_replace(new_sell_price, self.routing_sell_order_id, isBuy=False)

                elif (self.routing_sell_order_id is None and self.validate_going_short()):
                    sell_price = self.price_logic.get_ask_reference_price(ref_price, md)
                    self.do_sell(self.symbol,self.get_sell_qty(), sell_price)

        except Exception as e:
            self.log(traceback.print_exc())
            msg = "Critical ERROR @MarketMaker.on_market_data:{}".format(str(e))
            self.log(msg)

        finally:
            self.log("{}-Exit processing market data".format(time_util.full_now()))
            if self._routing_lock.locked():
                self._routing_lock.release()

    def on_execution_report(self, exec_report, summary):
        try:
            self._routing_lock.acquire(blocking=True)
            self.log("DB-llego ER {} for ClOrdId {}".format(summary.Position.PosStatus, exec_report.Order.ClOrdId))

            if summary.Position.IsOpenPosition() and not summary.Position.IsTradedPosition():  # New/#Pending New
                self.process_order_started(summary, exec_report)
            elif summary.Position.IsRejectedPosition():
                self.process_order_finished(summary,exec_report)
            elif summary.Position.IsFinishedPosition() and not summary.Position.IsTradedPosition():  # Canceled, Rejected, etc.
                self.process_order_finished(summary, exec_report)
            elif summary.Position.IsTradedPosition():  # Filled , Partially Filled
                self.process_order_traded(summary, exec_report)
            else:
                raise Exception("Unknown execution report status for ClOrdId {}:{}".format(exec_report.Order.ClOrdId,
                                                                                           summary.Position.PosStatus))

        except Exception as e:
            print(traceback.print_exc())
            msg = "Critical ERROR @MarketMaker.on_execution_report:{}".format(str(e))
            self.log(msg)

        finally:
            if self._routing_lock.locked():
                self._routing_lock.release()

    # endregion
