import math


class MidPriceLogic():
    #region Constructor

    def __init__(self,mid_price_calculation,default_spread,tick_size,imbalance):
        self.mid_price_calculation = mid_price_calculation  # 1=last,2=average to last,3=average with imbalance
        self.default_spread=default_spread
        self.imbalance=imbalance
        self.tick_size=tick_size
    #endregion

    #region Private Methods

    def log(self,msg):
        pass
        #TODO: implement log mechanism @imbalance

    def round_to_tick(self,price,round_down):

        if (price % self.tick_size) != 0:
            price = (int(price / self.tick_size)) * self.tick_size
            price = price if self.imbalance.mon_volume_bid > self.imbalance.mon_volume_ask else price + self.tick_size

        return price

    # mid price strategy #1 --> it's the easiest one.
    # the mid price is the last px
    def get_market_price_last_px(self, md):
        return md.Trade

    # mid price strategy #2 --> it's the most volatile
    # the mid price is the average. If not round number --> we use the last_px
    def get_market_price_avg_to_last_px(self, md,round_down):
        if md.BestBidPrice is not None and md.BestAskPrice is not None:

            mid_price = (md.BestBidPrice + md.BestAskPrice) / 2

            mid_price = self.round_to_tick(mid_price,round_down)

            self.log("<mid-price #2>-Evaluating Last Px.={} Best Bid={} and Best Ask={}".format(md.Trade,
                                                                                                md.BestBidPrice,
                                                                                                md.BestAskPrice))

            if (mid_price % self.default_spread) == 0:
                return mid_price
            else:
                return md.Trade
        else:
            return md.Trade

    # mid price strategy #3
    # the mid price is the average. If not round number --> we use the bid or ask depending on imbalance
    def get_market_price_avg_to_imbalance(self, md):

        if md.BestBidPrice is not None and md.BestAskPrice is not None:

            mid_price = (md.BestBidPrice + md.BestAskPrice) / 2

            if (mid_price % self.default_spread) != 0:
                mid_price = md.BestBidPrice if self.imbalance.mon_volume_bid > self.imbalance.mon_volume_ask else md.BestAskPrice

            self.log(
                "<mid-price #3>-Evaluating Last Px.={} Best Bid={} and Best Ask={} Def_Spread={} Mon. Volume Bid={} Mon.Volume.Ask={} Mid.Price={}"
                    .format(md.Trade, md.BestBidPrice, md.BestAskPrice, self.default_spread, self.imbalance.mon_volume_bid,
                            self.imbalance.mon_volume_ask, mid_price))

            return mid_price

        else:
            return md.BestBidPrice if self.imbalance.mon_volume_bid > self.imbalance.mon_volume_ask else md.BestAskPrice

        # mid price strategy #4
        # the mid price is the average. If not round number --> we use average up by tick on imbalance

    # mid price strategy #4
    def get_market_price_avg_to_tick(self, md,round_down):

        if md.BestBidPrice is not None and md.BestAskPrice is not None \
            and not math.isnan(md.BestBidPrice) and not math.isnan(md.BestAskPrice):

            mid_price = (md.BestBidPrice + md.BestAskPrice) / 2

            if (mid_price % self.tick_size) != 0:
                mid_price = self.round_to_tick(mid_price,round_down)

            self.log(
                "<mid-price #4>-Evaluating Last Px.={} Best Bid={} and Best Ask={} Def_Spread={} Mon. Volume Bid={} Mon.Volume.Ask={} Mid.Price={}"
                    .format(md.Trade, md.BestBidPrice, md.BestAskPrice, self.default_spread, self.imbalance.mon_volume_bid,
                            self.imbalance.mon_volume_ask, mid_price))

            return mid_price

        else:
            return md.BestBidPrice if self.imbalance.mon_volume_bid > self.imbalance.mon_volume_ask else md.BestAskPrice

    #endregion

    #region Public Methods

    def get_market_price(self, md,round_down):

        ref_price = None
        if md.Trade is not None:

            if self.mid_price_calculation == 1:
                ref_price = self.get_market_price_last_px(md)
            elif self.mid_price_calculation == 2:
                ref_price = self.get_market_price_avg_to_last_px(md,round_down)
            elif self.mid_price_calculation == 3:
                ref_price = self.get_market_price_avg_to_imbalance(md)
            elif self.mid_price_calculation == 4:
                ref_price = self.get_market_price_avg_to_tick(md,round_down)
            else:
                raise Exception("Invalid mid_price_calculation: {}".format(self.mid_price_calculation))

        return ref_price

    #endregion