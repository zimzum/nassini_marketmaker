import decimal
import winsound

_SIDE_BID="BID"
_SIDE_ASK="ASK"
_IMBALANCE_BID="BID"
_IMBALANCE_BID_END="BID_END"
_IMBALANCE_ASK_END="ASK_END"
_IMBALANCE_ASK="ASK"


class OrderPriceLogic():
    def __init__(self,imbalance,tick_size,invokingStrategy,impl_opposite_inventory,impl_strong_direction,
                 impl_rope_price_on_regular_markets,default_spread,inv_threshold,inventory_spread_unit,
                 elastic_rope_pct,logger):
        self.imbalance=imbalance
        self.invokingStrategy=invokingStrategy
        self.impl_opposite_inventory=impl_opposite_inventory
        self.impl_strong_direction=impl_strong_direction
        self.impl_rope_price_on_regular_markets=impl_rope_price_on_regular_markets
        self.default_spread=default_spread
        self.inv_threshold=inv_threshold
        self.inventory_spread_unit=inventory_spread_unit
        self.elastic_rope_pct=elastic_rope_pct
        self.tick_size=tick_size
        self.logger=logger

    def start(self):
        self.market_data=None
        self.now=None

    #region Private Methods

    def log(self,msg):
        self.logger.log(msg)

    def set_now(self,datetime):
        self.now=datetime
        self.imbalance.set_now(datetime)

    def get_now(self):
        return self.now

    def round_to_tick(self,price):

        if (price % self.tick_size) != 0:
            price = (int(price / self.tick_size)) * self.tick_size
            price = price if self.imbalance.mon_volume_bid > self.imbalance.mon_volume_ask else price + self.tick_size

        return price

    def get_rope_price(self, mid_price, side,md):
        #winsound.Beep(2500, 1000)
        if side == _SIDE_BID:
            spread = (1 - self.elastic_rope_pct) * self.default_spread
            new_price = mid_price - spread
            new_price = self.round_to_tick(new_price)
            return new_price
        elif side == _SIDE_ASK:
            spread = (1 - self.elastic_rope_pct) * self.default_spread
            new_price = mid_price + spread
            new_price = self.round_to_tick(new_price)
            return new_price
        else:
            raise Exception("Invalid side {} at get_opposite_inventory_price".format(side))

    def get_ask_inventory_spread(self):

        # Cada <inv_threshold> unidades de la cantidad de contratos, DISMINUYO inventory_spread_units en el precio del ask
        # cuanto mas short estoy, mas bajo es el precio que ofrezco
        # NOTA: El restultado de este método va a ser SUMADO al precio de bid
        if (self.invokingStrategy.net_shares < 0):
            return (int(abs(self.invokingStrategy.net_shares) / self.inv_threshold)) * self.inventory_spread_unit
        else:
            return 0

    def is_opposite_inventory(self, side):

        if self.impl_opposite_inventory != 1:
            return False

        if side == _SIDE_BID:  # I must make the bid aggressive because of opposite invalance
            return self.imbalance.imbalance_activated == _IMBALANCE_ASK and int(
                self.invokingStrategy.net_shares) < 0  # I am short and the ASK imbalance is activated

        if side == _SIDE_ASK:  # I must make the ask aggressive because of opposite invalance
            return self.imbalance.imbalance_activated == _IMBALANCE_BID and int(
                self.invokingStrategy.net_shares) > 0  # I am long and the BID imbalance is activated

        return False

    def is_strong_direction(self,side):

        if self.impl_strong_direction != 1:
            return False

        if side == _SIDE_BID:  # I must make the bid aggressive because of opposite invalance
            return self.imbalance.imbalance_activated == _IMBALANCE_ASK   # I am short and the ASK imbalance is activated

        if side == _SIDE_ASK:  # I must make the ask aggressive because of opposite invalance
            return self.imbalance.imbalance_activated == _IMBALANCE_BID  # I am long and the BID imbalance is activated

        return False

    def get_opposite_inventory_price(self,mid_price,side,md):
        #winsound.Beep(2500, 1000)
        if side == _SIDE_BID:
            return self.market_data.BestAskPrice

        elif side == _SIDE_ASK:
            return self.market_data.BestBidPrice
        else:
            raise Exception("Invalid bid {} at get_opposite_inventory_price".format(side))

    def get_strong_direction_price(self,mid_price, side,md):
        if side == _SIDE_BID:
            return self.market_data.BestAskPrice

        elif side == _SIDE_ASK:
            return self.market_data.BestBidPrice
        else:
            raise Exception("Invalid bid {} at get_opposite_inventory_price".format(side))

    def get_bid_inventory_spread(self):

        # Cada <inv_threshold> unidades de la cantidad de contratos en inventario, DISMINUYO inventory_spread_unit en el precio del bid
        # cuanto mas long estoy, mas bajo es el precio que ofrezco
        # NOTA: El restultado de este método va a ser RESTADO al precio de bid

        if (self.invokingStrategy.net_shares > 0):
            return (int(self.invokingStrategy.net_shares / self.inv_threshold)) * self.inventory_spread_unit
        else:
            return 0

    def get_bid_imbalance_spread(self,mid_price):

        if not self.imbalance.validate_imbalance_min_opx_timespan():
            return 0

        tot_vol = self.imbalance.mon_volume_ask + self.imbalance.mon_volume_bid
        imbalance_bid = self.imbalance.mon_volume_bid / tot_vol if tot_vol != 0 else 0
        self.log("Imbalance Bid calculated={} (tot_vol={} - mon_volume_bid={}".format(imbalance_bid,tot_vol,self.imbalance.mon_volume_bid))

        if self.imbalance.eval_bid_imbalance_entry_conditions(mid_price,imbalance_bid):
            #IMBALANCE ACTIVADO!
            net_excess_bid_imbalance = imbalance_bid - self.imbalance.imbalance_min_threshold if imbalance_bid>self.imbalance.imbalance_min_threshold else 0
            imb_multiplier = (int(decimal.Decimal( net_excess_bid_imbalance) / decimal.Decimal( self.imbalance.imbalance_unit))) + 1
            bid_imb_spread = imb_multiplier * self.imbalance.imbalance_price_delta

            if self.imbalance.imbalance_is_off(): #just triggered
                self.imbalance.imbalance_activated=_IMBALANCE_BID
                self.imbalance.imbalance_start=self.get_now()
                self.imbalance.imbalance_entry_price=mid_price
                self.imbalance.imbalance_end = None

            self.log(
                "BID IMBALANCE ACTIVATED: Bid Imbalance={}==> Bid Imb. Spread calculated={} (multiplier={})"
                    .format(imbalance_bid,bid_imb_spread,imb_multiplier))

            return bid_imb_spread

        else:
            if self.imbalance.imbalance_start is not None and self.imbalance.imbalance_activated==_IMBALANCE_BID:
                self.imbalance.imbalance_end=self.get_now()
                self.imbalance.imbalance_activated=_IMBALANCE_BID_END
                self.imbalance.imbalance_bid_first_triggered = None

                self.log("Closing bid imbalance at {} on threshold {}".format(self.get_now(),imbalance_bid))
            return 0

    def get_ask_imbalance_spread(self, mid_price):

        if not self.imbalance.validate_imbalance_min_opx_timespan():
            return 0

        tot_vol = self.imbalance.mon_volume_ask + self.imbalance.mon_volume_bid
        imbalance_ask = self.imbalance.mon_volume_ask / tot_vol if tot_vol != 0 else 0
        self.log("Imbalance Ask calculated={} (tot_vol={} - mon_volume_ask={}".format(imbalance_ask, tot_vol,self.imbalance.mon_volume_ask))

        if self.imbalance.eval_ask_imbalance_entry_conditions(mid_price, imbalance_ask):
            net_excess_ask_imbalance = imbalance_ask - self.imbalance.imbalance_min_threshold if imbalance_ask > self.imbalance.imbalance_min_threshold else 0
            imb_multiplier = (int(decimal.Decimal(net_excess_ask_imbalance) / decimal.Decimal(self.imbalance.imbalance_unit))) + 1
            ask_imb_spread = imb_multiplier * self.imbalance.imbalance_price_delta

            if self.imbalance.imbalance_is_off():  # just triggered!
                self.imbalance.imbalance_activated = _IMBALANCE_ASK
                self.imbalance.imbalance_start = self.get_now()
                self.imbalance.imbalance_entry_price = mid_price
                self.imbalance.imbalance_end = None

            self.log(
                "ASK IMBALANCE ACTIVATED: Ask Imbalance={}==> Ask Imb. Spread calculated={} (multiplier={})"
                    .format(imbalance_ask, ask_imb_spread, imb_multiplier))

            return ask_imb_spread
        else:
            if self.imbalance.imbalance_start is not None and self.imbalance.imbalance_activated == _IMBALANCE_ASK:
                self.imbalance.imbalance_end = self.get_now()
                self.imbalance.imbalance_activated = _IMBALANCE_ASK_END
                self.imbalance.imbalance_ask_first_triggered = None
                self.log("Closing ask imbalance at {} on threshold {}".format(self.get_now(), imbalance_ask))
            return 0

    def get_regular_bid_price(self,mid_price,md):

        if self.invokingStrategy.net_shares>=0 or self.impl_rope_price_on_regular_markets==0 or not self.imbalance.imbalance_is_off():
            price = mid_price - self.default_spread - self.get_bid_inventory_spread() - self.get_bid_imbalance_spread(mid_price)
            return price
        else:
            self.get_bid_imbalance_spread(mid_price)  # I have to validate that teh ask-imbalance is on-off
            return self.get_rope_price(mid_price,_SIDE_BID,md)

    def get_regular_ask_price(self,mid_price,md):
        if self.invokingStrategy.net_shares<=0 or self.impl_rope_price_on_regular_markets==0  or not self.imbalance.imbalance_is_off():
            price  = mid_price + self.default_spread + self.get_ask_inventory_spread() + self.get_ask_imbalance_spread(mid_price)
            return price
        else:
            self.get_ask_imbalance_spread(mid_price)  # I have to validate that the bid-imbalance is on or off
            return self.get_rope_price(mid_price,_SIDE_ASK,md)

    def get_regular_price(self,mid_price,side,md):

        if side==_SIDE_BID:
            return self.get_regular_bid_price(mid_price,md)
        elif side==_SIDE_ASK:
            return self.get_regular_ask_price(mid_price,md)
        else:
            raise Exception("Unknown side {}".format(side))

    #endregion

    #region Public Methods

    def get_bid_reference_price(self,mid_price,md=None):
        if md is not None:
            self.market_data=md

        if self.is_opposite_inventory(_SIDE_BID):
            return self.get_opposite_inventory_price(mid_price,_SIDE_BID,md) # self.get_opposite_inventory_price(mid_price,BID)
        elif self.is_strong_direction(_SIDE_BID):
            return self.get_strong_direction_price(mid_price,_SIDE_BID,md) # self.get_strong_direction_price(mid_price,BID)
        else:
            return self.get_regular_price(mid_price,_SIDE_BID,md)

    def get_ask_reference_price(self,mid_price,md):
        self.market_data = md

        if self.is_opposite_inventory(_SIDE_ASK):
            return self.get_opposite_inventory_price(mid_price,_SIDE_ASK,md) # self.get_opposite_inventory_price(mid_price,ASK)
        elif self.is_strong_direction(_SIDE_ASK):
            return self.get_strong_direction_price(mid_price,_SIDE_ASK,md) # self.get_strong_direction_price(mid_price,ASK)
        else:
            return self.get_regular_price(mid_price,_SIDE_ASK,md)

    #endregion