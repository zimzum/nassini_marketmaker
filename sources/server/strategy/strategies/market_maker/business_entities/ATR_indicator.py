class ATRIndicator():
    def __init__(self,ATR_imbalance_exit_length):
        self.candle_prices = []
        self.last_candle_timestamp = None
        self.ATR_imbalance_exit_length=ATR_imbalance_exit_length
        self.now=None

    #region Public Methods

    def calculate_ATR_bid_reference_price(self):
        min_val = min(self.candle_prices[-1 * self.ATR_imbalance_exit_length:])
        return min_val

    def calculate_ATR_ask_reference_price(self):
        max_val = max(self.candle_prices[-1 * self.ATR_imbalance_exit_length:])
        return max_val

    def calculate_ATR_value(self):

        last=len(self.candle_prices)-1
        first = len(self.candle_prices)-self.ATR_imbalance_exit_length-1
        first= 0 if first<0 else first

        ATRs=[]
        if(len(self.candle_prices)>=2):
            for i in (last,first+1):
                ATRs.append(abs(self.candle_prices[i]-self.candle_prices[i-1]))

            return  sum(ATRs)/len(ATRs)
        else:
            return 0

    def set_now(self,datetime):
        self.now=datetime

    def get_now(self):
        return self.now

    def persist_candle(self,md):

        if md.Trade is not None and self.last_candle_timestamp is not None and  (self.get_now().minute != self.last_candle_timestamp.minute):
            self.candle_prices.append(md.Trade)
            self.last_candle_timestamp = self.get_now()
        elif self.last_candle_timestamp is None:
            self.last_candle_timestamp=self.get_now()


    #endregion