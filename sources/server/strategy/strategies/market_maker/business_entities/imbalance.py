import decimal
import time

from sources.server.strategy.strategies.market_maker.business_entities.ATR_indicator import ATRIndicator

_IMBALANCE_ASK="ASK"
_IMBALANCE_BID="BID"
_IMBALANCE_BID_END="BID_END"
_IMBALANCE_ASK_END="ASK_END"
class Imbalance():

    #region Constructor
    def __init__(self,imbalance_min_threshold,imbalance_exit_threshold,imbalance_unit,imbalance_price_delta,
                    imbalance_min_opx_timespan,imbalance_depuration_threshold,
                    imbalance_min_entry_sec, switched_off_imbalance_time,
                    ATR_imbalance_exit_length,ATR_imbalance_safety_mult,impl_ATR_imbalance_exit):
        self.imbalance_min_threshold = imbalance_min_threshold
        self.imbalance_exit_threshold = imbalance_exit_threshold
        self.imbalance_unit = imbalance_unit
        self.imbalance_price_delta = imbalance_price_delta

        self.imbalance_min_opx_timespan = imbalance_min_opx_timespan
        self.imbalance_depuration_threshold = imbalance_depuration_threshold

        self.ATR_imbalance_exit_length = ATR_imbalance_exit_length
        self.ATR_imbalance_safety_mult = ATR_imbalance_safety_mult

        self.impl_ATR_imbalance_exit = impl_ATR_imbalance_exit
        self.ATR_indicator=ATRIndicator(ATR_imbalance_exit_length)

        self.imbalance_min_entry_sec=imbalance_min_entry_sec
        self.switched_off_imbalance_time=switched_off_imbalance_time

    def start(self):
        self.shares_bid = 0
        self.shares_ask = 0
        self.mon_volume_bid = 0
        self.mon_volume_ask = 0
        self.shares_bid_snapshot = 0
        self.shares_ask_snapshot = 0
        self.mon_volume_bid_snapshot = 0
        self.mon_volume_ask_snapshot = 0
        self.last_price = None
        self.last_size = None

        self.imbalance_activated=None
        self.imbalance_start = None
        self.imbalance_end = None
        self.imbalance_entry_price= None
        self.imbalance_bid_ATR_exit=False
        self.imbalance_ask_ATR_exit = False
        self.imbalance_bid_first_triggered=None
        self.imbalance_ask_first_triggered = None

        self.last_imbalance_depuration=None

        self.process_start = None
        self.now=None

    #endregion

    #region Private Methods

    def log(self,msg):
        pass
        #TODO: implement log mechanism @imbalance

    def set_now(self,datetime):
        if self.process_start is None:
            self.process_start=datetime
            self.last_imbalance_depuration=datetime
        self.now=datetime
        self.ATR_indicator.set_now(datetime)

    def get_now(self):
        return self.now

    def clean_time_format(self, time):

        if time.endswith("PM") and not time.endswith(" PM"):
            if time.endswith("PM"):
                time = time.replace("PM", " PM")

        if time.endswith("AM") and not time.endswith(" AM"):
            if time.endswith("PM"):
                time = time.replace("AM", " AM")

        return time

    def eval_imbalance_time(self):
        now=self.get_now()
        off_imbalance_time = self.clean_time_format(self.switched_off_imbalance_time)
        exit_time = time.strptime(off_imbalance_time, "%I:%M %p")
        exit_date_time = now.replace(hour=exit_time.tm_hour, minute=exit_time.tm_min, second=0, microsecond=0)
        return now<exit_date_time

    def ask_imbalance_still_active(self,imbalance_ask):

        # imbalance > entry imbalance (ex:75%) y activado y mayor que el exit_imbalance (ex:60%)
        span = None
        if self.imbalance_ask_first_triggered is not None:
            span = self.get_now() - self.imbalance_ask_first_triggered
            self.log("<imbalance> Ask- Elapsed {} seconds from imb. triggered (must wait {} seconds)".format(span.total_seconds(), self.imbalance_min_entry_sec))

        entry_cond = imbalance_ask > self.imbalance_min_threshold and (span is not None and span.seconds > self.imbalance_min_entry_sec)
        stil_on_cond = self.imbalance_start is not None and self.imbalance_activated == _IMBALANCE_ASK and imbalance_ask > self.imbalance_exit_threshold
        time_cond = self.eval_imbalance_time()

        #imbalance > entry imbalance (ex:75%) y activado y mayor que el exit_imbalance (ex:60%)
        return  (entry_cond or stil_on_cond) and time_cond

    def bid_imbalance_still_active(self,imbalance_bid):
        # imbalance > entry imbalance (ex:75%) y activado y mayor que el exit_imbalance (ex:60%)

        span=None
        if self.imbalance_bid_first_triggered is not None:
            span = self.get_now() - self.imbalance_bid_first_triggered
            self.log("<imbalance> Bid- Elapsed {} seconds from imb. triggered (must wait {} seconds)".format(span.total_seconds(),self.imbalance_min_entry_sec))

        entry_cond = imbalance_bid > self.imbalance_min_threshold and (span is not None and span.seconds > self.imbalance_min_entry_sec)
        stil_on_cond =   self.imbalance_start is not None and self.imbalance_activated == _IMBALANCE_BID and imbalance_bid > self.imbalance_exit_threshold
        time_cond=self.eval_imbalance_time()

        return     (entry_cond or stil_on_cond) and  time_cond


    #endregion

    #region Public Methods

    def eval_depurate_imbalance(self):

        time_delta_last_depur_min = ((self.get_now() - self.last_imbalance_depuration).total_seconds()) / 60
        if time_delta_last_depur_min > self.imbalance_depuration_threshold:
            # 1- Guardamos un temp de los snapshot
            temp_shares_bid = self.shares_bid
            temp_shares_ask = self.shares_ask
            temp_mon_volume_bid = self.mon_volume_bid
            temp_mon_volume_ask = self.mon_volume_ask

            # 2- Restamos de las variabls de cálculo del imbalance, el snapshot que tomamos en la última depuración
            self.shares_bid -= self.shares_bid_snapshot
            self.shares_ask -= self.shares_ask_snapshot
            self.mon_volume_bid -= self.mon_volume_bid_snapshot
            self.mon_volume_ask -= self.mon_volume_ask_snapshot

            # 3- Actualizamo las variables de snapshot que serán restada la próxima depuración
            self.shares_bid_snapshot = self.shares_bid
            self.shares_ask_snapshot = self.shares_ask
            self.mon_volume_bid_snapshot = self.mon_volume_bid
            self.mon_volume_ask_snapshot = self.mon_volume_ask

            self.log("{}-Depurating imbalance calculation: NewMonVolAsk={} NewMonVolBid={}".format(
                self.get_now(), self.mon_volume_ask, self.mon_volume_bid)
                     + " NewSharesAsk={} NewSharesBid={}".format(self.shares_ask, self.shares_bid)
                     + "  OldMonVolAsk={} OldMonVolBid={}".format(temp_mon_volume_ask, temp_mon_volume_bid)
                     + "  OldSharesAsk={} OldSharesBid={}".format(temp_shares_ask, temp_shares_bid)
                     )

            self.last_imbalance_depuration = self.get_now()

    def calculate_imbalance(self, md):

        if (md.Trade is None or md.MDTradeSize is None):
            return

        self.ATR_indicator.persist_candle(md)
        self.ATR_indicator.calculate_ATR_value()

        if ((self.last_price is None or self.last_price != md.Trade)
                or (self.last_size is None or self.last_size != md.MDTradeSize)
        ):
            dist_bid = abs(md.Trade - md.BestBidPrice)
            dist_ask = abs(md.Trade - md.BestAskPrice)

            if (dist_bid < dist_ask):  # most probably it was a sell

                self.shares_bid += md.MDTradeSize
                self.mon_volume_bid += md.MDTradeSize  * md.Trade
                # self.log("New sell detected! New shares_bid={} New Mon. Volume Bid={}".format(self.shares_bid,self.mon_volume_bid))
            elif (dist_ask < dist_bid):  # most probably it was a sell

                self.shares_ask += md.MDTradeSize
                self.mon_volume_ask += md.MDTradeSize  * md.Trade
                # self.log("New buy detected! New shares_ask={} New Mon. Volume Ask={}".format(self.shares_ask,self.mon_volume_ask))

            self.last_size = md.MDTradeSize
            self.last_price = md.Trade

    def validate_imbalance_min_opx_timespan(self):

        time_delta = (self.get_now() - self.process_start)
        self.log("validate_imbalance_min_opx_timespan.timedelta={}".format(time_delta))
        return (time_delta.total_seconds() / 60) > self.imbalance_min_opx_timespan

    def imbalance_is_off(self):
        return  self.imbalance_activated == None or self.imbalance_activated == _IMBALANCE_BID_END or self.imbalance_activated == _IMBALANCE_ASK_END

    def eval_ask_imbalance_entry_conditions(self,mid_price,imbalance_ask):

        if imbalance_ask > self.imbalance_min_threshold and self.validate_imbalance_min_opx_timespan() and self.imbalance_ask_first_triggered is None:
            self.imbalance_ask_first_triggered=self.get_now()
            self.log("<imbalance>- Imbalance ask triggered at {}".format(self.imbalance_ask_first_triggered))

        return self.ask_imbalance_still_active(imbalance_ask)

    def eval_bid_imbalance_entry_conditions(self,mid_price,imbalance_bid):

        if imbalance_bid > self.imbalance_min_threshold and self.validate_imbalance_min_opx_timespan() and self.imbalance_bid_first_triggered is None:
            self.imbalance_bid_first_triggered=self.get_now()
            self.log("<imbalance>- Imbalance bid triggered at {}".format(self.imbalance_bid_first_triggered))

        return self.bid_imbalance_still_active(imbalance_bid)

    def get_imbalance_bid(self):
        tot_vol = self.mon_volume_ask + self.mon_volume_bid
        imbalance_bid = self.mon_volume_bid / tot_vol if tot_vol != 0 else 0
        return imbalance_bid

    def get_imbalance_ask(self):
        tot_vol = self.mon_volume_ask + self.mon_volume_bid
        imbalance_ask = self.mon_volume_ask / tot_vol if tot_vol != 0 else 0
        return imbalance_ask

    #endregion
