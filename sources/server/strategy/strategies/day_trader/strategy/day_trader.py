import queue
import traceback

from sources.framework.common.converters.execution_report_converter import ExecutionReportConverter, \
    ExecutionReportField
from sources.framework.common.interfaces.icommunication_module import ICommunicationModule
from sources.framework.common.abstract.base_communication_module import *
from sources.framework.common.dto.cm_state import *
from sources.framework.common.wrappers.error_wrapper import *
from sources.server.strategy.common.util.logger import Logger
from sources.server.strategy.strategies.day_trader.common.util.log_helper import LogHelper
from sources.server.strategy.strategies.day_trader.common.util.trading_signal_helper import *
from sources.server.strategy.strategies.day_trader.common.configuration.configuration import *
from sources.server.strategy.strategies.day_trader.common.converters.market_data_converter import *
from sources.framework.business_entities.positions.position import *
from sources.framework.business_entities.positions.execution_summary import *
from sources.server.strategy.common.wrappers.position_wrapper import *
import threading
import time
import uuid


class DayTrader(BaseCommunicationModule, ICommunicationModule):

    def __init__(self):#test-develop
        self.NextPostId = uuid.uuid4()
        self.LockMarketData = threading.Lock()
        self.RoutingLock = threading.Lock()
        self.SymbolsMarketData={}
        self.SingleExecutionSummaries={}
        self.PendingCancels = {}

        self.SummariesQueue = queue.Queue(maxsize=1000000)
        self.Logger=Logger("DayTraderTest")
        self.TradeSent = False
        self.OpenPos = False

    #region Error Handling Methods

    def DoLog(self, msg, message_type):
        self.Logger.log(msg)

    def ProcessCriticalError(self, exception,msg):
        self.FailureException=exception
        self.ServiceFailure=True
        self.DoLog(msg, MessageType.ERROR)

    def ProcessErrorInMethod(self,method,e, symbol=None):
        try:
            msg = "Error @{} for security {}:{} ".format(method, symbol if symbol is not None else "-",str(e))
            error = ErrorWrapper(Exception(msg))
            self.ProcessError(error)
            self.DoLog(msg, MessageType.ERROR)
        except Exception as e:
            self.DoLog("Critical error @DayTrader.ProcessError2: " + str(e), MessageType.ERROR)

    def SendToInvokingModule(self,wrapper):
        try:
           #self.CommandsModule.ProcessMessage(wrapper)
            pass
        except Exception as e:
            self.DoLog("Critical error @DayTrader.SendToInvokingModule.:{}".format(str(e)), MessageType.ERROR)

    def ProcessError(self,wrapper):
        try:
            #self.CommandsModule.ProcessMessage(wrapper)
            pass
        except Exception as e:
            self.DoLog("Critical error @DayTrader.ProcessError: " + str(e), MessageType.ERROR)

    def LoadConfig(self):
        self.Configuration = Configuration(self.ModuleConfigFile)
        return True

    #endregion

    #region Business Methods

    def DoRunPositiion(self,symbol,side,price):
        newPos = Position(PosId=self.NextPostId,
                          Security=Security(Symbol=symbol, Exchange="", SecType=None),
                          Side=side, PriceType=PriceType.FixedAmount, Qty=10, QuantityType=QuantityType.SHARES,
                          Account=None, Broker=None, Strategy=None,
                          OrderType=OrdType.Limit,
                          OrderPrice=price)

        newPos.ValidateNewPosition()

        self.SingleExecutionSummaries[self.NextPostId] = ExecutionSummary(datetime.datetime.now(), newPos)
        self.NextPostId = uuid.uuid4()

        posWrapper = PositionWrapper(newPos)
        self.OutgoingModule.ProcessMessage(posWrapper)
        self.TradeSent = True

    def EvaluateRunningTrade(self,md):

        try:
            if self.TradeSent:
                return

            prevMinuteDateTime = md.MDEntryDate - datetime.timedelta(minutes=1)

            if not self.OpenPos:
                if(prevMinuteDateTime in self.SymbolsMarketData[md.Security.Symbol]):
                    if md.Trade > self.SymbolsMarketData[md.Security.Symbol][prevMinuteDateTime].Trade:
                        self.DoRunPositiion(md.Security.Symbol,Side.Buy,md.Trade)
            else:#I am Long
                if (prevMinuteDateTime in self.SymbolsMarketData[md.Security.Symbol]):
                    if md.Trade < self.SymbolsMarketData[md.Security.Symbol][prevMinuteDateTime].Trade:
                        self.DoRunPositiion(md.Security.Symbol, Side.Sell, md.Trade)
        except Exception as e:
            msg = "Exception @DayTrader.EvaluateRunningTrade: {}!".format(str(e))
            self.ProcessCriticalError(e, msg)
            self.ProcessError(ErrorWrapper(Exception(msg)))

    def UpdateManagedPosExecutionSummary(self,summary, execReport):

        #self.ProcessExecutionPrices(potPos,execReport)
        summary.UpdateStatus(execReport)

        if summary.Position.IsFinishedPosition():
            self.WaitForFilledToArrive = False
            self.TradeSent=False
            self.OpenPos=not self.OpenPos #if I was closed I opened, if opened I close

            LogHelper.LogPositionUpdate(self, "Managed Position Finished", summary, execReport)

            if summary.Position.PosId in self.PendingCancels:
                del self.PendingCancels[summary.Position.PosId]

        else:
            LogHelper.LogPositionUpdate(self, "Managed Position Updated", summary, execReport)

        self.SummariesQueue.put(summary)


    def ProcessExecutionReport(self,wrapper):
        try:
            try:
                exec_report = ExecutionReportConverter.ConvertExecutionReport(wrapper)
            except Exception as e:
                self.DoLog("Discarding execution report with bad data: " + str(e), MessageType.INFO)
                return

            pos_id = wrapper.GetField(ExecutionReportField.PosId)

            if pos_id in self.SingleExecutionSummaries:
                summary = self.SingleExecutionSummaries[pos_id]
                self.UpdateManagedPosExecutionSummary(summary, exec_report)

        except Exception as e:
            traceback.print_exc()
            self.DoLog("Critical error @DayTrader.ProcessExecutionReport: " + str(e), MessageType.ERROR)
            return CMState.BuildFailure(self, Exception=e)
        finally:
            if self.RoutingLock.locked():
                self.RoutingLock.release()


    def ProcessMarketData(self,wrapper):

        try:
            self.LockMarketData.acquire()
            md = MarketDataConverter.ConvertMarketData(wrapper)

            if md.Security.Symbol not in self.SymbolsMarketData:
                self.SymbolsMarketData[md.Security.Symbol]={}

            self.SymbolsMarketData[md.Security.Symbol][md.MDEntryDate] = md

            self.EvaluateRunningTrade(md)

            self.DoLog("Marktet Data successfully loaded for symbol {} DateTime={} open={} high={} close= {} low={}"
                       .format(md.Security.Symbol,md.MDEntryDate,md.OpeningPrice,md.TradingSessionHighPrice,
                               md.Trade,md.TradingSessionLowPrice),MessageType.DEBUG)

        except Exception as e:
            self.ProcessErrorInMethod("@DayTrader.ProcessMarketData", e, None)
        finally:
            if self.LockMarketData.locked():
                self.LockMarketData.release()

    #endregion

    def ProcessMessage(self, wrapper):
        try:
            raise Exception("DayTrader.ProcessMessage: Not prepared for routing message {}".format(wrapper.GetAction()))
        except Exception as e:
            self.DoLog("Critical error @DayTrader.ProcessMessage: " + str(e), MessageType.ERROR)
            return CMState.BuildFailure(self, Exception=e)

    def ProcessIncoming(self, wrapper):
        try:
            if wrapper.GetAction() == Actions.MARKET_DATA:
                threading.Thread(target=self.ProcessMarketData, args=(wrapper,)).start()
                return CMState.BuildSuccess(self)
            else:
                raise Exception("ProcessIncoming: Not prepared for routing message {}".format(wrapper.GetAction()))
        except Exception as e:
            self.DoLog("Critical error @DayTrader.ProcessIncoming: " + str(e), MessageType.ERROR)
            return CMState.BuildFailure(self, Exception=e)

    def ProcessOutgoing(self, wrapper):
        try:
            if wrapper.GetAction() == Actions.EXECUTION_REPORT:
                threading.Thread(target=self.ProcessExecutionReport, args=(wrapper,)).start()
                return CMState.BuildSuccess(self)
            else:
                raise Exception("ProcessOutgoing: Not prepared for routing message {}".format(wrapper.GetAction()))
        except Exception as e:
            self.DoLog("Critical error @DayTrader.ProcessOutgoing: " + str(e), MessageType.ERROR)
            return CMState.BuildFailure(self, Exception=e)

    def Initialize(self, pInvokingModule, pConfigFile):
        self.ModuleConfigFile = pConfigFile
        self.InvokingModule = pInvokingModule
        self.DoLog("DayTrader  Initializing", MessageType.INFO)

        if self.LoadConfig():

            self.IncomingModule = self.InitializeModule(self.Configuration.IncomingModule, self.Configuration.IncomingModuleConfigFile)

            self.OutgoingModule = self.InitializeModule(self.Configuration.OutgoingModule,
                                                        self.Configuration.OutgoingModuleConfigFile)

            time.sleep(self.Configuration.PauseBeforeExecutionInSeconds)

            self.DoLog("DayTrader Successfully initialized", MessageType.INFO)

            return CMState.BuildSuccess(self)

        else:
            msg = "Error initializing Day Trader"
            self.DoLog(msg, MessageType.ERROR)
            return CMState.BuildFailure(self,errorMsg=msg)
