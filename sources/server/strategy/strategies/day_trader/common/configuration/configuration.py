import configparser

class Configuration:
    def __init__(self, configFile):
        config = configparser.ConfigParser()
        config.read(configFile)

        self.PauseBeforeExecutionInSeconds = int( config['DEFAULT']['PAUSE_BEFORE_EXECUTION_IN_SECONDS'])
        #self.DBConectionString = config['DB']['CONNECTION_STRING']

        self.IncomingModule = config['DEFAULT']['INCOMING_MODULE']
        self.IncomingModuleConfigFile = config['DEFAULT']['INCOMING_CONFIG_FILE']

        self.OutgoingModule = config['DEFAULT']['OUTGOING_MODULE']
        self.OutgoingModuleConfigFile = config['DEFAULT']['OUTGOING_CONFIG_FILE']