import logging
import os
from logging.handlers import TimedRotatingFileHandler


class Logger:

    def __init__(self,name):#test-develop
        self.Name=name
        self.logger_started = False
        self.prepare_logs(name)

    def prepare_logs(self, name):
        self.logger = logging.getLogger(name)

        log_path = os.path.join("logs", name)

        main_formatter = logging.Formatter(
            fmt='%(asctime)s [%(module)s %(levelname)s] %(message)s',
            datefmt='%Y-%m-%d %H:%M:%S')

        console_handler = logging.StreamHandler()
        file_handler = TimedRotatingFileHandler(
            filename=log_path, when='d', backupCount=20)

        for handler in [console_handler, file_handler]:
            handler.setFormatter(main_formatter)
            self.logger.addHandler(handler)
        self.logger.setLevel(20)  # 20 = normal
        self.logger_started = True

    def log(self, str):
        if not self.logger_started:
            self.prepare_logs(self.ProxyName)
        self.logger.info(str)