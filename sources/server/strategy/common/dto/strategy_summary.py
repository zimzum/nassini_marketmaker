
class strategy_summary():

    #region Constructors

    def __init__(self,start,end,type,date,profit,net_buys,net_sells,net_shares,imbalance_activated,imbalance_start,
                      imbalance_end,nominal_volatiliy,imbalance_bid,imbalance_ask,market_price,
                      new_high_lows_start,new_high_lows_end,new_high_lows_type,reggr_slope,candle_breadth):
        self.start=start
        self.end=end
        self.date=date
        self.type=type
        self.backtest=None
        self.profit=profit
        self.net_buys=net_buys
        self.net_sells=net_sells
        self.net_shares=net_shares
        self.imbalance_activated=imbalance_activated
        self.imbalance_start=imbalance_start
        self.imbalance_end=imbalance_end
        self.nominal_volatility=nominal_volatiliy
        self.imbalance_bid=imbalance_bid
        self.imbalance_ask=imbalance_ask
        self.market_price=market_price
        self.new_high_lows_start=new_high_lows_start
        self.new_high_lows_end=new_high_lows_end
        self.new_high_lows_type=new_high_lows_type
        self.reggr_slope=reggr_slope
        self.candle_breadth=candle_breadth


    #endregion
