from sources.server.generic_orderrouter.market_order_router.common.wrappers.order_wrapper import OrderWrapper, Actions, \
    OrderField


class UpdateOrderWrapper(OrderWrapper):
    def __init__(self,pOrigClOrdId, pClOrdId,pOrderPrice):
        super().__init__(None, None, pClOrdId,None,None,None,None,None,None,None,None,pOrderPrice)
        self.OrigClOrdId= pOrigClOrdId


    def GetAction(self):
        """

        Returns:

        """
        return Actions.UPDATE_ORDER

    def GetField(self, field):
        """

        Args:
            field ():

        Returns:

        """
        if field is None:
            return None

        if field == OrderField.OrigClOrdID:
            return self.OrigClOrdId
        else:
            return super().GetField(field)
