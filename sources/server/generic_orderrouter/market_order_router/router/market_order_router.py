import importlib
import os
import sys
import time
import threading
import traceback
import winsound

from sources.framework.common.abstract.base_communication_module import BaseCommunicationModule
from sources.framework.common.interfaces.icommunication_module import ICommunicationModule
from sources.framework.common.logger.message_type import MessageType
from sources.framework.util.generic.time_util import time_util
from sources.server.generic_orderrouter.market_order_router.common.configuration.configuration import *
from sources.framework.common.dto.cm_state import *
from sources.server.generic_orderrouter.common.converters.position_converter import *
from sources.framework.common.wrappers.cancel_order_wrapper import *
from sources.framework.common.wrappers.order_cancel_reject_wrapper import *
from sources.server.generic_orderrouter.market_order_router.common.wrappers.new_order_wrapper import *
from sources.server.generic_orderrouter.market_order_router.common.wrappers.execution_report_list_request_wrapper import *
from sources.server.generic_orderrouter.common.converters.execution_report_list_converter import *
from sources.framework.common.enums.CxlRejReason import *
from sources.framework.common.enums.CxlRejResponseTo import *
from sources.server.generic_orderrouter.common.wrappers.position_list_wrapper import *
from sources.server.generic_orderrouter.market_order_router.common.wrappers.execution_report_wrapper import \
    ExecutionReportWrapper as GenericERWrapper
from sources.server.generic_orderrouter.market_order_router.common.wrappers.update_order_wrapper import \
    UpdateOrderWrapper


class MarketOrderRouter(BaseCommunicationModule, ICommunicationModule):

    def __init__(self):
        self.Name = "Market Order Router"
        self.OutgoingModule = None
        self.Positions = {}
        self.PositionsLock = threading.Lock()

    #region Private Methods

    def LoadConfig(self):
        self.Configuration = Configuration(self.ModuleConfigFile)
        return True

    def ProcessError(self,wrapper):
        self.InvokingModule.ProcessOutgoing(wrapper)
        return CMState.BuildSuccess(self)

    def EvalRemoval(self,execReport):

        if not execReport.IsOpenOrder():
            self.DoLog("Removing no longer needed order {}".format(execReport.Order.ClOrdId),MessageType.INFO)
            del self.Positions[execReport.Order.ClOrdId]


    def FetchByClOrdId(self,clOrdId):
        return self.Positions[clOrdId] if clOrdId in self.Positions else None
        #return next((x for x in self.Positions.values() if x.GetLastOrder().ClOrdId == clOrdId), None)

    def FetchByOrderId(self,orderId):
        return next((x for x in self.Positions.values() if x.GetLastOrder().OrderId == orderId), None)

    #endregion


    #region Public Methods

    def ProcessOrderCancelRejectFromOutgoingModule(self,wrapper):
        self.InvokingModule.ProcessOutgoing(wrapper)
        return CMState.BuildSuccess(self)

    def ProcessExecutionReportList(self, wrapper):
        try:

            positions = ExecutionReportListConverter.GetPositionsFromExecutionReportList(self, wrapper)

            self.PositionsLock.acquire(blocking=True)
            for pos in positions:
                if pos.GetLastOrder() is not None:
                    pos.GetLastOrder().ClOrdId = pos.PosId
                    self.Positions[pos.PosId] = pos
                else:
                    self.DoLog("Could not find order for pre existing position on execution report initial load. PosId = {}".format(pos.PosId), MessageType.ERROR)
            self.PositionsLock.release()

            pos_list_wrapper = PositionListWrapper(positions)
            self.InvokingModule.ProcessOutgoing(pos_list_wrapper)
            return CMState.BuildSuccess(self)
        except Exception as e:
            errWrapper = PositionListWrapper([],e)
            self.InvokingModule.ProcessOutgoing(errWrapper)
            self.DoLog("Error processing execution report list:{}".format(e), MessageType.ERROR)
            return CMState.BuildFailure(e)
        finally:
            if self.PositionsLock.locked():
                self.PositionsLock.release()

    def UpdateNewPosition(self,old_pos,execReport):
        new_pos = old_pos.Clone()

        new_pos.PosId=execReport.ClOrdId

        new_pos.SetPositionStatusFromExecution(execReport)

        self.AppendNewOrder(new_pos, execReport.Order.OrigClOrdId, execReport.ClOrdId, execReport.OrderId, execReport.OrdStatus)

        self.Positions[execReport.ClOrdId] = new_pos

        return new_pos

    def AppendNewOrder(self,pos,orig_cl_ord_id,cl_ord_id,order_id,status):
        last_order=pos.GetLastOrder()

        self.DoLog("<generic_order_router> upd. old order {} with new order {} (status={})".format(last_order.ClOrdId,cl_ord_id,status),MessageType.INFO)

        new_order= Order(ClOrdId=cl_ord_id,OrigClOrdId=orig_cl_ord_id,
                         Security=last_order.Security,SettlType=last_order.SettlType,
                         Side=last_order.Side,Exchange=last_order.Exchange,OrdType=last_order.OrdType,
                         QuantityType=last_order.QuantityType,OrderQty=last_order.OrderQty,
                         PriceType=last_order.PriceType,Price=last_order.Price,Currency=last_order.Currency,
                         TimeInForce=last_order.TimeInForce,Account=last_order.Account,OrdStatus=status,
                         Broker=last_order.Broker,Strategy=last_order.Strategy,MarketArrivalTime=last_order.MarketArrivalTime)

        new_order.OrderId=order_id
        pos.Orders.append(new_order)

    def DoSendToStrategy(self,posId,wrapper):
        processed_exec_report = GenericERWrapper(posId, wrapper)
        if self.PositionsLock.locked():
            self.PositionsLock.release()
        self.InvokingModule.ProcessOutgoing(processed_exec_report)

    def ProcessExecutionReport(self, wrapper):
        try:
            execReport = ExecutionReportConverter.ConvertExecutionReport(wrapper)
            self.DoLog("DB-gen_order_router.ProcessExecutionReport {}".format(time_util.full_now()),MessageType.INFO)
            self.PositionsLock.acquire(blocking=True)
            pos=self.FetchByClOrdId( execReport.ClOrdId)
            #pos = next(iter(list(filter(lambda x:x.GetLastOrder() is not None and  x.GetLastOrder().ClOrdId == execReport.ClOrdId, self.Positions.values()))), None)

            if pos is not None:
                self.DoLog("<generic_order_router> recv tracked execution report for OrderId={} (ClOrdId={}) and Status={}".format(execReport.OrderId,execReport.ClOrdId,execReport.OrdStatus),MessageType.INFO)
                pos.UpdateLastOrder(execReport.OrderId,execReport.ClOrdId,execReport.OrdStatus)
                self.DoSendToStrategy(pos.PosId,wrapper)
            elif self.FetchByClOrdId(execReport.Order.OrigClOrdId) is not None:
                #we have a cancel/replace
                self.DoLog("<generic_order_router> recv upd. execution report for OrderId={} (ClOrdId={}) and Status={}".format(execReport.OrderId, execReport.ClOrdId, execReport.OrdStatus), MessageType.INFO)
                old_pos=self.FetchByClOrdId(execReport.Order.OrigClOrdId)
                new_pos=self.UpdateNewPosition(old_pos,execReport)
                self.DoSendToStrategy(new_pos.PosId,wrapper)
            elif self.FetchByOrderId(execReport.OrderId):
                self.DoLog("<generic_order_router> recv not tracked cl_ord_id <but order_id> execution report for OrderId={} (ClOrdId={}) and Status={}".format(execReport.OrderId, execReport.ClOrdId, execReport.OrdStatus), MessageType.INFO)
                pos=self.FetchByOrderId(execReport.OrderId)
                pos.UpdateLastOrder(execReport.OrderId,execReport.ClOrdId,execReport.OrdStatus)
                self.DoSendToStrategy(pos.PosId,wrapper)

            else:
                self.DoLog("<generic_order_router> recv not tracked execution report for OrderId={} (ClOrdId={}) and Status={}".format(execReport.OrderId, execReport.ClOrdId, execReport.OrdStatus), MessageType.INFO)
                #Execution report for unknown position. We create it and it will be the strategy that will decide what to do
                newPos = ExecutionReportListConverter.CreatePositionFromExecutionReport(self,wrapper)
                #self.Positions[newPos.PosId]=newPos
                self.DoSendToStrategy(str(uuid.uuid4()),wrapper)
                self.DoLog( "Received ExecutionReport for unknown ClOrdId ={} OrderId= {}. Sending as External trading".format(execReport.ClOrdId, execReport.OrderId),MessageType.INFO)

        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            winsound.Beep(5000, 2000)
            self.DoLog("Error processing execution report:{}".format(e), MessageType.ERROR)
            self.DoLog("type={} name={} line={}".format(exc_type, fname, exc_tb.tb_lineno),MessageType.ERROR)
        finally:
            self.DoLog("DB-gen_order_router.ProcessExecutionReport.exit {}".format(time_util.full_now()),MessageType.INFO)
            #self.EvalRemoval(execReport)
            if(self.PositionsLock.locked()):
                self.PositionsLock.release()

    def ProcessOrderCancelReject(self,wrapper,ex):
        try:

            posId = wrapper.GetField(PositionField.PosId)
            msg = "Error sending order cancel request: {} ".format(str(ex))

            self.PositionsLock.acquire(blocking=True)

            if posId in self.Positions:
                posToCancel = self.Positions[posId]
                rejWrapper = OrderCancelRejectWrapper(pText=msg, pCxlRejResponseTo=CxlRejResponseTo.OrderCancelRequest,
                                                      pCxlRejReason=CxlRejReason.Other,
                                                      pOrder = None,pClOrdId=posToCancel.GetLastOrder().ClOrdId,
                                                      pOrigClOrdId=None, pOrderId=posToCancel.GetLastOrder().OrderId)

                self.PositionsLock.release()
                self.InvokingModule.ProcessOutgoing(rejWrapper)
            else:
                rejWrapper = OrderCancelRejectWrapper(pText=msg, pCxlRejResponseTo=CxlRejResponseTo.OrderCancelRequest,
                                                      pCxlRejReason=CxlRejReason.Other,
                                                      pOrder=None, pClOrdId=None, pOrigClOrdId=None, pOrderId=None)
                self.PositionsLock.release()
                self.InvokingModule.ProcessOutgoing(rejWrapper)
        except Exception as e:
            self.DoLog("Critical Error processing order cancel reject: {}".format(str(e)))
        finally:
            if self.PositionsLock.locked():
                self.PositionsLock.release()

    def ProcessPositionCancelThread(self, wrapper):
        try:
            posId = wrapper.GetField(PositionField.PosId)

            if posId in self.Positions:
                posToCancel = self.Positions[posId]
                if(posToCancel.GetLastOrder() is not None):
                    cxlWrapper= CancelOrderWrapper(posToCancel.GetLastOrder().ClOrdId,posToCancel.GetLastOrder().OrderId)
                    self.OutgoingModule.ProcessMessage(cxlWrapper)
                else:
                    raise Exception("Received cancellation for position {} which doesn't have an order to cancel".format(posId))

            else:
                raise Exception("Received cancellation request for unknown position {}".format(posId))

        except Exception as e:
            self.ProcessOrderCancelReject(wrapper,e)

    def ProcessPositionCancel(self,wrapper):
        threading.Thread(target=self.ProcessPositionCancelThread, args=(wrapper,)).start()
        return CMState.BuildSuccess(self)

    def ProcessPositionListRequest(self, wrapper):
        exec_report_list_req_wrapper = ExecutionReportListRequestWrapper()
        return self.OutgoingModule.ProcessMessage(exec_report_list_req_wrapper)

    def ProcessUpdatePosition(self,wrapper):

        origPosId = wrapper.GetField(PositionField.OrigPosId)
        posId = wrapper.GetField(PositionField.PosId)
        orderPrice = wrapper.GetField(PositionField.OrderPrice)

        try:

            self.DoLog("<generic_market_making>Updating order {} with new ClOrdId {} and Price {}".format(origPosId,posId,orderPrice),MessageType.INFO)

            self.PositionsLock.acquire(blocking=True)

            if origPosId in self.Positions:

                pos= self.Positions[origPosId]

                if pos.GetLastOrder() is not None:

                    order = pos.GetLastOrder()
                    self.DoLog("<generic_market_making> Found last order with OrigClOrdId={} ClOrdId={}".format(order.OrigClOrdId,order.ClOrdId),MessageType.INFO)
                    newOrder = Order(ClOrdId=posId,OrigClOrdId=origPosId,Security=order.Security,SettlType=order.SettlType,
                                     Side=order.Side,Exchange=order.Exchange,OrdType=order.OrdType,
                                     QuantityType=order.QuantityType,OrderQty=order.OrderQty,
                                     PriceType=order.PriceType,Price=orderPrice,Currency=order.Currency,
                                     TimeInForce=order.TimeInForce,Account=order.Account,OrdStatus=order.OrdStatus,
                                     Broker=order.Broker,Strategy=order.Strategy,MarketArrivalTime=order.MarketArrivalTime)
                    pos.AppendOrder(newOrder)
                    self.Positions[posId] = pos
                    wrapper = UpdateOrderWrapper(pOrigClOrdId=origPosId,pClOrdId=posId,pOrderPrice=orderPrice)

                    if self.PositionsLock.locked():
                        self.PositionsLock.release()

                    return self.OutgoingModule.ProcessMessage(wrapper)
                else:
                    raise Exception("Could not find order for PositionId {}".format(origPosId))

            else:
                raise Exception("Could not find a position for PosId {}".format(origPosId))

        finally:
            if self.PositionsLock.locked():
                self.PositionsLock.release()

        pass

    def ProcessNewPosition(self, wrapper):
        try:

            self.PositionsLock.acquire(blocking=True)
            new_pos = PositionConverter.ConvertPosition(self, wrapper)
            # In this Generic Order Router ClOrdID=PosId
            self.Positions[new_pos.PosId] = new_pos
            order_wrapper = NewOrderWrapper(new_pos.Security.Symbol, new_pos.OrderQty, new_pos.PosId,
                                            new_pos.Security.Currency,new_pos.Security.SecurityType,new_pos.Security.Exchange,
                                            new_pos.Side, new_pos.Account, new_pos.Broker,
                                            new_pos.Strategy, new_pos.OrderType, new_pos.OrderPrice)
            new_pos.Orders.append(Order(ClOrdId=new_pos.PosId,Security=new_pos.Security,SettlType=SettlType.Regular,
                                        Side=new_pos.Side,Exchange=new_pos.Exchange,OrdType=OrdType.Market,
                                        QuantityType=new_pos.QuantityType,OrderQty=new_pos.Qty,PriceType=new_pos.PriceType,
                                        Price=None,StopPx=None,Currency=new_pos.Security.Currency,
                                        TimeInForce=TimeInForce.Day,Account=new_pos.Account,
                                        OrdStatus=OrdStatus.PendingNew,Broker=new_pos.Broker,Strategy=new_pos.Strategy))
            self.PositionsLock.release()
            return self.OutgoingModule.ProcessMessage(order_wrapper)
        except Exception as e:
            raise e
        finally:
            if self.PositionsLock.locked():
                self.PositionsLock.release()

    def ProcessCandleBar(self,wrapper):
        self.InvokingModule.ProcessIncoming(wrapper)

    def ProcessMarketData(self, wrapper):
        self.InvokingModule.ProcessIncoming(wrapper)

    def ProcessHistoricalPrices(self,wrapper):
        self.InvokingModule.ProcessIncoming(wrapper)

    def ProcessIncoming(self, wrapper):
        try:

            if wrapper.GetAction() == Actions.CANDLE_BAR_DATA:
                self.ProcessCandleBar(wrapper)
                return CMState.BuildSuccess(self)
            elif wrapper.GetAction() == Actions.MARKET_DATA:
                self.ProcessMarketData(wrapper)
                return CMState.BuildSuccess(self)
            elif wrapper.GetAction() == Actions.HISTORICAL_PRICES:
                self.ProcessHistoricalPrices(wrapper)
                return CMState.BuildSuccess(self)
            else:
                raise Exception("ProcessOutgoing: GENERIC Order Router not prepared for outgoing message {}".format(
                    wrapper.GetAction()))

        except Exception as e:
            self.DoLog("Error running ProcessOutgoing @GenericOrderRouter module:{}".format(str(e)), MessageType.ERROR)
            return CMState.BuildFailure(self, Exception=e)

    def ProcessOutgoing(self, wrapper):
        try:

            if wrapper.GetAction() == Actions.EXECUTION_REPORT:
                self.ProcessExecutionReport(wrapper)
                #threading.Thread(target=self.ProcessExecutionReport, args=(wrapper,)).start()
                return CMState.BuildSuccess(self)
            elif wrapper.GetAction() == Actions.EXECUTION_REPORT_LIST:
                return self.ProcessExecutionReportList(wrapper)
            elif wrapper.GetAction() == Actions.ORDER_CANCEL_REJECT:
                return self.ProcessOrderCancelRejectFromOutgoingModule(wrapper)
            elif wrapper.GetAction() == Actions.ERROR:
                return self.ProcessError(wrapper)
            else:
                raise Exception("ProcessOutgoing: GENERIC Order Router not prepared for outgoing message {}".format(
                    wrapper.GetAction()))

        except Exception as e:
            self.DoLog("Error running ProcessOutgoing @GenericOrderRouter module:{}".format(str(e)), MessageType.ERROR)
            return CMState.BuildFailure(self, Exception=e)

    def ProcessMessage(self, wrapper):
        try:

            if wrapper.GetAction() == Actions.NEW_POSITION:
                return self.ProcessNewPosition(wrapper)
            elif wrapper.GetAction() == Actions.POSITION_LIST_REQUEST:
                return self.ProcessPositionListRequest(wrapper)
            elif wrapper.GetAction() == Actions.UPDATE_POSITION:
                return self.ProcessUpdatePosition(wrapper)
            elif wrapper.GetAction() == Actions.CANCEL_POSITION:
                return self.ProcessPositionCancel(wrapper)
            elif wrapper.GetAction() == Actions.CANCEL_ALL_POSITIONS:
                return self.OutgoingModule.ProcessMessage(wrapper)
            elif wrapper.GetAction() == Actions.CANDLE_BAR_REQUEST:
                return self.OutgoingModule.ProcessMessage(wrapper)
            elif wrapper.GetAction() == Actions.MARKET_DATA_REQUEST:
                return self.OutgoingModule.ProcessMessage(wrapper)
            elif wrapper.GetAction() == Actions.ORDER_MASS_STATUS_REQUEST:
                return self.OutgoingModule.ProcessMessage(wrapper)
            elif wrapper.GetAction() == Actions.CANCEL_ORDER:
                return self.OutgoingModule.ProcessMessage(wrapper)
            else:
                raise Exception("Generic Order Router not prepared for routing message {}".format(wrapper.GetAction()))

            return CMState.BuildSuccess(self)

        except Exception as e:
            self.DoLog("Error running ProcessMessage @GenericOrderRouter module:{}".format(str(e)), MessageType.ERROR)
            return CMState.BuildFailure(self, Exception=e)

    def Initialize(self, pInvokingModule, pConfigFile):
        self.InvokingModule = pInvokingModule
        self.ModuleConfigFile = pConfigFile

        try:
            self.DoLog("Initializing Generic Market Order Router", MessageType.INFO)
            if self.LoadConfig():
                self.OutgoingModule = self.InitializeModule(self.Configuration.OutgoingModule,self.Configuration.OutgoingConfigFile)
                self.DoLog("Generic Market Order Router Initialized", MessageType.INFO)
                return CMState.BuildSuccess(self)
            else:
                raise Exception("Unknown error initializing config file for Generic Market Order Router")

        except Exception as e:

            self.DoLog("Error Loading Generic Market Order Router module:{}".format(str(e)), MessageType.ERROR)
            return CMState.BuildFailure(self, Exception=e)

    #endregion

