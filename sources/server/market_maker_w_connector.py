from sources.framework.common.logger.logger import *
from sources.framework.common.logger.message_type import MessageType
from sources.framework.common.enums.Actions import *
from sources.framework.common.enums.fields.execution_report_field import *
from sources.framework.common.enums.fields.market_data_field import *
from sources.server.strategy.strategies.market_maker.strategy.market_maker import MarketMaker
import logging

class MainApp:

    def __init__(self):
        self.processor = None
        #self.logger = Logger("../../configs/logger.ini")
        self.prepare_logs("server-market-maker")
        #self.logger.use_timed_rotating_file_handler()

    def prepare_logs(self,name):
        self.logger = logging.getLogger(name)

        log_path = os.path.join("logs", name)

        main_formatter = logging.Formatter(
            fmt='%(asctime)s [%(module)s %(levelname)s] %(message)s',
            datefmt='%Y-%m-%d %H:%M:%S')

        console_handler = logging.StreamHandler()
        file_handler = TimedRotatingFileHandler(
            filename=log_path, when='d', backupCount=20)

        for handler in [console_handler, file_handler]:
            handler.setFormatter(main_formatter)
            self.logger.addHandler(handler)
        self.logger.setLevel(20)  # 20 = normal
        self.logger_started=True

    def DoLog(self, msg, message_type):
        """ Log every message from strategy and other modules

        Args:
            msg (String): Message to log.
            message_type (:obj:`Enum`): MessageType (DEBUG, INFO, WARNING, ..).
        """
        #self.logger.print(msg,message_type)
        if message_type==MessageType.INFO:
            self.logger.info(msg)
        elif message_type==MessageType.ERROR:
            self.logger.error(msg)
        else:
            self.logger.info(msg)
        #print(msg)

    def ProcessOutgoing(self, wrapper):
        """ To Process Order Routing Module messages.

        Args:
            wrapper (:obj:`Wrapper`): Generic wrapper to communicate strategy with other modules.
        """
        if wrapper.GetAction() == Actions.EXECUTION_REPORT:
            symbol = wrapper.GetField(ExecutionReportField.Symbol)
            ord_status = wrapper.GetField(ExecutionReportField.OrdStatus)
            self.DoLog("MainApp: Received Exec Report {} for symbol {}".format(ord_status, symbol), MessageType.INFO)
        elif wrapper.GetAction() == Actions.MARKET_DATA:
            symbol = wrapper.GetField(MarketDataField.Symbol)
            last = wrapper.GetField(MarketDataField.Trade)
            self.DoLog("MainApp: Received Market Data Trade {} for symbol {}".format(last, symbol), MessageType.INFO)
        else:
            self.DoLog("MainApp: Not prepared for routing message {}".format(wrapper.GetAction()), MessageType.INFO)

    def run(self):
        """Create new Simple CSV Processor object strategy, then initialize

        """
        self.processor = MarketMaker()
        self.processor.Initialize(self, "../../configs/strategies/market_maker/client/market_maker.ini")


if __name__ == '__main__':
    app = MainApp()
    app.run()
    input()