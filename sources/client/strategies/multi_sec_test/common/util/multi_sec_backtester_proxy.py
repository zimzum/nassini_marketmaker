from sources.client.framework.business_entities.backtest_parameter import *
from sources.client.strategies.market_maker.business_entities.market_making_summary import *
from sources.client.strategies.market_maker.data_access_layer.market_making_summary_manager import *
from sources.client.framework.util.backtester_proxy import *
from sources.client.framework.enums.backtest_period import *
import statistics
import threading
import queue
import time
from scipy import stats

class multi_sec_backtester_proxy(backtester_proxy):

    def __init__(self):
        self.data = self.data[0]
        self.FakeMarket = None
        #self.market_making_summary_manager=None
        self.periodic_persist=None

    #region Init Methods
    def on_init_backtester(self,p_configuration):
        self.symbols=p_configuration.symbol.split(",")
        self.logger_started = False
        self.ProxyName="MultiSecBacktest"
        pass
    def get_config_params(self, backtest):
        prms = []

        #retrieve all the config params from strategy for persistance

        return prms

    #endregion

    #region Periodic Methods

    def persist_summary(self):
        #TODO persist summaries thread
        pass

    def on_periodic_change(self,period ,backtest,p_start,p_end):
        #Create summaries with statistical parameters
        #persist and clean them
        pass

    def on_hourly_change(self, backtest,p_start,p_end):
        if self. periodic_persist==backtest_period.Hourly.value:
            self.on_periodic_change(backtest_period.Hourly ,backtest,p_start,p_end)

    def on_minute_change(self, backtest,p_start,p_end):
        if self.periodic_persist==backtest_period.Minute.value:
            self.on_periodic_change(backtest_period.Minute, backtest,p_start,p_end)

    def on_daily_change(self, backtest,p_start,p_end):
        if self.periodic_persist==backtest_period.Daily.value:
            self.on_periodic_change(backtest_period.Daily, backtest,p_start,p_end)



    def get_cum_profit(self):
        profit = 0
        for symbol in self.symbols:
            profit += self.profit[symbol]
        return profit

    def get_net_buys(self):
        net_buys = 0
        for symbol in self.symbols:
            net_buys += self.net_buys[symbol]
        return net_buys

    def get_net_sells(self):
        net_sells = 0
        for symbol in self.symbols:
            net_sells += self.net_sells[symbol]
        return net_sells

    def get_net_long_contracts(self, symbol):
        return self.net_long_contracts[symbol] if self.net_long_contracts[symbol] is not None else 0

    def get_net_short_contracts(self, symbol):
        return self.net_short_contracts[symbol] if self.net_short_contracts[symbol] is not None else 0

    def get_net_long_contracts_for_symbol(self, symbol):
        return self.get_net_long_contracts(symbol)  # it's a one security strategy

    def get_net_short_contracts_for_symbol(self, symbol):
        return self.get_net_short_contracts(symbol)  # it's a one security strategy

    def get_summary(self):
        return "<BACKTESTER SUMMARY! cum_profit={} day_profit={} cum_buys={} cum_sells={}".format(self.get_cum_profit(),
                                                                                                  self.get_cum_profit(),
                                                                                                  self.get_net_buys(),
                                                                                                  self.get_net_sells())

    #endregion

