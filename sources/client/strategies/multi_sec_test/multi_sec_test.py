from sources.client.framework.util.Strategy import *
from sources.client.strategies.multi_sec_test.common.util.multi_sec_backtester_proxy import *
import uuid
import datetime
import threading
import math
import traceback
import decimal
import time
#IMPORTANT
#LOOK FOR COMMENTS LIKE "TO CHANGE IN PROD ENV" to fully fit the backtrader fwk implementation
#to Aruants impl


class market_maker(multi_sec_backtester_proxy):
#class market_maker(Strategy):

    # TO CHANGE IN PROD ENV --> TO UNCOMMENT IN PROD ENV
    def __init__(self, symbols_csv=""):
        self.name = "Market Maker Mult. Test"
        self.symbols=symbols_csv.split(",")

    # endregion

    def backtest_prestart(self,start_time):

        self.now = start_time
        self.process_start = start_time
        self.last_imbalance_depuration = start_time

    def start(self):
        self.log("Starting Nassini - Market Maker")

        # region Administrative
        self.buy_order_id = {}
        self.sell_order_id = {}
        self.order_symbols={}
        self.pending_cancel_replaces = {}
        self.impl_replace = True
        self.data0=None
        self.data1=None

        self.max_long_qty={}
        self.max_short_qty={}
        self.defaut_spread = {}
        self.CumQtyDict = {}
        # endregion

        # region Calculated Contract
        self.last_market_price ={}
        self.routing_long = {}
        self.routing_short = {}
        self.net_shares = {}
        self.net_sells = {}
        self.net_buys = {}
        self.net_long_contracts = {}
        self.net_short_contracts = {}
        self.profit={}

        for symbol in self.symbols:
            self.buy_order_id[symbol] = None
            self.sell_order_id[symbol] = None
            self.last_market_price[symbol] = 0
            self.routing_long[symbol] = 0
            self.routing_short[symbol]  = 0
            self.net_shares[symbol]  = 0
            self.net_sells[symbol]  = 0
            self.net_buys[symbol]  = 0
            self.net_long_contracts[symbol]  = 0
            self.net_short_contracts[symbol]  = 0
            self.profit[symbol]  = 0
            self.max_long_qty[symbol]  = 10 #You can apply different values for diff securities
            self.max_short_qty[symbol]  = -10
            if symbol.startswith("RFX"):
                self.defaut_spread[symbol]  = decimal.Decimal(10)
            elif symbol.startswith("DO"):
                self.defaut_spread[symbol]  = decimal.Decimal(0.01)
            else:
                self.defaut_spread[symbol] = decimal.Decimal(10)

        self.now = None
        self.process_start = self.get_now()

        # endregion
        self._routing_lock = threading.Lock()

        self.orders = {}

    #region MD

    def get_trade_contract(self, data):

        if data is None:
            return None
        trade_contract = None
        if hasattr(data, 'tradecontract'):
            trade_contract = data.tradecontract  # ArQuants
        elif hasattr(data, '_name'):
            trade_contract = data._name  # backtrader
        else:
            raise Exception("ERROR: Could not find trade contract in data structure!")

        return trade_contract

    def get_market_price(self, data):
        if data is None:
            return None
        if len(data.last_px)>0:
            return data.last_px[0]
        else:
            return None

    #endregion

    # region Order Routing

    def get_order_id(self, order):
        orderId = None
        if hasattr(order, 'm_orderId'):
            orderId = order.m_orderId  # ArQuants
        elif hasattr(order, 'tradeid'):
            orderId = order.tradeid  # backtrader
        else:
            raise Exception("ERROR: Could not find order id in  order!")

        return orderId

    def do_buy(self,data, qty, price):
        symbol=self.get_trade_contract( data)
        self.buy_order_id[symbol] = uuid.uuid4()
        self.order_symbols[str(self.buy_order_id[symbol])]=symbol
        new_order = self.buy(data=data,
                             size=qty,
                             price=price,
                             exectype=Order.Limit,
                             tradeid=self.buy_order_id[symbol])

        if (hasattr(new_order, 'm_orderId')):
            self.log("<buy>-Assigning buy m_orderId {} for symbol {}".format(new_order.m_orderId,symbol))
            self.buy_order_id[symbol] = new_order.m_orderId
            self.order_symbols[str(new_order.m_orderId)] = symbol

        self.routing_long[symbol] += qty
        self.log(
            "<buy>-{}-Sending buy order {} for qty {} price {}  for symbol".format(self.get_now(), self.buy_order_id[symbol],
                                                                                    qty, round(price, 2),symbol))

    def do_sell(self,data, qty, price):
        symbol = self.get_trade_contract( data)
        self.sell_order_id[symbol] = uuid.uuid4()
        self.order_symbols[str(self.sell_order_id[symbol])] = symbol
        new_order = self.sell(data=data,
                              size=qty,
                              price=price,
                              exectype=Order.Limit,
                              tradeid=self.sell_order_id[symbol])

        if (hasattr(new_order, 'm_orderId')):
            self.log("<sell>-Assigning sell m_orderId {} for symbol".format(new_order.m_orderId, symbol))
            self.sell_order_id[symbol] = new_order.m_orderId
            self.order_symbols[str(new_order.m_orderId)] = symbol

        self.routing_short[symbol] += qty * -1
        self.log("<sell>-{}-Sending sell order {} for qty {} price {} for symbol {}".format(self.get_now(),
                                                                              self.sell_order_id[symbol], qty,
                                                                              round(price, 2),symbol))

    def discard_cancel_replace(self, data,order_id):
        symbol = self.get_trade_contract( data)
        del self.pending_cancel_replaces[order_id]
        if self.buy_order_id[symbol] == order_id:
            self.log("Reset for routing_buy_order_id and routing_long after discarding cancel/replace")
            self.buy_order_id[symbol] = None
            self.routing_long[symbol] = 0
        elif self.sell_order_id[symbol] == order_id:
            self.log("Reset for routing_sell_order_id and routing_short after discarding cancel/replace")
            self.sell_order_id[symbol] = None
            self.routing_short[symbol] = 0

    def eval_cancel_replace_timeouts(self,data, order_id):

        if order_id in self.pending_cancel_replaces:
            count = self.pending_cancel_replaces[order_id]
            count += 1
            self.pending_cancel_replaces[order_id] = count

            if count > 3:
                self.log(
                    "WARNING! :  Discarding cancel replace for order id {} because of excess of cancellations!!!".format(
                        order_id))
                self.discard_cancel_replace(data,order_id)
        else:
            self.pending_cancel_replaces[order_id] = 1

    def do_replace(self,data,new_price ,order_id, isBuy):

        symbol = self.get_trade_contract( data)
        self.eval_cancel_replace_timeouts(data,order_id)
        if order_id in self.orders:
            order = self.orders[order_id]

            new_size = 1
            self.log("Replace {} order with OrderId {} and price {} with new price {} and new size={} for symbol" .format(
                "buy" if isBuy else "sell", order_id, round(order.price, 2), round(new_price, 2), new_size,symbol))
            repl_order = self.replace(order=order,
                                      size=new_size,
                                      price=new_price)

            if (hasattr(repl_order, 'm_orderId')):
                self.log(
                    "Assigning replaced {} m_orderId {}".format("Buy" if isBuy else "Sell", repl_order.m_orderId))
                if isBuy:
                    self.buy_order_id[symbol] = repl_order.m_orderId
                    self.order_symbols[str(repl_order.m_orderId)]=symbol
                else:
                    self.sell_order_id[symbol] = repl_order.m_orderId
                    self.order_symbols[str(repl_order.m_orderId)] = symbol

    def do_cancel(self,data, order_id):
        symbol = self.get_trade_contract( data)
        self.eval_cancel_replace_timeouts(data,order_id)
        if order_id in self.orders:  # the order was acepted in the exchange

            order = self.orders[order_id]
            self.cancel(order)
            self.log("Cancelling {} order with OrderId {} and price {} for symbol".format(
                "buy" if order.size > 0 else "sell", order_id, round(order.price, 2),symbol))
        # else:
        # self.log("ERROR: Could not find order id {} to cancel!!".format(order_id))

    def do_cancel_replace(self,data,ref_price, order_id, isBuy):
        if self.impl_replace:
            self.do_replace(data,ref_price,order_id, isBuy)
        else:
            self.do_cancel(data,order_id)

    # endregion

    # region Strategy. Process Execution Reports

    def translate_status(self, order):
        if (order.status == Order.Submitted):
            return "Pending New"
        elif (order.status == Order.Created):
            return "Pending New"
        elif (order.status == Order.Accepted):
            return "New"
        elif (order.status == Order.Canceled):
            return "Canceled"
        elif (order.status == Order.Margin):
            return "Margin Rejected"
        elif (order.status == Order.Rejected):
            return "Rejected"
        elif (order.status == Order.Completed):
            return "Filled"
        elif (order.status == Order.Partial):
            return "Partially Filled"
        elif (order.status == Order.Expired):
            return "Expired"
        else:
            self.log(
                "ERROR!: Unknown order status: {} for order id {}".format(order.status, self.get_order_id(order)))

    def process_order_started(self, order, updateOrder=True):

        orderId = self.get_order_id(order)

        self.log("<started> - {}-New {} Order {} Size={} Price0{} status = {}"
                 .format(self.get_now(), "buy" if order.size > 0 else "sell", orderId, order.size,
                         order.price, self.translate_status(order)))
        if updateOrder:
            self.orders[orderId] = order

    def get_prev_cum_qty(self, orderId, newCumQty):

        if orderId not in self.CumQtyDict:
            self.CumQtyDict[orderId] = 0

        prevCumQty = self.CumQtyDict[orderId]
        self.CumQtyDict[orderId] = newCumQty
        return prevCumQty

    def process_order_traded(self, order, isPartial):

        orderId = self.get_order_id(order)
        symbol = self.order_symbols[str(orderId)]
        last_qty = self.get_last_qty(symbol,order)

        self.log("<traded>-{}=Traded {} {} Order {} status= {} OrdQty={} CumQty={} LastQty={} Price={}"
                 .format(self.get_now(),
                         "Partial" if isPartial else "",
                         "buy" if order.size > 0 else "sell", orderId, self.translate_status(order),
                         order.size, order.executed.size, last_qty, order.executed.price))

        if orderId in self.orders:

            if order.size > 0:

                self.net_shares[symbol] += last_qty
                self.net_long_contracts[symbol] += abs(last_qty)
                self.log(
                    "Reducing Routing Long: routing_long_prev={} last_qty={} for symbol".format(self.routing_long, last_qty,symbol))
                self.routing_long[symbol] -= last_qty
                self.log("Updated Routing Long: routing_long_after={} ".format(self.routing_long[symbol]))
                self.buy_order_id[symbol] = None if self.routing_long[symbol] <= 0 else self.buy_order_id[symbol]

                if self.buy_order_id[symbol] is None:
                    del self.orders[orderId]
            elif order.size < 0:

                self.net_shares[symbol] += last_qty
                self.net_short_contracts[symbol] += abs(last_qty)
                self.log("Reducing Routing Short: routing_short_prev={} last_qty={} for symbol".format(self.routing_short[symbol],
                                                                                                        last_qty,symbol))
                self.routing_short[symbol] -= last_qty
                self.log("Updated Routing Short: routing_short_after={} ".format(self.routing_short[symbol]))
                self.sell_order_id[symbol] = None if self.routing_short[symbol] >= 0 else self.sell_order_id[symbol]

                if self.sell_order_id[symbol] is None:
                    del self.orders[orderId]

            self.evaluate_trade(symbol,last_qty, order.executed.price)
        else:
            self.log("Detected traded order id {} which is no longer processed. Cancel/Replace?".format(orderId))

    def process_order_finished(self, order):

        orderId = self.get_order_id(order)
        symbol = self.order_symbols[str(orderId)]

        self.log("<finished>- Finished {} Symbol {} Order {}  status= {} net_shares={} net_buys={} net_sells={}".format(
            "buy" if order.size > 0 else "sell",symbol, orderId, self.translate_status(order), self.net_shares[symbol],
            self.net_buys[symbol], self.net_sells[symbol]))
        if orderId in self.orders:

            del self.orders[orderId]
            if order.size > 0 and orderId == self.buy_order_id[symbol]:
                self.log("Cleaning routing_buy_order_id: net_shares={} routing_long={} for symbol {} ".format(self.net_shares[symbol],
                                                                                        self.routing_long[symbol],
                                                                                        symbol))
                self.buy_order_id[symbol] = None
                self.routing_long[symbol] = 0


            elif order.size <= 0 and orderId == self.sell_order_id[symbol]:
                self.log("Cleaning routing_sell_order_id: net_shares={} routing_short={} for symbol {} ".format(self.net_shares[symbol],
                                                                                          self.routing_short[symbol],
                                                                                          symbol))
                self.sell_order_id[symbol] = None
                self.routing_short[symbol] = 0

    # endregion

    # region Strategy.Common.Util Methods

    def get_now(self):

        if not self.impl_backtest:
            return datetime.datetime.now()
        else:
            return self.now

    def get_timestamp(self, data):

        # test =matplotlib.dates.num2date(737585.8739735069)
        if hasattr(data, 'datetime'):
            return data.datetime[0]  # arQuants
            # return  matplotlib.dates.num2date(data.datetime[0])  # arQuants
        else:
            return "-"

    def log_engine_status(self, method, data, action):

        if data is None:
            return

        symbol = self.get_trade_contract( data)

        if self.net_sells[symbol] is not None and self.net_buys[symbol] is not None and self.net_shares[symbol] is not None \
            and self.last_market_price[symbol] is not None:
            self.profit[symbol] = self.net_sells[symbol] + self.net_buys[symbol] + (self.net_shares[symbol] * self.last_market_price[symbol])

        self.log(
            "{}- @ {} for action {} --> symbol={} timestamp={} net_shares={} routing_long={} routing_short={} routing_buy_order_id={} routing_sell_order_id={} "
            "net_buys={} net_sells={} last_market_price={} profit={}".format(self.get_now(),
                                                                             method, action,symbol,
                                                                             self.get_timestamp(data),
                                                                             self.net_shares[symbol],
                                                                             self.routing_long[symbol],
                                                                             self.routing_short[symbol],
                                                                             self.buy_order_id[symbol],
                                                                             self.sell_order_id[symbol],
                                                                             round(self.net_buys[symbol], 2),
                                                                             round(self.net_sells[symbol], 2),
                                                                             self.last_market_price[symbol],
                                                                             round(self.profit[symbol], 2)))

    #Price in normal market situations

    #region Trading Methods

    def validate_going_long(self,data):
        symbol = self.get_trade_contract( data)
        # No llegamos al limite de tenencias long y no estamos ruteando ya toda la trade_unit al mercado
        return ((self.net_shares[symbol] + self.routing_long[symbol]) < (self.max_long_qty[symbol]))

    def validate_going_short(self,data):
        symbol = self.get_trade_contract(data)
        # No llegamos al limite de tenencias long y no estamos ruteando ya toda la trade_unit al mercado
        # max_short_qty,routing_short están expresados como negativos asi que hay que hacer las consideraciones de signo
        # necesarias
        return (self.net_shares[symbol] + self.routing_short[symbol]) > (self.max_short_qty[symbol])

    def calcualte_net_profitability(self,symbol):

        profit = self.net_buys[symbol] + self.net_sells[symbol] + (self.net_shares[symbol] * self.last_market_price[symbol])

        self.log("{}-Net money for strategy:{} Net Shares={} Market Price={} NetBuys={} NetSells={}".format(
            symbol,profit, self.net_shares[symbol], round(self.last_market_price[symbol], 2), round(self.net_buys[symbol], 2),
            round(self.net_sells[symbol], 2)))

    def get_last_qty(self,symbol, order):
        last_qty = None

        if hasattr(order, 'm_orderId'):
            prevCumQty = self.get_prev_cum_qty(order.m_orderId, order.executed.size)
            last_qty = order.executed.size - prevCumQty
            # last_qty = order.executed.size - (order.size - (self.routing_long if order.size >0 else self.routing_short))#ArQuants
            self.log( "last qty calculated on order.executed.size={} prevCumQty={} order.size={} routing_long={} routing_short={} "
                    .format(order.executed.size, prevCumQty, order.size, self.routing_long[symbol], self.routing_short[symbol]))
        elif hasattr(order, 'tradeid'):
            last_qty = order.executed.size - self.get_prev_cum_qty(order.tradeid, order.executed.size)
            # last_qty=order.executed.size - (order.size - (self.routing_long if order.size >0 else self.routing_short)) #backtrader
        else:
            raise Exception("ERROR: Could not find order id in  order!")

        return last_qty

    def evaluate_trade(self,symbol, last_qty, price):
        # self.net_shares += trade.size
        # self.log("Recv notify_trade: {}".format(trade))

        if (last_qty < 0):
            self.net_sells[symbol] += abs(last_qty * price)  # it's money I receive. It's a positive number
        else:
            self.net_buys[symbol] += last_qty * price * -1  # it's money I pay. It's a negative number

        self.calcualte_net_profitability(symbol)

    def eval_execution(self,data, ref_price,qty,prev_market_price,last_market_price):
        if data is None:
            return
        symbol = self.get_trade_contract(data)
        if ref_price is not None and not math.isnan(ref_price):

            # self.log_engine_status("next",self.data, "New market price")
            if self.buy_order_id[symbol] is not None and prev_market_price != self.last_market_price[symbol]:
                self.log("<buy>-Last Market Price changed - Running Replace: prev={} new={} for symbol".format(prev_market_price,
                                                                                                  last_market_price,symbol))
                new_price = ref_price - self.defaut_spread[symbol]
                self.do_cancel_replace(data,new_price, self.buy_order_id[symbol], isBuy=True)
            elif (self.buy_order_id[symbol] is None and  self.validate_going_long(data)):
                buy_price = ref_price -self.defaut_spread[symbol]
                self.log("<buy>-sending buy order:  qty={} price={} for symbol".format(qty, buy_price,symbol))
                self.do_buy(data,qty, buy_price)

            if self.sell_order_id[symbol] is not None and prev_market_price != self.last_market_price:
                self.log("<sell>-Last Market Price changed - Running Replace: prev={} new={} for symbol"
                         .format(prev_market_price,last_market_price,symbol))
                new_price = ref_price + self.defaut_spread[symbol]
                self.do_cancel_replace(data,new_price, self.sell_order_id[symbol], isBuy=False)
            elif (self.sell_order_id[symbol] is None  and self.validate_going_short(data)):
                sell_price = ref_price + self.defaut_spread[symbol]
                self.log("<buy>-sending sell order:  qty={} price={} for symbol".format(qty, sell_price,symbol))
                self.do_sell(data,qty, sell_price)

    #endregion

    #region Strategy.Public Methods

    def next(self):
        try:

            self.log_engine_status("next", self.data0, "Recv market data")
            self.log_engine_status("next", self.data1, "Recv market data")

            self._routing_lock.acquire(blocking=True)

            symbol0 = self.get_trade_contract(self.data0)
            ref_price0 = self.get_market_price(self.data0)
            prev_market_price0 = self.last_market_price[symbol0] if self.data0 is not None else None
            self.last_market_price[symbol0] = ref_price0

            symbol1 = self.get_trade_contract(self.data1)
            ref_price1 = self.get_market_price(self.data1)
            prev_market_price1 = self.last_market_price[symbol1] if self.data1 is not None else None
            self.last_market_price[symbol1] = ref_price1

            self.eval_execution(self.data0,ref_price0,1,prev_market_price0,self.last_market_price[symbol0])
            self.eval_execution(self.data1, ref_price1, 1, prev_market_price1, self.last_market_price[symbol1])

        except Exception as e:
            self.log(traceback.print_exc())
            msg = "Critical ERROR @MarketMaker.next:{}".format(str(e))
            self.log(msg)

        finally:
            if self._routing_lock.locked():
                self._routing_lock.release()

    def notify_order(self, order):
        try:
            self._routing_lock.acquire(blocking=True)

            if (order.status == Order.Submitted):
                self.process_order_started(order)
            elif (order.status == Order.Created):
                self.process_order_started(order)
            elif (order.status == Order.Accepted):
                self.process_order_started(order)
            elif (order.status == Order.Completed):
                self.process_order_traded(order, isPartial=False)
            elif (order.status == Order.Partial):
                self.process_order_traded(order, isPartial=True)
            elif (order.status == Order.Margin):
                self.process_order_finished(order)
            elif (order.status == Order.Rejected):
                self.process_order_finished(order)
            elif (order.status == Order.Canceled):
                self.process_order_finished(order)
            elif (order.status == Order.Expired):
                self.process_order_finished(order)
            else:
                self.log("ERROR!: Unknown order status: {} for order id {}".format(order.status, self.get_order_id(order)))
        except Exception as e:
            msg = "Critical ERROR @MarketMaker.notify_order:{}".format(str(e))
            self.log(msg)

        finally:
            if self._routing_lock.locked():
                self._routing_lock.release()

    def notify_trade(self, trade):
        # self.net_shares += trade.size
        # self.log("Recv notify_trade: {}".format(trade))
        pass

    # endregion
