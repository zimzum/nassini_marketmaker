import os
import sys
import traceback

from sources.client.framework.util.Strategy import *
from websocket import create_connection
import threading
import datetime
import json
from sources.client.strategies.dummy_router.common.util.dummy_router_proxy import dummy_router_proxy
from sources.server.order_router.websocket.common.DTO.order_routing import route_order_req


class dummy_router(dummy_router_proxy):
#class dummy_router(Strategy):

    def __init__(self,destURL=None):
        self.CumQtyDict={}
        self.CreationTimes={}
        self.order_requests={}
        self.orders={}
        self.now=None
        self._routing_lock = threading.Lock()
        self.DestURL=destURL
        self.last_market_data_sent_timestmap=[]
        self.connected=False

    #region DTOs

    class web_socket_message():
        def __init__(self,Msg=None):
            self.Msg=Msg

    class execution_report(web_socket_message):

        @staticmethod
        def translate_status( status):
            if (status == Order.Submitted):
                return "PendingNew"
            elif (status == Order.Created):
                return "Pending New"
            elif (status == Order.Accepted):
                return "New"
            elif (status == Order.Canceled):
                return "Canceled"
            elif (status == Order.Cancelled):
                return "Canceled"
            elif (status == Order.Margin):
                return "MarginRejected"
            elif (status == Order.Rejected):
                return "Rejected"
            elif (status == Order.Partial):
                return "PartiallyFilled"
            elif (status == Order.Completed):
                return "Filled"
            elif (status == Order.Expired):
                return "Expired"
            else:
                #self.log("ERROR!: Unknown order status on translation: {}".format(status))
                return "Unknown"


        def __init__(self, Msg=None,order=None,last_qty=0,cum_qty=0,effective_time=None,last_filled_time=None,
                     datetime_format=None):

            super(dummy_router.execution_report, self).__init__(Msg)


            order_size=abs(order.size) if order is not None and  order.size is not  None else None

            cum_qty=abs(cum_qty) if cum_qty is not None else None
            last_qty=abs(last_qty) if last_qty is not None else None

            self.Status= dummy_router.execution_report.translate_status(order.status) if order is not None else None

            self.Text=""
            self.LeavesQty=float(order_size-cum_qty) if order_size is not None and cum_qty is not None else None
            self.CumQty=float(cum_qty)
            self.AvgPx=float(order.executed.price) if order is not None and order.executed.price is not None else None
            self.LastPx=float(order.executed.price) if order is not None and order.executed.price is not None else None
            self.LastQty=float(last_qty)
            self.Commission=None

            self.EffectiveTime=effective_time.strftime(datetime_format) if effective_time is not None else None
            self.LastFilledTime=last_filled_time.strftime(datetime_format) if last_filled_time is not None else None
            self.OrderId=str(order.m_orderId) if order is not None and order.m_orderId is not None else None
            self.ClOrdId=str(order.m_serverOrderId) if order is not None else None #see if it's worth it to assign this
            self.DatetimeFormat=datetime_format


        def toJSON(self):
            return json.dumps(self, default=lambda o: o.__dict__, sort_keys=True)

    class update_order_req(web_socket_message):
        def __init__(self, Msg=None, ClOrdId=None,OrigClOrdId=None, Price=None):
            super(dummy_router.update_order_req, self).__init__(Msg)
            self.OrigClOrdId = OrigClOrdId
            self.ClOrdId = ClOrdId
            self.Price = Price

    class market_data_req(web_socket_message):
        def __init__(self, Msg=None,  Symbol=None, SecurityType=None, Currency=None,SubscriptionRequestType=None,
                    MDReqId=None):
            super(dummy_router.market_data_req, self).__init__(Msg)
            self.Msg = Msg
            self.Symbol = Symbol
            self.SecurityType = SecurityType
            self.Currency = Currency
            self.SubscriptionRequestType = SubscriptionRequestType
            self.MDReqId = MDReqId

    class route_order_req(web_socket_message):

        @staticmethod
        def BUY():
            return "BUY"

        @staticmethod
        def SELL():
            return "SELL"

        def __init__(self ,Msg=None,Symbol=None,Side=None,ReqId=None,Qty=None, Type=None,Account=None,ClOrdId=None,Price=None):
            super(dummy_router.route_order_req, self).__init__(Msg)
            self.Symbol = Symbol
            self.ClOrdId = ClOrdId

            self.Type = Type
            self.Side = Side
            self.Price = Price

            self.ReqId = ReqId
            self.Qty = Qty
            self.Account = Account


        def clone(self):
            cloned = dummy_router.route_order_req()
            cloned.Symbol =self.Symbol
            cloned.ClOrdId =self.ClOrdId
            cloned.Type =self.Type
            cloned.Side =self.Side
            cloned.Price =self.Price
            cloned.ReqId =self.ReqId
            cloned.Qty = self.Qty
            cloned.Account = self.Account
            return cloned

    class market_data_dto(web_socket_message):

        def __init__(self, Symbol=None, MDLocalEntryDate=None, DateTimeFormat = None, Trade=None,MDTradeSize=None,
                     BestBidPrice=None,BestBidSize=None,BestAskPrice=None,BestAskSize=None):
            super(dummy_router.market_data_dto, self).__init__("MarketDataMsg")
            self.MDLocalEntryDate= MDLocalEntryDate.strftime(DateTimeFormat)

            self.Symbol = Symbol
            self.Trade = float(Trade) if Trade is not None else None
            self.MDTradeSize = float(MDTradeSize) if MDTradeSize is not None else None
            self.BestBidPrice = float(BestBidPrice) if BestBidPrice is not None else None
            self.BestAskPrice = float(BestAskPrice) if BestAskPrice is not None else None
            self.BestBidSize = float(BestBidSize) if BestBidSize is not None else None
            self.BestAskSize = float(BestAskSize) if BestAskSize is not None else None
            self.DateTimeFormat=DateTimeFormat

        def toJSON(self):
            return json.dumps(self, default=lambda o: o.__dict__, sort_keys=True)

    class cum_qty_dto():
        def __init__(self,pClOrdId,pOrderId,pCumQty):
            self.ClOrdId=pClOrdId
            self.OrderId=pOrderId
            self.CumQty=pCumQty


    #endregion

    #region Private Methods

    def log_line(self):
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        self.log("ERROR - type={} name={} line={}".format(exc_type, fname, exc_tb.tb_lineno))

    def full_now(self):
        return datetime.datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S.%f')[:-3]

    def create_rejected_er(self,cl_order_id,reason):
        er=dummy_router.execution_report()
        er.Msg="ExecutionReportMsg"
        er.CumQty=0
        er.LastQty=0
        er.Text=reason
        er.LeavesQty=0
        er.AvgPx=None
        er.Commission=0

        er.Status=dummy_router.execution_report.translate_status(Order.Rejected)

        er.ClOrdId=cl_order_id

        er.EffectiveTime=None
        er.DatetimeFormat = self.DateTimeFormat

        return er


    def init_variables(self):
        self.URL=self.DestURL if self.DestURL is not None else "ws://127.0.0.1:7172/"
        self.DateTimeFormat = "%d/%m/%Y %H:%M:%S"
        self.connected=False

    def connect(self):
        try:

            self.ws = create_connection(self.URL)

            threading.Thread(target=self.receive_thread, args=(self.ws,)).start()

            self.connected=True

        except Exception as e:
            self.log("CRITICAL ERROR Connecting to websocket server: " + str(e))

    def market_data_request(self,marketDataReq):

        try:
            self.log("Received market data request for symbol {}".format(marketDataReq.Symbol))
            self.log("WARNING- Dummy Router for ArQuants does NOT work under market data request")
        except Exception as e:
            self.log("ERROR on market data request:{}".format(e))
            self.log_line()
        finally:
            pass

    def update_order(self,updOrderReq):
        try:
            self._routing_lock.acquire(blocking=True)
            if updOrderReq.OrigClOrdId in self.order_requests:


                orig_ord_req = self.order_requests[updOrderReq.OrigClOrdId]
                new_ord_req=orig_ord_req.clone()

                cum_qty_dto = next((s for s in self.CumQtyDict.values() if s.ClOrdId==updOrderReq.OrigClOrdId), None)
                if cum_qty_dto is not None:
                    self.log("Replacing OrigClOrdId {} (ClOrdId {}) with reduced qty {}->{}"
                             .format(updOrderReq.OrigClOrdId,updOrderReq.ClOrdId,new_ord_req.Qty,orig_ord_req.Qty-cum_qty_dto.CumQty))

                    new_ord_req.Qty=orig_ord_req.Qty-cum_qty_dto.CumQty

                else:
                    new_ord_req.Qty=orig_ord_req.Qty

                new_ord_req.Price=updOrderReq.Price
                self.order_requests[updOrderReq.ClOrdId] = new_ord_req

                orig_order =  self.orders[updOrderReq.OrigClOrdId]

                self.log("Updating orderId {} (OrigClOrdId={},ClOrdId={}) old price {} for price {}"
                         .format(orig_order.m_orderId, updOrderReq.OrigClOrdId,updOrderReq.ClOrdId,orig_ord_req.Price,new_ord_req.Price))

                if self._routing_lock.locked():
                    self._routing_lock.release()

                repl_order = self.replace(orig_order,new_ord_req.Qty,new_ord_req.Price,tradeid=updOrderReq.ClOrdId)

                if repl_order is not None and repl_order.m_orderId is not None:
                    self.log("Old order w/orderId {} replaced with order w/ orderId {}".format(orig_order.m_orderId,repl_order.m_orderId))

                    self.orders[updOrderReq.ClOrdId]=repl_order

            else:
                traceback.print_exc()
                raise Exception("Error-Could not find OrigClOrdId {} to update!".format(updOrderReq.OrigClOrdId))
        except Exception as e:

            rejected_er=self.create_rejected_er(updOrderReq.ClOrdId,str(e))
            msg = rejected_er.toJSON()
            self.send_message(msg)
            self.log("Sending REJECTED order on UPDATE(cl_ord_id={})".format(updOrderReq.ClOrdId))
            self.log("WARNING --> Could not @update_order: " + str(e))
            self.log_line()
        finally:
            if self._routing_lock.locked():
                self._routing_lock.release()

    def find_data_by_symbol(self,symbol):
        i=0
        while True:
            if hasattr(self,"data"+str(i)):
                datax=getattr(self,"data"+str(i))

                if datax.tradecontract == symbol:
                    return datax
                i+=1
            elif self.data is not None and self.data.tradecontract==symbol:
                return self.data
            else:
                raise Exception("Could not find market data for symbol {}".format(symbol))

    def new_order(self,newOrderReq):

        try:
            self._routing_lock.acquire(blocking=True
                                       )
            if newOrderReq.Side == dummy_router.route_order_req.BUY():
                self.order_requests[newOrderReq.ClOrdId]=newOrderReq

                self.log("Sending buy order ClOrdId={} for qty {} price {}".format(newOrderReq.ClOrdId,newOrderReq.Qty,
                                                                           round(newOrderReq.Price,2) if newOrderReq.Price is not None else "<market>"))

                if self._routing_lock.locked():
                    self._routing_lock.release()


                new_order  = self.buy(self.find_data_by_symbol(newOrderReq.Symbol),
                                     size=newOrderReq.Qty,
                                     price=newOrderReq.Price if newOrderReq.Price is not None else None,
                                     exectype=Order.Limit if newOrderReq.Price is not None else Order.Market,
                                     tradeid= newOrderReq.ClOrdId )

                self.log("new buy orderId {} sent".format(new_order.m_orderId))

                self.orders[newOrderReq.ClOrdId]=new_order


            elif newOrderReq.Side == dummy_router.route_order_req.SELL():
                self.order_requests[newOrderReq.ClOrdId] = newOrderReq

                self.log("Sending sell order ClOrdId={} for qty {} price {}".format(newOrderReq.ClOrdId, newOrderReq.Qty,
                                                                                    round(newOrderReq.Price,2) if newOrderReq.Price is not None else "<market>"))

                if self._routing_lock.locked():
                    self._routing_lock.release()

                new_order  = self.sell(  data=self.find_data_by_symbol(newOrderReq.Symbol),
                                         size=newOrderReq.Qty,
                                         price=newOrderReq.Price if newOrderReq.Price is not None else None,
                                         exectype=Order.Limit if newOrderReq.Price is not None else Order.Market,
                                         tradeid=newOrderReq.ClOrdId)

                self.log("new sell orderId {} sent".format(new_order.m_orderId))

                self.orders[newOrderReq.ClOrdId]=new_order


            else:
                self.log("Critical error! Can not send an order of side {} for ClOrdId".format(newOrderReq.Side,newOrderReq.ClOrdId))
        except Exception as e:
            self.log("CRITICAL Error @new_order: " + str(e))
            rejected_er=self.create_rejected_er(newOrderReq.ClOrdId,str(e))
            msg = rejected_er.toJSON()
            self.send_message(msg)
            self.log("Sending REJECTED order on NEW ORDER(cl_ord_id={})".format(newOrderReq.ClOrdId))
        finally:
            if self._routing_lock.locked():
                self._routing_lock.release()

    def process_message(self,message):
        try:
            fieldsDict = json.loads(message)
            if "Msg" in fieldsDict and fieldsDict["Msg"] == "NewOrderReq":
                newOrderReq = dummy_router.route_order_req(**json.loads(message))
                threading.Thread(target=self.new_order, args=(newOrderReq,)).start()
            elif "Msg" in fieldsDict and fieldsDict["Msg"] == "UpdOrderReq":
                updOrderReq = dummy_router.update_order_req(**json.loads(message))
                threading.Thread(target=self.update_order, args=(updOrderReq,)).start()
            elif "Msg" in fieldsDict and fieldsDict["Msg"] == "MarketDataReq":
                mdReq = dummy_router.market_data_req(**json.loads(message))
                threading.Thread(target=self.market_data_request, args=(mdReq,)).start()
            self.log(message)
        except Exception as e:
            self.log("Error @process_message: " + str(e))

    def receive_thread(self,ws):
        try:
            while True:

                message = ws.recv()
                self.process_message(message)

        except Exception as e:
            self.log("Critical error @receive_thread: " + str(e))

    def send_message(self,msg):
        self.ws.send(msg)

    def close(self):
        self.ws.close()

    def get_prev_cum_qty(self, orderId,clOrdId, newCumQty):
        if orderId not in self.CumQtyDict:
            self.CumQtyDict[orderId] = dummy_router.cum_qty_dto(orderId,clOrdId,0)

        prevCumQty = self.CumQtyDict[orderId].CumQty

        if(abs(newCumQty)<abs(prevCumQty)):
            self.log("ERROR- Weird scenario where prevCumQty>newCumQty :{}->{} for orderId {} (ClOrdId={})"
                     .format(prevCumQty,newCumQty,orderId,clOrdId))

        self.CumQtyDict[orderId].CumQty = newCumQty
        return prevCumQty

    def get_last_qty(self, order):
        last_qty = None

        prevCumQty = self.get_prev_cum_qty(order.m_orderId,order.m_serverOrderId, order.executed.size)
        last_qty = order.executed.size - prevCumQty
        self.log("last qty calculated on order.executed.size={} prevCumQty={} order.size={}  ".format(order.executed.size, prevCumQty, order.size))

        return last_qty

    def get_now(self):
        return self.now if self.now is not None else datetime.datetime.now()

    def eval_new_market_data(self,data, i):

        if i not in self.last_market_data_sent_timestmap:
            self.last_market_data_sent_timestmap.insert(i,data)
            return True
        else:
            old_data= self.last_market_data_sent_timestmap[i]

            if old_data.get_last_date_time()<data.get_last_date_time():
                self.last_market_data_sent_timestmap[i]=data
                return True
            else:
                return False

    def send_market_data(self,data):

        self.log(
            "Recv market data for Symbol={} last={}".format(data.tradecontract, data.last_px[0]))

        market_data_dto = dummy_router.market_data_dto(Symbol=data.tradecontract,
                                                   MDLocalEntryDate=data.datetime[0], DateTimeFormat=self.DateTimeFormat,
                                                   Trade=data.last_px[0], MDTradeSize=data.last_qty[0],
                                                   BestBidPrice=data.bid_px[0] if len(data.bid_px) > 0 else None,
                                                   BestBidSize=data.bid_qty[0] if len(data.bid_qty) > 0 else None,
                                                   BestAskPrice=data.offer_px[0] if len(data.offer_px) > 0 else None,
                                                   BestAskSize=data.offer_qty[0] if len(data.offer_qty) > 0 else None
                                                   )


        msg = market_data_dto.toJSON()
        self.send_message(msg)

    #endregion

    #region Public Methods

    def start(self):
        if not self.connected:
            self.log("Starting Dummy Router")
            self.init_variables()
            self.connect()
            self.connected=True

    def next(self):
        try:

            i=0
            if self.connected:

                while True:
                    if hasattr(self,"data"+str(i)):
                        data = getattr(self,"data"+str(i))

                        if data is not None and self.eval_new_market_data(data,i):
                            self.send_market_data(data)
                    else:
                        break

                    i+=1

                if self.data is not None and not hasattr(self,"data0"):
                    self.send_market_data(self.data)

        except Exception as e:
            self.log("Error processing market data!: " + str(e))
        finally:
            #self.log("DB-dummy.next_exit {}".format(self.full_now()))
            if self._routing_lock.locked():
                self._routing_lock.release()

    def process_order_started(self,order,upateOrder):
        try:
            self._routing_lock.acquire(blocking=True)
            self.log("<started> - New {} OrderId {} (ClOrdId {}) Size={} Price0{} status = {}"
                     .format( "buy" if order.size > 0 else "sell", order.m_orderId,order.m_serverOrderId,
                              order.size,order.price, dummy_router.execution_report.translate_status(order.status)))

            self.CreationTimes[order.m_orderId]=self.get_now()
            exec_report = dummy_router.execution_report("ExecutionReportMsg", order,
                                                        effective_time=self.CreationTimes[order.m_orderId],
                                                        datetime_format=self.DateTimeFormat)
            msg = exec_report.toJSON()
            if self._routing_lock.locked():
                self._routing_lock.release()
            self.send_message(msg)
        except Exception as e:
            self.log("Error @process_order_started: " + str(e))
        finally:
            if self._routing_lock.locked():
                self._routing_lock.release()

    def process_order_finished(self,order):
        try:
            self._routing_lock.acquire(blocking=True)
            self.log("<finished>- Finished {} OrderId {} (ClOrdId={})  status= {} ".format(
                    "buy" if order.size > 0 else "sell", order.m_orderId,order.m_serverOrderId,
                    dummy_router.execution_report.translate_status(order.status)))

            if order.m_orderId not in self.CreationTimes:
                self.CreationTimes[order.m_orderId]=self.get_now()

            exec_report = dummy_router.execution_report("ExecutionReportMsg", order,
                                                        effective_time=self.CreationTimes[order.m_orderId],
                                                        datetime_format=self.DateTimeFormat)
            msg = exec_report.toJSON()

            if self._routing_lock.locked():
                self._routing_lock.release()

            self.send_message(msg)
        except Exception as e:
            self.log("Error @process_order_finished: " + str(e))
        finally:
            if self._routing_lock.locked():
                self._routing_lock.release()

    def process_order_traded(self,order,isPartial):

        try:
            self._routing_lock.acquire(blocking=True)
            last_qty=self.get_last_qty(order)

            cum_qty=self.CumQtyDict[order.m_orderId].CumQty
            self.log("<traded>=Traded {} {} Order {} (ClOrdId={}) status= {} OrdQty={} CumQty={} LastQty={} Price={}"
                     .format("Partial" if isPartial else "",
                             "buy" if order.size > 0 else "sell", order.m_orderId,order.m_serverOrderId,
                             dummy_router.execution_report.translate_status(order.status),
                             order.size, order.executed.size, last_qty, order.executed.price))

            if order.m_orderId not in self.CreationTimes:
                self.CreationTimes[order.m_orderId]=self.get_now()

            exec_report = dummy_router.execution_report("ExecutionReportMsg", order, last_qty=last_qty, cum_qty=cum_qty,
                                                        effective_time=self.CreationTimes[order.m_orderId],
                                                        last_filled_time=self.get_now(),
                                                        datetime_format=self.DateTimeFormat
                                                        )
            msg = exec_report.toJSON()

            if self._routing_lock.locked():
                self._routing_lock.release()

            self.send_message(msg)

        except Exception as e:
            self.log("Error @process_order_traded: " + str(e))
        finally:
            if self._routing_lock.locked():
                self._routing_lock.release()


    def notify_order(self, order):


        if (order.status == Order.Submitted):
            updateOrder=False
            threading.Thread(target=self.process_order_started, args=(order,updateOrder)).start()
        elif (order.status == Order.Created):
            updateOrder = False
            threading.Thread(target=self.process_order_started, args=(order,updateOrder)).start()
        elif (order.status == Order.Accepted):
            updateOrder = True
            threading.Thread(target=self.process_order_started, args=(order,updateOrder)).start()

        elif (order.status == Order.Canceled):
            threading.Thread(target=self.process_order_finished, args=(order,)).start()
        elif (order.status == Order.Margin):
            threading.Thread(target=self.process_order_finished, args=(order,)).start()
        elif (order.status == Order.Rejected):
            threading.Thread(target=self.process_order_finished, args=(order,)).start()
        elif (order.status == Order.Expired):
            threading.Thread(target=self.process_order_finished, args=(order,)).start()
        elif (order.status == Order.Completed):
            isPartial = False
            threading.Thread(target=self.process_order_traded, args=(order,isPartial)).start()
        elif (order.status == Order.Partial):
            isPartial = True
            threading.Thread(target=self.process_order_traded, args=(order,isPartial)).start()
        else:
            self.log("ERROR!: Unknown order status: {}".format(order.status))

    def notify_trade(self, trade):
        pass

    #endregion


