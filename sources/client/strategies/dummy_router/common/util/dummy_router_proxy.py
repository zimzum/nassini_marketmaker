import os
from logging.handlers import TimedRotatingFileHandler

from sources.client.framework.util.backtester_proxy import backtester_proxy
from sources.framework.common.logger import *
import logging

class dummy_router_proxy(backtester_proxy):

    def __init__(self):
        self.logger = logging.getLogger("nasini-dummy-router")
        self.logger_started = False

    def backtest_prestart(self, start_time):
        self.now = start_time
        self.process_start = start_time
        #self.last_imbalance_depuration = start_time

    def prepare_logs(self,name):
        self.logger = logging.getLogger(name)

        log_path = os.path.join("logs", name)

        main_formatter = logging.Formatter(
            fmt='%(asctime)s [%(module)s %(levelname)s] %(message)s',
            datefmt='%Y-%m-%d %H:%M:%S')

        console_handler = logging.StreamHandler()
        file_handler = TimedRotatingFileHandler(
            filename=log_path, when='d', backupCount=20)

        for handler in [console_handler, file_handler]:
            handler.setFormatter(main_formatter)
            self.logger.addHandler(handler)
        self.logger.setLevel(20)  # 20 = normal
        self.logger_started=True

    def log(self,msg):
        self.logger.info(msg)

    def on_init_backtester(self,p_configuration):
        self.data = None
        self.periodic_persist = None
        self.ProxyName = "dummy_router_proxy"
        self.prepare_logs("DummyRouter")

    def on_periodic_change(self, period, backtest, p_start, p_end):
        pass

    def on_hourly_change(self, backtest, p_start, p_end):
        pass

    def on_minute_change(self, backtest, p_start, p_end):
        pass

    def on_daily_change(self, backtest, p_start, p_end):
        pass

    def get_config_params(self, backtest):
        return []

    def get_summary(self):
        return ""

    def get_cum_profit(self):
        return 0

    def get_net_sells(self):
        return 0

    def get_net_buys(self):
        return 0

    def get_net_long_contracts_for_symbol(self,symbol):
        return 0

    def get_net_short_contracts_for_symbol(self,symbol):
        return 0
