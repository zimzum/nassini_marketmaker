from sources.client.framework.util.Strategy import *
from sources.client.strategies.market_maker.common.util.market_making_backtester_proxy import *
import uuid
import datetime
import threading
import math
import traceback
import decimal
import time
#IMPORTANT
#LOOK FOR COMMENTS LIKE "TO CHANGE IN PROD ENV" to fully fit the backtrader fwk implementation
#to Aruants impl

_SIDE_BID="BID"
_SIDE_ASK="ASK"
_IMBALANCE_BID="BID"
_IMBALANCE_ASK="ASK"
_IMBALANCE_BID_END="BID_END"
_IMBALANCE_ASK_END="ASK_END"
_NEW_MAX="NEW_MAX"
_NEW_MIN="NEW_MIN"


class market_maker(market_making_backtester_proxy):
#class market_maker(Strategy):

    # TO CHANGE IN PROD ENV --> TO UNCOMMENT IN PROD ENV
    def __init__(self, max_long_qty=100, max_short_qty=-100, default_spread=5, inventory_spread_unit=100, trade_unit=5,
             inv_threshold=1, impl_imbalance=1, imbalance_min_threshold=0.8, imbalance_exit_threshold=0.6, imbalance_unit=0.05,
             imbalance_price_delta=10, imbalance_min_opx_timespan=15, imbalance_depuration_threshold=30,impl_rope_price_on_regular_markets=1,
             impl_closing_time=1, impl_opposite_inventory=1,impl_strong_direction=1, closing_time="11:30 PM", mid_price_calculation=3,tick_size=5,
             new_maxmin_start_count=20,new_max_min_end_count=10,elastic_rope_pct=0.5,
             evaluate_high_lows=False, strong_dir_pacing_sec=10,imbalance_min_entry_sec=600,switched_off_imbalance_time="07:40 PM"):


        self.name = "Market Maker3"

        # region Config Parameters
        self.max_long_qty = max_long_qty
        self.max_short_qty = max_short_qty

        self.default_spread = default_spread
        self.inventory_spread_unit = inventory_spread_unit
        self.trade_unit = trade_unit

        self.inv_threshold = inv_threshold

        self.impl_imbalance = impl_imbalance
        self.impl_backtest=False
        self.tick_size=tick_size

        self.imbalance_min_threshold = imbalance_min_threshold
        self.imbalance_exit_threshold=imbalance_exit_threshold
        self.imbalance_unit = imbalance_unit
        self.imbalance_price_delta = imbalance_price_delta

        self.new_maxmin_start_count=new_maxmin_start_count
        self.new_maxmin_end_count=new_max_min_end_count

        self.imbalance_min_opx_timespan = imbalance_min_opx_timespan
        self.imbalance_depuration_threshold = imbalance_depuration_threshold

        self.impl_closing_time = impl_closing_time
        self.closing_time = closing_time

        self.mid_price_calculation = mid_price_calculation  # 1=last,2=average to last,3=average with imbalance

        self.impl_opposite_inventory=impl_opposite_inventory
        self.impl_strong_direction=impl_strong_direction
        self.impl_rope_price_on_regular_markets=impl_rope_price_on_regular_markets

        self.elastic_rope_pct=elastic_rope_pct
        self.evaluate_high_lows = evaluate_high_lows

        self.strong_dir_pacing_sec=strong_dir_pacing_sec
        self.imbalance_min_entry_sec=imbalance_min_entry_sec
        self.switched_off_imbalance_time=switched_off_imbalance_time

    # endregion


    def start(self):
        self.log("Starting Nassini - Market Maker")

        # region Administrative
        self.buy_order_id = None
        self.sell_order_id = None
        self.pending_cancel_replaces = {}
        self.impl_replace = True
        # endregion

        # region Calculated
        self.last_market_price = 0
        self.routing_long = 0
        self.routing_short = 0
        self.net_shares = 0
        self.net_sells = 0
        self.net_buys = 0
        self.net_long_contracts = 0
        self.net_short_contracts = 0
        self.profit=0

        self.shares_bid = 0
        self.shares_ask = 0
        self.mon_volume_bid = 0
        self.mon_volume_ask = 0
        self.shares_bid_snapshot = 0
        self.shares_ask_snapshot = 0
        self.mon_volume_bid_snapshot = 0
        self.mon_volume_ask_snapshot = 0
        self.last_price = None
        self.last_size = None
        self.trading_closed = False

        self.imbalance_activated=None
        self.imbalance_bid_first_triggered=None
        self.imbalance_ask_first_triggered = None
        self.imbalance_start = None
        self.imbalance_end = None

        self.new_high_lows_activated=None
        self.new_high_lows_start = None
        self.new_high_lows_end = None

        self.candle_prices = []
        self.last_candle_timestamp=None

        self.CumQtyDict = {}

        self.now = None
        self.process_start = self.get_now()
        self.last_imbalance_depuration = self.get_now()
        self.last_buy_order_sent=None
        self.last_sell_order_sent = None


        # endregion

        self._routing_lock = threading.Lock()

        self.orders = {}
        self.finished_orders={}
        self.sent_orders={}

    # region Order Routing

    def do_buy_market(self, qty):

        self.buy_order_id = str(uuid.uuid4())
        self.last_buy_order_sent=self.get_now()
        new_order = self.buy(data=self.data,
                             size=qty,
                             price=None,
                             exectype=Order.Market,
                             tradeid=self.buy_order_id)

        if (hasattr(new_order, 'm_orderId')):
            self.log("Assigning buy m_orderId {}".format(new_order.m_orderId))
            self.buy_order_id = new_order.m_orderId

        self.routing_long += qty
        self.log(
            "{}-Sending  market buy order {} for qty {} ".format(self.get_now(), self.buy_order_id, qty))

    def do_sell_market(self, qty):

        self.sell_order_id = str(uuid.uuid4())
        self.last_sell_order_sent = self.get_now()
        new_order = self.sell(data=self.data,
                              size=qty,
                              price=None,
                              exectype=Order.Market,
                              tradeid=self.sell_order_id)

        if (hasattr(new_order, 'm_orderId')):
            self.log("Assigning sell m_orderId {}".format(new_order.m_orderId))
            self.sell_order_id = new_order.m_orderId

        self.routing_short += qty * -1
        self.log(
            "{}-Sending market sell order {} for qty {} ".format(self.get_now(), self.sell_order_id, qty))

    def do_buy(self, qty, price):

        self.buy_order_id = str(uuid.uuid4())
        self.last_buy_order_sent = self.get_now()
        new_order = self.buy(data=self.data,
                             size=qty,
                             price=price,
                             exectype=Order.Limit,
                             tradeid=self.buy_order_id)

        if (hasattr(new_order, 'm_orderId')):
            self.log("<buy>-Assigning buy m_orderId {}".format(new_order.m_orderId))
            self.buy_order_id = new_order.m_orderId
            self.orders[new_order.m_orderId] = new_order
            self.sent_orders[new_order.m_orderId] = new_order
        else:
            self.log("WARNING!: new buy order with client_order_id={} did not receive a server id!")

        self.routing_long += qty
        self.log(
            "<buy>-{}-Sending buy order {} for qty {} price {}".format(self.get_now(), self.buy_order_id,
                                                                       qty, round(price, 2)))

    def do_sell(self, qty, price):

        self.sell_order_id = str(uuid.uuid4())
        self.last_sell_order_sent = self.get_now()
        new_order = self.sell(data=self.data,
                              size=qty,
                              price=price,
                              exectype=Order.Limit,
                              tradeid=self.sell_order_id)

        if (hasattr(new_order, 'm_orderId')):
            self.log("<sell>-Assigning sell m_orderId {}".format(new_order.m_orderId))
            self.sell_order_id = new_order.m_orderId
            self.orders[new_order.m_orderId] = new_order
            self.sent_orders[new_order.m_orderId] = new_order
        else:
            self.log("WARNING!: new sell order with client_order_id={} did not receive a server id!")

        self.routing_short += qty * -1
        self.log("<sell>-{}-Sending sell order {} for qty {} price {}".format(self.get_now(),
                                                                              self.sell_order_id, qty,
                                                                              round(price, 2)))

    def discard_cancel_replace(self, order_id):

        del self.pending_cancel_replaces[order_id]
        if self.buy_order_id == order_id:
            self.log("Reset for routing_buy_order_id and routing_long after discarding cancel/replace")
            self.buy_order_id = None
            self.routing_long = 0
        elif self.sell_order_id == order_id:
            self.log("Reset for routing_sell_order_id and routing_short after discarding cancel/replace")
            self.sell_order_id = None
            self.routing_short = 0

    def eval_cancel_replace_timeouts(self, order_id):

        if order_id in self.pending_cancel_replaces:
            count = self.pending_cancel_replaces[order_id]
            count += 1
            self.pending_cancel_replaces[order_id] = count

            if count > 3:
                self.log(
                    "WARNING! :  Discarding cancel replace for order id {} because of excess of cancellations!!!".format(
                        order_id))
                self.discard_cancel_replace(order_id)
        else:
            self.pending_cancel_replaces[order_id] = 1

    def do_replace(self,mid_price ,order_id, isBuy):
        self.eval_cancel_replace_timeouts(order_id)
        if order_id in self.orders :
            order = self.orders[order_id]

            new_price = self.get_bid_reference_price(mid_price) if isBuy else self.get_ask_reference_price(mid_price)
            new_size = self.routing_long if isBuy else self.routing_short

            self.log("Replace {} order with OrderId {} and price {} with new price {} and new size={}".format(
                "buy" if isBuy else "sell", order_id, round(order.price, 2), round(new_price, 2), new_size))
            repl_order = self.replace(order=order,
                                      size=new_size,
                                      price=new_price)

            if (hasattr(repl_order, 'm_orderId')):
                self.log(
                    "Assigning replaced {} m_orderId {}".format("Buy" if isBuy else "Sell", repl_order.m_orderId))
                if isBuy:
                    self.last_buy_order_sent=self.get_now()
                    self.buy_order_id = repl_order.m_orderId
                    self.orders[repl_order.m_orderId] = repl_order
                    self.sent_orders[repl_order.m_orderId] = repl_order
                else:
                    self.last_sell_order_sent=self.get_now()
                    self.sell_order_id = repl_order.m_orderId
                    self.orders[repl_order.m_orderId] = repl_order
                    self.sent_orders[repl_order.m_orderId] = repl_order
            else:
                self.log("WARNING!: new replace order with client_order_id={} did not receive a server id!")

    def do_cancel(self, order_id):

        self.eval_cancel_replace_timeouts(order_id)
        if order_id in self.orders:  # the order was acepted in the exchange

            order = self.orders[order_id]
            self.cancel(order)
            self.log("Cancelling {} order with OrderId {} and price {}".format(
                "buy" if order.size > 0 else "sell", order_id, round(order.price, 2)))
        # else:
        # self.log("ERROR: Could not find order id {} to cancel!!".format(order_id))

    def do_cancel_replace(self,ref_price, order_id, isBuy):

        #we should not replace an order before it was accepted in the exchange
        if order_id in self.sent_orders:
            return

        if self.impl_replace:
            self.do_replace(ref_price,order_id, isBuy)
        else:
            self.do_cancel(order_id)

    # endregion

    # region Strategy. Process Execution Reports

    def process_order_started(self, order, updateOrder=True):

        orderId = self.get_order_id(order)

        self.log("<started> - {}-New {} Order {} Size={} Price0{} status = {}"
                 .format(self.get_now(), "buy" if order.size > 0 else "sell", orderId, order.size,
                         order.price, self.translate_status(order)))

        if updateOrder:
            self.orders[orderId] = order
            del self.sent_orders[orderId]

    def process_order_traded(self, order, isPartial):

        orderId = self.get_order_id(order)
        last_qty = self.get_last_qty(order)

        self.log("<traded>-{}=Traded {} {} Order {} status= {} OrdQty={} CumQty={} LastQty={} Price={}"
                 .format(self.get_now(),
                         "Partial" if isPartial else "",
                         "buy" if order.size > 0 else "sell", orderId, self.translate_status(order),
                         order.size, order.executed.size, last_qty, order.executed.price))

        if orderId in self.sent_orders:
            del self.sent_orders[orderId]

        if orderId in self.orders:

            if order.size > 0:

                self.net_shares += last_qty
                self.net_long_contracts += abs(last_qty)
                self.log(
                    "Reducing Routing Long: routing_long_prev={} last_qty={}".format(self.routing_long, last_qty))
                self.routing_long -= last_qty
                self.log("Updated Routing Long: routing_long_after={} ".format(self.routing_long))
                self.buy_order_id = None if self.routing_long <= 0 else self.buy_order_id

                if self.buy_order_id is None:
                    del self.orders[orderId]
                    self.finished_orders[orderId]=order

            elif order.size < 0:

                self.net_shares += last_qty
                self.net_short_contracts += abs(last_qty)
                self.log("Reducing Routing Short: routing_short_prev={} last_qty={}".format(self.routing_short,
                                                                                            last_qty))
                self.routing_short -= last_qty
                self.log("Updated Routing Short: routing_short_after={} ".format(self.routing_short))
                self.sell_order_id = None if self.routing_short >= 0 else self.sell_order_id

                if self.sell_order_id is None:
                    del self.orders[orderId]
                    self.finished_orders[orderId] = order

            self.evaluate_trade(last_qty, order.executed.price)
        else:
            if orderId not in self.finished_orders:
                msg="Detected traded order id {} which is no longer processed. Cancel/Replace?".format(orderId)
                self.log(msg)
                raise Exception(msg)

    def process_order_finished(self, order):

        orderId = self.get_order_id(order)

        self.log("<finished>- Finished {} Order {}  status= {} net_shares={} net_buys={} net_sells={}".format(
            "buy" if order.size > 0 else "sell", orderId, self.translate_status(order), self.net_shares,
            self.net_buys, self.net_sells))

        if orderId in self.sent_orders:
            del self.sent_orders[orderId]

        if orderId in self.orders:

            del self.orders[orderId]
            self.finished_orders[orderId]=order
            if order.size > 0 and orderId == self.buy_order_id:
                self.log("Cleaning routing_buy_order_id: net_shares={} routing_long={} ".format(self.net_shares,
                                                                                        self.routing_long))
                self.buy_order_id = None
                self.routing_long = 0


            elif order.size <= 0 and orderId == self.sell_order_id:
                self.log("Cleaning routing_sell_order_id: net_shares={} routing_short={} ".format(self.net_shares,
                                                                                          self.routing_short))
                self.sell_order_id = None
                self.routing_short = 0
        else:
            if orderId not in self.finished_orders:
                msg="Detected finished order id {} which is no longer processed. Cancel/Replace?".format(orderId)
                self.log(msg)
                raise Exception(msg)

    # endregion

    # region Strategy.Common.Util Methods

    def get_now(self):

        if not self.impl_backtest:
            return datetime.datetime.now()
        else:
            return self.now

    # ArQuants bug where times like "07:30 PM" are entered like "07:30PM'
    def clean_time_format(self, time):

        if time.endswith("PM") and not time.endswith(" PM"):
            if time.endswith("PM"):
                time = time.replace("PM", " PM")

        if time.endswith("AM") and not time.endswith(" AM"):
            if time.endswith("PM"):
                time = time.replace("AM", " AM")

        return time

    def log_engine_status(self, method, data, action):

        self.profit = 0
        if self.net_sells is not None and self.net_buys is not None and self.net_shares is not None and self.last_market_price is not None:
            self.profit = self.net_sells + self.net_buys + (self.net_shares * self.last_market_price)

        self.log(
            "{}- @ {} for action {} --> timestamp={} net_shares={} routing_long={} routing_short={} routing_buy_order_id={} routing_sell_order_id={} "
            "net_buys={} net_sells={} last_market_price={} profit={}".format(self.get_now(),
                                                                             method, action,
                                                                             self.get_timestamp(data),
                                                                             self.net_shares, self.routing_long,
                                                                             self.routing_short,
                                                                             self.buy_order_id, self.sell_order_id,
                                                                             round(self.net_buys, 2),
                                                                             round(self.net_sells, 2),
                                                                             self.last_market_price,
                                                                             round(self.profit, 2)))

    def evaluate_time(self):
        now = self.get_now()
        # fecha en formato AM/PM Ex: 04:30 PM.
        # tiene que tener la misma zona horaria que ArQuants. Al momento de escribir este código este es GMT=0
        # Buenos Aires es GMT-3

        cleaned_closing_time = self.clean_time_format(self.closing_time)
        exit_time = time.strptime(cleaned_closing_time, "%I:%M %p")

        today_end = now.replace(hour=exit_time.tm_hour, minute=exit_time.tm_min, second=0, microsecond=0)
        return today_end < now

    def run_final_opx_thread(self):

        try:
            # Hasta que no termine de cancelar lo que estaba en el mercado, no intento comprar
            while self.buy_order_id is not None or self.sell_order_id is not None:
                if self.buy_order_id:
                    self.log("End of day closing buy order id {}".format(self.buy_order_id))
                    self.do_cancel(self.buy_order_id)

                if self.sell_order_id:
                    self.log("End of day closing sell order id {}".format(self.sell_order_id))
                    self.do_cancel(self.sell_order_id)

                time.sleep(10)

            if self.net_shares > 0:
                self.log("Closing long position for {} units at end of day ".format(self.net_shares))
                #self.do_sell_market(qty=self.net_shares)
                self.do_sell(qty=self.net_shares,price=self.data.bid_px[0])

            if self.net_shares < 0:
                self.log("Closing short position for {} units at end of day ".format(abs(self.net_shares)))
                #self.do_buy_market(qty=abs(self.net_shares))
                self.do_buy(qty=abs(self.net_shares), price=self.data.offer_px[0])

        except Exception as e:
            self.log("Critical error running final close because of end of day:{}".format(str(e)))

    def is_closing_time(self):

        if not self.trading_closed and self.evaluate_time():  # comparar fechas para ver si corresponde cierre
            self.log("Executing closing procedure at {}".format(self.get_now()))
            threading.Thread(target=self.run_final_opx_thread, args=()).start()
            self.trading_closed = True

        return self.trading_closed

    def validadte_going_long_inv(self):
        # No llegamos al limite de tenencias long y no estamos ruteando ya toda la trade_unit al
        # mercado
        return ((self.net_shares + self.routing_long) < (self.max_long_qty)) and (
                self.trade_unit > self.routing_long)

    def validate_going_long(self):

        if (self.is_strong_direction(_SIDE_BID) or self.is_opposite_inventory(_SIDE_BID)) and self.last_buy_order_sent is not None:
            span = self.get_now() - self.last_buy_order_sent
            self.log("<pacing> Long- Evaluating that {} seconds elapsed is bigger than req_pacing (sec)= {}".format(span.seconds,self.strong_dir_pacing_sec))
            if (span.seconds > self.strong_dir_pacing_sec):
                self.log("<pacing>-good to go long")
                return self.validadte_going_long_inv()
            else:
                return False
        else:

            return self.validadte_going_long_inv()

    def validate_going_short_inv(self):
        # No llegamos al limite de tenencias long y no estamos ruteando ya toda la trade_unit al mercado
        # max_short_qty,routing_short están expresados como negativos asi que hay que hacer las consideraciones de signo
        # necesarias
        return (self.net_shares + self.routing_short) > (self.max_short_qty) and (
                self.trade_unit > abs(self.routing_short))

    def validate_going_short(self):

        if (self.is_strong_direction(_SIDE_ASK) or self.is_opposite_inventory(_SIDE_ASK)) and self.last_sell_order_sent is not None:
            span = self.get_now() - self.last_sell_order_sent
            self.log("<pacing> Short- Evaluating that {} seconds elapsed is bigger than req_pacing (sec)= {}".format(span.seconds,self.strong_dir_pacing_sec))

            if (span.seconds > self.strong_dir_pacing_sec):
                self.log("<pacing>-good to go short")
                return self.validate_going_short_inv()
            else:
                return False
        else:

            return self.validate_going_short_inv()

    def validate_imbalance_min_opx_timespan(self):
        time_delta = (self.get_now() - self.process_start)
        self.log("validate_imbalance_min_opx_timespan.timedelta={}".format(time_delta))
        return (time_delta.total_seconds() / 60) > self.imbalance_min_opx_timespan

    def get_buy_qty(self):

        if (self.trade_unit - self.routing_long) > 0:
            return self.trade_unit - self.routing_long
        else:
            raise Exception("ERROR:Could not route {} units to market when we already have {} units routed".format(
                self.trade_unit, self.routing_long))

    def get_sell_qty(self):

        if (self.trade_unit - abs(self.routing_short)) > 0:
            return self.trade_unit - abs(self.routing_short)
        else:
            raise Exception("ERROR:Could not route {} units to market when we already have {} units routed".format(
                self.trade_unit, self.routing_short))

    def get_mid_price(self):
        return self.last_market_price

    def is_opposite_inventory(self,side):

        if self.impl_opposite_inventory!=1:
            return False

        if side == _SIDE_BID: #I must make the bid aggressive because of opposite invalance
            return self.imbalance_activated==_IMBALANCE_ASK and int(self.net_shares) <0 #I am short and the ASK imbalance is activated

        if side == _SIDE_ASK: #I must make the ask aggressive because of opposite invalance
            return self.imbalance_activated==_IMBALANCE_BID and int(self.net_shares) >0 #I am long and the BID imbalance is activated

        return False

    def get_opposite_inventory_price(self,mid_price,side):
        if side == _SIDE_BID:
            return self.data.offer_px[0]

        elif side == _SIDE_ASK:
            return self.data.bid_px[0]
        else:
            raise Exception("Invalid bid {} at get_opposite_inventory_price".format(side))

    def is_strong_direction(self,side):

        if self.impl_strong_direction != 1:
            return False

        if side == _SIDE_BID:  # I must make the bid aggressive because of opposite imbalance
            return self.imbalance_activated == _IMBALANCE_ASK   # I am short and the ASK imbalance is activated

        if side == _SIDE_ASK:  # I must make the ask aggressive because of opposite imbalance
            return self.imbalance_activated == _IMBALANCE_BID  # I am long and the BID imbalance is activated

        return False

    def get_strong_direction_price(self,mid_price, side):
        if side == _SIDE_BID:
            return self.data.offer_px[0]

        elif side == _SIDE_ASK:
            return self.data.bid_px[0]
        else:
            raise Exception("Invalid bid {} at get_opposite_inventory_price".format(side))

    def get_rope_price(self,mid_price, side):

        if side == _SIDE_BID:
            spread = (1 - self.elastic_rope_pct) * self.default_spread
            new_price = mid_price - spread
            new_price=self.round_to_tick(new_price)
            return new_price

        elif side == _SIDE_ASK:
            spread = (1 - self.elastic_rope_pct) * self.default_spread
            new_price = mid_price + spread
            new_price = self.round_to_tick(new_price)
            return new_price
        else:
            raise Exception("Invalid side {} at get_opposite_inventory_price".format(side))

    def get_regular_bid_price(self,mid_price):

        if self.net_shares>=0 or self.impl_rope_price_on_regular_markets==0 or not self.imbalance_is_off():
            return mid_price - self.default_spread - self.get_bid_inventory_spread() - self.get_bid_imbalance_spread(mid_price)
        else:
            self.get_bid_imbalance_spread(mid_price)#I have to validate that teh ask-imbalance is on-off
            return self.get_rope_price(mid_price,_SIDE_BID)

    def get_regular_ask_price(self,mid_price):
        if self.net_shares<=0 or self.impl_rope_price_on_regular_markets==0 or not self.imbalance_is_off():
            return mid_price + self.default_spread + self.get_ask_inventory_spread() + self.get_ask_imbalance_spread(mid_price)
        else:
            self.get_ask_imbalance_spread(mid_price)#I have to validate that the bid-imbalance is on or off
            return self.get_rope_price(mid_price,_SIDE_ASK)

    #Price in normal market situations
    def get_regular_price(self,mid_price,side):

        if side==_SIDE_BID:
            return self.get_regular_bid_price(mid_price)
        elif side==_SIDE_ASK:
            return self.get_regular_ask_price(mid_price)
        else:
            raise Exception("Unknown side {}".format(side))

    def get_bid_reference_price(self,mid_price):
        if self.is_opposite_inventory(_SIDE_BID):
            return self.get_opposite_inventory_price(mid_price,_SIDE_BID) # self.get_opposite_inventory_price(mid_price,BID)
        elif self.is_strong_direction(_SIDE_BID):
            return self.get_strong_direction_price(mid_price,_SIDE_BID) # self.get_strong_direction_price(mid_price,BID)
        else:
            return self.get_regular_price(mid_price,_SIDE_BID)

    def get_ask_reference_price(self,mid_price):
        if self.is_opposite_inventory(_SIDE_ASK):
            return self.get_opposite_inventory_price(mid_price,_SIDE_ASK) # self.get_opposite_inventory_price(mid_price,ASK)
        elif self.is_strong_direction(_SIDE_ASK):
            return self.get_strong_direction_price(mid_price,_SIDE_ASK) # self.get_strong_direction_price(mid_price,ASK)
        else:
            return self.get_regular_price(mid_price,_SIDE_ASK)

    def eval_imbalance_time(self):
        now=self.get_now()
        off_imbalance_time = self.clean_time_format(self.switched_off_imbalance_time)
        exit_time = time.strptime(off_imbalance_time, "%I:%M %p")
        exit_date_time = now.replace(hour=exit_time.tm_hour, minute=exit_time.tm_min, second=0, microsecond=0)
        return now<exit_date_time


    def imbalance_is_off(self):
        return  self.imbalance_activated == None or self.imbalance_activated == _IMBALANCE_BID_END or self.imbalance_activated == _IMBALANCE_ASK_END

    def bid_imbalance_still_active(self,imbalance_bid):
        # imbalance > entry imbalance (ex:75%) y activado y mayor que el exit_imbalance (ex:60%)

        span=None
        if self.imbalance_bid_first_triggered is not None:
            span = self.get_now() - self.imbalance_bid_first_triggered
            self.log("<imbalance> Bid- Elapsed {} seconds from imb. triggered (must wait {} seconds)".format(span.total_seconds(),self.imbalance_min_entry_sec))

        entry_cond = imbalance_bid > self.imbalance_min_threshold and (span is not None and span.seconds > self.imbalance_min_entry_sec)
        stil_on_cond =   self.imbalance_start is not None and self.imbalance_activated == _IMBALANCE_BID and imbalance_bid > self.imbalance_exit_threshold
        time_cond=self.eval_imbalance_time()

        return     (entry_cond or stil_on_cond) and  time_cond

    def eval_bid_imbalance_entry_conditions(self,mid_price,imbalance_bid):

        if imbalance_bid > self.imbalance_min_threshold and self.validate_imbalance_min_opx_timespan() and self.imbalance_bid_first_triggered is None:
            self.imbalance_bid_first_triggered=self.get_now()
            self.log("<imbalance>- Imbalance bid triggered at {}".format(self.imbalance_bid_first_triggered))

        return self.bid_imbalance_still_active(imbalance_bid)

    def get_bid_imbalance_spread(self,mid_price):

        if not self.validate_imbalance_min_opx_timespan():
            return 0

        tot_vol = self.mon_volume_ask + self.mon_volume_bid
        imbalance_bid = self.mon_volume_bid / tot_vol if tot_vol != 0 else 0
        # self.log("Imbalance Bid calcualted={} (tot_vol={} - mon_volume_bid={}".format(imbalance_bid,tot_vol,self.mon_volume_bid))

        if self.eval_bid_imbalance_entry_conditions(mid_price,imbalance_bid):
            #IMBALANCE ACTIVADO!
            net_excess_bid_imbalance = imbalance_bid - self.imbalance_min_threshold if imbalance_bid>self.imbalance_min_threshold else 0
            imb_multiplier = (int(decimal.Decimal( net_excess_bid_imbalance) / decimal.Decimal( self.imbalance_unit))) + 1
            bid_imb_spread = imb_multiplier * self.imbalance_price_delta

            if self.imbalance_is_off(): #just triggered
                self.imbalance_activated=_IMBALANCE_BID
                self.imbalance_start=self.get_now()
                self.imbalance_end = None

            self.log(
                "BID IMBALANCE ACTIVATED: Bid Imbalance={}==> Bid Imb. Spread calculated={} (multiplier={})"
                    .format(imbalance_bid,bid_imb_spread,imb_multiplier))

            return bid_imb_spread

        else:
            if self.imbalance_start is not None and self.imbalance_activated==_IMBALANCE_BID:
                self.imbalance_end=self.get_now()
                self.imbalance_activated=_IMBALANCE_BID_END
                self.imbalance_bid_first_triggered=None
                self.log("Closing bid imbalance at {} on threshold {}".format(self.get_now(),imbalance_bid))
            return 0

    def ask_imbalance_still_active(self,imbalance_ask):

        # imbalance > entry imbalance (ex:75%) y activado y mayor que el exit_imbalance (ex:60%)
        span = None
        if self.imbalance_ask_first_triggered is not None:
            span = self.get_now() - self.imbalance_ask_first_triggered
            self.log("<imbalance> Ask- Elapsed {} seconds from imb. triggered (must wait {} seconds)".format(span.total_seconds(), self.imbalance_min_entry_sec))

        entry_cond = imbalance_ask > self.imbalance_min_threshold and (span is not None and span.seconds > self.imbalance_min_entry_sec)
        stil_on_cond = self.imbalance_start is not None and self.imbalance_activated == _IMBALANCE_ASK and imbalance_ask > self.imbalance_exit_threshold
        time_cond = self.eval_imbalance_time()

        #imbalance > entry imbalance (ex:75%) y activado y mayor que el exit_imbalance (ex:60%)
        return  (entry_cond or stil_on_cond) and time_cond

    def eval_ask_imbalance_entry_conditions(self,mid_price,imbalance_ask):

        if imbalance_ask > self.imbalance_min_threshold and self.validate_imbalance_min_opx_timespan() and self.imbalance_ask_first_triggered is None:
            self.imbalance_ask_first_triggered=self.get_now()
            self.log("<imbalance>- Imbalance ask triggered at {}".format(self.imbalance_ask_first_triggered))

        return self.ask_imbalance_still_active(imbalance_ask)

    def get_ask_imbalance_spread(self,mid_price):

        if not self.validate_imbalance_min_opx_timespan():
            return 0

        tot_vol = self.mon_volume_ask + self.mon_volume_bid
        imbalance_ask = self.mon_volume_ask / tot_vol if tot_vol != 0 else 0
        # self.log("Imbalance Ask calcualted={} (tot_vol={} - mon_volume_ask={}".format(imbalance_ask, tot_vol,self.mon_volume_ask))

        if self.eval_ask_imbalance_entry_conditions(mid_price,imbalance_ask):
            net_excess_ask_imbalance = imbalance_ask - self.imbalance_min_threshold  if imbalance_ask>self.imbalance_min_threshold else 0
            imb_multiplier = (int(decimal.Decimal( net_excess_ask_imbalance) / decimal.Decimal( self.imbalance_unit))) + 1
            ask_imb_spread = imb_multiplier * self.imbalance_price_delta

            if self.imbalance_is_off():#just triggered!
                self.imbalance_activated=_IMBALANCE_ASK
                self.imbalance_start=self.get_now()
                self.imbalance_end=None

            self.log(
                "ASK IMBALANCE ACTIVATED: Ask Imbalance={}==> Ask Imb. Spread calculated={} (multiplier={})"
                    .format(imbalance_ask,ask_imb_spread,imb_multiplier))

            return ask_imb_spread
        else:
            if self.imbalance_start is not None and self.imbalance_activated == _IMBALANCE_ASK:
                self.imbalance_end = self.get_now()
                self.imbalance_activated = _IMBALANCE_ASK_END
                self.imbalance_ask_first_triggered=None
                self.log("Closing ask imbalance at {} on threshold {}".format(self.get_now(), imbalance_ask))
            return 0

    def get_bid_inventory_spread(self):

        # Cada <inv_threshold> unidades de la cantidad de contratos en inventario, DISMINUYO inventory_spread_unit en el precio del bid
        # cuanto mas long estoy, mas bajo es el precio que ofrezco
        # NOTA: El restultado de este método va a ser RESTADO al precio de bid

        if (self.net_shares > 0):
            return (int(self.net_shares / self.inv_threshold)) * self.inventory_spread_unit
        else:
            return 0

    def get_ask_inventory_spread(self):

        # Cada <inv_threshold> unidades de la cantidad de contratos, DISMINUYO inventory_spread_units en el precio del ask
        # cuanto mas short estoy, mas bajo es el precio que ofrezco
        # NOTA: El restultado de este método va a ser SUMADO al precio de bid
        if (self.net_shares < 0):
            return (int(abs(self.net_shares) / self.inv_threshold)) * self.inventory_spread_unit
        else:
            return 0

    def get_timestamp(self, data):

        # test =matplotlib.dates.num2date(737585.8739735069)
        if hasattr(data, 'datetime'):
            return data.datetime[0]  # arQuants
            # return  matplotlib.dates.num2date(data.datetime[0])  # arQuants
        else:
            return "-"

    def eval_depurate_imbalance(self):

        time_delta_last_depur_min = ((
                                                 self.get_now() - self.last_imbalance_depuration).total_seconds()) / 60
        if time_delta_last_depur_min > self.imbalance_depuration_threshold:
            # 1- Guardamos un temp de los snapshot
            temp_shares_bid = self.shares_bid
            temp_shares_ask = self.shares_ask
            temp_mon_volume_bid = self.mon_volume_bid
            temp_mon_volume_ask = self.mon_volume_ask

            # 2- Restamos de las variabls de cálculo del imbalance, el snapshot que tomamos en la última depuración
            self.shares_bid -= self.shares_bid_snapshot
            self.shares_ask -= self.shares_ask_snapshot
            self.mon_volume_bid -= self.mon_volume_bid_snapshot
            self.mon_volume_ask -= self.mon_volume_ask_snapshot

            # 3- Actualizamo las variables de snapshot que serán restada la próxima depuración
            self.shares_bid_snapshot = self.shares_bid
            self.shares_ask_snapshot = self.shares_ask
            self.mon_volume_bid_snapshot = self.mon_volume_bid
            self.mon_volume_ask_snapshot = self.mon_volume_ask

            self.log("{}-Depurating imbalance calculation: NewMonVolAsk={} NewMonVolBid={}".format(
                self.get_now(), self.mon_volume_ask, self.mon_volume_bid)
                     + " NewSharesAsk={} NewSharesBid={}".format(self.shares_ask, self.shares_bid)
                     + "  OldMonVolAsk={} OldMonVolBid={}".format(temp_mon_volume_ask, temp_mon_volume_bid)
                     + "  OldSharesAsk={} OldSharesBid={}".format(temp_shares_ask, temp_shares_bid)
                     )

            self.last_imbalance_depuration = self.get_now()

    def evaluate_new_highs_lows(self,data):

        if not self.evaluate_high_lows:
            return

        if self.new_high_lows_activated is None and len(self.candle_prices)>= (self.new_maxmin_start_count*5) and len(self.candle_prices)>= (self.new_maxmin_end_count*5):

            candlesToEval = self.candle_prices[-1 * self.new_maxmin_start_count:]
            maxCandle = max(candlesToEval)
            minCandle = min(candlesToEval)
            if maxCandle<=data.last_px[0]:
                self.new_high_lows_activated=_NEW_MAX
                self.new_high_lows_start=self.get_now()
            if minCandle>=data.last_px[0]:
                self.new_high_lows_activated=_NEW_MIN
                self.new_high_lows_start=self.get_now()

        elif self.new_high_lows_activated is not None and len(self.candle_prices)>= (self.new_maxmin_start_count*5) and len(self.candle_prices)>= (self.new_maxmin_end_count*5):
            candlesToEval = self.candle_prices[-1 * self.new_maxmin_end_count:]
            maxCandle = max(candlesToEval)
            minCandle = min(candlesToEval)

            if self.new_high_lows_activated == _NEW_MAX and minCandle >= data.last_px[0]:
                self.new_high_lows_end = self.get_now()
            if self.new_high_lows_activated == _NEW_MIN and maxCandle <= data.last_px[0]:
                self.new_high_lows_end = self.get_now()

    def persist_candle(self,data):

        if data.last_px[0] is not None and self.last_candle_timestamp is not None and  (self.get_now().minute != self.last_candle_timestamp.minute):
            self.candle_prices.append(data.last_px[0])
            self.last_candle_timestamp = self.get_now()
        elif self.last_candle_timestamp is None:
            self.last_candle_timestamp=self.get_now()

        self.evaluate_new_highs_lows(data)

    def calculate_imbalance(self, data):

        if hasattr(data, 'last_px'):

            # self.log("Calculating imbalance for last_px={} and last_size={} best_bid={} best_ask={}"
            #         .format(data.last_px[0], data.last_qty[0], data.bid_px[0], data.offer_px[0]))

            if (data.last_px[0] is None or data.last_qty[0] is None):
                return

            # self.log(("Evaluating new trade detected"))

            if ((self.last_price is None or self.last_price != data.last_px[0])
                    or (self.last_size is None or self.last_size != data.last_qty[0])
            ):

                # self.log("Evaluating trade impact for best bid={} best ask ={} and last px={}"
                #         .format(data.bid_px[0],data.offer_px[0],data.last_px[0]))

                dist_bid = abs(data.last_px[0] - data.bid_px[0])
                dist_ask = abs(data.last_px[0] - data.offer_px[0])

                # self.log("New trade detected: dist_bid={} dist_ask={}".format(dist_bid, dist_ask))

                if (dist_bid < dist_ask):  # most probably it was a sell

                    self.shares_bid += data.last_qty[0]
                    self.mon_volume_bid += data.last_qty[0] * data.last_px[0]
                    # self.log("New sell detected! New shares_bid={} New Mon. Volume Bid={}".format(self.shares_bid,self.mon_volume_bid))
                elif (dist_ask < dist_bid):  # most probably it was a sell

                    self.shares_ask += data.last_qty[0]
                    self.mon_volume_ask += data.last_qty[0] * data.last_px[0]
                    # self.log("New buy detected! New shares_ask={} New Mon. Volume Ask={}".format(self.shares_ask,self.mon_volume_ask))

                self.last_size = data.last_qty[0]
                self.last_price = data.last_px[0]  # arQuants


    # region Mid Price Strategies

    # mid price strategy #1 --> it's the easiest one.
    # the mid price is the last px
    def get_market_price_last_px(self, data):

        return data.last_px[0]

    # mid price strategy #2 --> it's the most volatile
    # the mid price is the average. If not round number --> we use the last_px
    def get_market_price_avg_to_last_px(self, data):
        if data.bid_px[0] is not None and data.offer_px[0] is not None:

            mid_price = (data.bid_px[0] + data.offer_px[0]) / 2

            mid_price = self.round_to_tick(mid_price)

            self.log("<mid-price #2>-Evaluating Last Px.={} Best Bid={} and Best Ask={}".format(data.last_px[0],
                                                                                                data.bid_px[0],
                                                                                                data.offer_px[0]))

            if (mid_price % self.default_spread) == 0:
                return mid_price
            else:
                return data.last_px[0]
        else:
            return data.last_px[0]

    # mid price strategy #3
    # the mid price is the average. If not round number --> we use the bid or ask depending on imbalance
    def get_market_price_avg_to_imbalance(self, data):

        if data.bid_px[0] is not None and data.offer_px[0] is not None:

            mid_price = (data.bid_px[0] + data.offer_px[0]) / 2

            if (mid_price % self.default_spread) != 0:
                mid_price = data.bid_px[0] if self.mon_volume_bid > self.mon_volume_ask else data.offer_px[0]

            self.log(
                "<mid-price #3>-Evaluating Last Px.={} Best Bid={} and Best Ask={} Def_Spread={} Mon. Volume Bid={} Mon.Volume.Ask={} Mid.Price={}"
                .format(data.last_px[0], data.bid_px[0], data.offer_px[0], self.default_spread, self.mon_volume_bid,
                        self.mon_volume_ask,mid_price))

            return mid_price

        else:
            return data.bid_px[0] if self.mon_volume_bid > self.mon_volume_ask else data.offer_px[0]

    def round_to_tick(self,price):

        if (price % self.tick_size) != 0:
            price = (int(price / self.tick_size)) * self.tick_size
            price = price if self.mon_volume_bid > self.mon_volume_ask else price + self.tick_size

        return price


    # mid price strategy #4
    # the mid price is the average. If not round number --> we use average up by tick on imbalance
    def get_market_price_avg_to_tick(self, data):

        if data.bid_px[0] is not None and data.offer_px[0] is not None \
            and not math.isnan(data.bid_px[0])  and not math.isnan(data.offer_px[0])  :

            mid_price = (data.bid_px[0] + data.offer_px[0]) / 2
            mid_price=self.round_to_tick(mid_price)

            self.log(
                "<mid-price #4>-Evaluating Last Px.={} Best Bid={} and Best Ask={} Def_Spread={} Mon. Volume Bid={} Mon.Volume.Ask={} Mid.Price={}"
                    .format(data.last_px[0], data.bid_px[0], data.offer_px[0], self.default_spread, self.mon_volume_bid,
                            self.mon_volume_ask, mid_price))

            return mid_price

        else:
            return data.bid_px[0] if self.mon_volume_bid > self.mon_volume_ask else data.offer_px[0]

    def get_market_price(self, data):

        ref_price = None
        if hasattr(data, 'last_px'):

            if self.mid_price_calculation == 1:
                ref_price = self.get_market_price_last_px(data)
            elif self.mid_price_calculation == 2:
                ref_price = self.get_market_price_avg_to_last_px(data)
            elif self.mid_price_calculation == 3:
                ref_price = self.get_market_price_avg_to_imbalance(data)
            elif self.mid_price_calculation == 4:
                ref_price = self.get_market_price_avg_to_tick(data)
            else:
                raise Exception("Invalid mid_price_calculation: {}".format(self.mid_price_calculation))



        elif hasattr(data, 'close'):
            ref_price = data.close[0]  # backtrader
        else:
            raise Exception("ERROR: Could not find market px in data structure!")

        return ref_price

    def get_trade_contract(self, data):

        trade_contract = None
        if hasattr(data, 'tradecontract'):
            trade_contract = data.tradecontract  # ArQuants
        elif hasattr(data, '_name'):
            trade_contract = data._name  # backtrader
        else:
            raise Exception("ERROR: Could not find trade contract in data structure!")

        return trade_contract

    def get_order_id(self, order):
        orderId = None
        if hasattr(order, 'm_orderId'):
            orderId = order.m_orderId  # ArQuants
        elif hasattr(order, 'tradeid'):
            orderId = order.tradeid  # backtrader
        else:
            raise Exception("ERROR: Could not find order id in  order!")

        return orderId

    def get_prev_cum_qty(self, orderId, newCumQty):

        if orderId not in self.CumQtyDict:
            self.CumQtyDict[orderId] = 0

        prevCumQty = self.CumQtyDict[orderId]
        self.CumQtyDict[orderId] = newCumQty
        return prevCumQty

    def get_last_qty(self, order):
        last_qty = None

        if hasattr(order, 'm_orderId'):
            prevCumQty = self.get_prev_cum_qty(order.m_orderId, order.executed.size)
            last_qty = order.executed.size - prevCumQty
            # last_qty = order.executed.size - (order.size - (self.routing_long if order.size >0 else self.routing_short))#ArQuants
            self.log(
                "last qty calculated on order.executed.size={} prevCumQty={} order.size={} routing_long={} routing_short={} ".format(
                    order.executed.size, prevCumQty, order.size, self.routing_long, self.routing_short))
        elif hasattr(order, 'tradeid'):
            last_qty = order.executed.size - self.get_prev_cum_qty(order.tradeid, order.executed.size)
            # last_qty=order.executed.size - (order.size - (self.routing_long if order.size >0 else self.routing_short)) #backtrader
        else:
            raise Exception("ERROR: Could not find order id in  order!")

        return last_qty

    def evaluate_trade(self, last_qty, price):
        # self.net_shares += trade.size
        # self.log("Recv notify_trade: {}".format(trade))

        if (last_qty < 0):
            self.net_sells += abs(last_qty * price)  # it's money I receive. It's a positive number
        else:
            self.net_buys += last_qty * price * -1  # it's money I pay. It's a negative number

        self.calcualte_net_profitability()

    def calcualte_net_profitability(self):

        profit = self.net_buys + self.net_sells + (self.net_shares * self.last_market_price)

        self.log("Net money for strategy:{} Net Shares={} Market Price={} NetBuys={} NetSells={}".format(
            profit, self.net_shares, round(self.last_market_price, 2), round(self.net_buys, 2),
            round(self.net_sells, 2)))

    def translate_status(self, order):
        if (order.status == Order.Submitted):
            return "Pending New"
        elif (order.status == Order.Created):
            return "Pending New"
        elif (order.status == Order.Accepted):
            return "New"
        elif (order.status == Order.Canceled):
            return "Canceled"
        elif (order.status == Order.Margin):
            return "Margin Rejected"
        elif (order.status == Order.Rejected):
            return "Rejected"
        elif (order.status == Order.Completed):
            return "Filled"
        elif (order.status == Order.Partial):
            return "Partially Filled"
        elif (order.status == Order.Expired):
            return "Expired"
        else:
            self.log(
                "ERROR!: Unknown order status: {} for order id {}".format(order.status, self.get_order_id(order)))

    # endregion

    #region Strategy.Public Methods

    def next(self):
        try:

            self.log_engine_status("next", self.data, "Recv market data")

            self._routing_lock.acquire(blocking=True)

            if self.is_closing_time():
                return

            self.calculate_imbalance(self.data)

            self.eval_depurate_imbalance()

            self.persist_candle(self.data)

            ref_price = self.get_market_price(self.data)
            trade_contract = self.get_trade_contract(self.data)

            prev_market_price = self.last_market_price
            self.last_market_price = ref_price

            if ref_price is not None and not math.isnan(ref_price):

                # self.log_engine_status("next",self.data, "New market price")
                if self.buy_order_id is not None and prev_market_price != self.last_market_price:
                    self.log("<buy>-Last Market Price changed - Running Replace: prev={} new={}".format(prev_market_price,
                                                                                                        self.last_market_price))
                    self.do_cancel_replace(ref_price,self.buy_order_id, isBuy=True)
                elif (self.buy_order_id is None and self.validate_going_long()):
                    buy_price = self.get_bid_reference_price(ref_price)
                    self.log("<buy>-sending order to complete bid: trade unit={} routing long={} price={}".format(
                        self.trade_unit, self.routing_long, buy_price))
                    self.do_buy(self.get_buy_qty(), buy_price)

                if self.sell_order_id is not None and prev_market_price != self.last_market_price:
                    self.log("<sell>-Last Market Price changed - Running Replace: prev={} new={}".format(prev_market_price,
                                                                                                         self.last_market_price))
                    self.do_cancel_replace(ref_price,self.sell_order_id, isBuy=False)
                elif (self.sell_order_id is None and self.validate_going_short()):
                    sell_price = self.get_ask_reference_price(ref_price)
                    self.log("<sell>-sending order to complete ask: trade unit={} routing long={} price={}".format(
                        self.trade_unit, self.routing_short, sell_price))
                    self.do_sell(self.get_sell_qty(), sell_price)

        except Exception as e:
            self.log(traceback.print_exc())
            msg = "Critical ERROR @MarketMaker.next:{}".format(str(e))
            self.log(msg)

        finally:
            if self._routing_lock.locked():
                self._routing_lock.release()

    def notify_order(self, order):
        try:
            self._routing_lock.acquire(blocking=True)

            if (order.status == Order.Submitted):
                self.process_order_started(order,updateOrder=False)
            elif (order.status == Order.Created):
                self.process_order_started(order,updateOrder=False)
            elif (order.status == Order.Accepted):
                self.process_order_started(order)
            elif (order.status == Order.Completed):
                self.process_order_traded(order, isPartial=False)
            elif (order.status == Order.Partial):
                self.process_order_traded(order, isPartial=True)
            elif (order.status == Order.Margin):
                self.process_order_finished(order)
            elif (order.status == Order.Rejected):
                self.process_order_finished(order)
            elif (order.status == Order.Canceled):
                self.process_order_finished(order)
            elif (order.status == Order.Expired):
                self.process_order_finished(order)
            else:
                self.log("ERROR!: Unknown order status: {} for order id {}".format(order.status, self.get_order_id(order)))

        except Exception as e:
            msg = "Critical ERROR @MarketMaker.notify_order:{}".format(str(e))
            self.log(msg)

        finally:
            if self._routing_lock.locked():
                self._routing_lock.release()

    def notify_trade(self, trade):
        # self.net_shares += trade.size
        # self.log("Recv notify_trade: {}".format(trade))
        pass

    # endregion
