import os
from logging.handlers import TimedRotatingFileHandler

from sources.client.framework.business_entities.backtest_parameter import *
from sources.client.strategies.market_maker.business_entities.market_making_summary import *
from sources.client.strategies.market_maker.data_access_layer.market_making_summary_manager import *
from sources.client.framework.util.backtester_proxy import *
from sources.client.framework.enums.backtest_period import *
from sources.framework.common.logger import *
import statistics
import threading
import queue
import time
import logging
from scipy import stats

class market_making_backtester_proxy(backtester_proxy):

    def __init__(self):
        pass

    def backtest_prestart(self, start_time):
        self.now = start_time
        self.process_start = start_time
        self.last_imbalance_depuration = start_time

    #region Util methods

    def log(self,msg):
        self.logger.info(msg)

    def calculate_reggr_slope(self):

        index=[]
        prices=[]

        i=1
        for price in self.candle_prices:
            index.append(i)
            prices.append(float(price))
            i+=1

        if len(prices)>1:
            reggr = stats.linregress(index,prices)

            return reggr.slope
        else:
            return 0

    def calculate_breadth(self):
        prices = []
        for price in self.candle_prices:
            prices.append(float(price))

        if len(prices)>0:

            max_price = max(prices)
            min_price = min(prices)
            return max_price-min_price
        else:
            return 0

    def calculate_prices_std_dev(self):
        profits=[]

        self.min_candles_for_std_dev = 60

        prev=None
        if len(self.candle_prices)>self.min_candles_for_std_dev:
            for price in self.candle_prices[-1*self.min_candles_for_std_dev:]:
                if prev is not None and prev !=0:
                    rend = (price/prev)-1
                    profits.append(rend)
                prev=price

        #self.candle_prices.clear()

        return statistics.stdev(profits)*100 if len(profits)>=2 else None

    def get_imbalance_bid(self):

        if self.mon_volume_bid is not None and self.mon_volume_bid > 0:
            return self.mon_volume_bid / (self.mon_volume_bid+self.mon_volume_ask)

    def get_imbalance_ask(self):

        if self.mon_volume_ask is not None and self.mon_volume_ask > 0:
            return self.mon_volume_ask / (self.mon_volume_bid + self.mon_volume_ask)


    #endregion

    #region Init Methods
    def on_init_backtester(self,p_configuration):
        self.data = None
        self.market_making_summary_manager = None
        self.periodic_persist = None
        self.ProxyName = "market_making_backtester_proxy"

        self.prepare_logs("MarketMakingAlgo")
        self.market_making_summary_manager=market_making_summary_manager(p_configuration.connection_string)
        self.periodic_persist= p_configuration.periodic_persist
        self.summaries_queue = queue.Queue(maxsize=100000)
        threading.Thread(target=self.persist_summary, args=()).start()

    def clean_imbalance_indicators(self):
        if self.imbalance_activated is not None and self.imbalance_activated.endswith("END"):
            self.imbalance_activated=None
            self.imbalance_start=None
            self.imbalance_end=None


    def clean_momentum_indicators(self):
        if self.new_high_lows_activated is not None and self.new_high_lows_end is not None:
            self.new_high_lows_activated=None
            self.new_high_lows_start=None
            self.new_high_lows_end=None


    def get_config_params(self, backtest):
        prms = []

        prms.append(backtest_parameter(backtest, p_key="max_long_qty",
                                       p_int_value=self.max_long_qty, p_string_value=None,
                                       p_float_value=None))
        prms.append(backtest_parameter(backtest, p_key="max_short_qty",
                                       p_int_value=self.max_short_qty, p_string_value=None,
                                       p_float_value=None))

        prms.append(backtest_parameter(backtest, p_key="default_spread",
                                       p_int_value=None, p_string_value=None,
                                       p_float_value=self.default_spread))

        prms.append(backtest_parameter(backtest, p_key="inventory_spread_unit",
                                       p_int_value=None, p_string_value=None,
                                       p_float_value=self.inventory_spread_unit))

        prms.append(backtest_parameter(backtest, p_key="trade_unit",
                                       p_int_value=None, p_string_value=None,
                                       p_float_value=self.trade_unit))

        prms.append(backtest_parameter(backtest, p_key="inv_threshold",
                                       p_int_value=None, p_string_value=None,
                                       p_float_value=self.inv_threshold))
        prms.append(backtest_parameter(backtest, p_key="impl_imbalance",
                                       p_int_value=None, p_string_value=self.impl_imbalance,
                                       p_float_value=None))

        prms.append(backtest_parameter(backtest, p_key="imbalance_min_threshold",
                                       p_int_value=None, p_string_value=None,
                                       p_float_value=float(self.imbalance_min_threshold)))

        prms.append(backtest_parameter(backtest, p_key="imbalance_unit",
                                       p_int_value=None, p_string_value=None,
                                       p_float_value=self.imbalance_unit))

        prms.append(backtest_parameter(backtest, p_key="imbalance_price_delta", p_int_value=None, p_string_value=None,
                                       p_float_value=self.imbalance_price_delta))

        prms.append(backtest_parameter(backtest, p_key="imbalance_min_opx_timespan", p_int_value=self.imbalance_min_opx_timespan,
                                        p_string_value=None, p_float_value=None))

        prms.append(backtest_parameter(backtest, p_key="imbalance_depuration_threshold",
                                       p_int_value=self.imbalance_depuration_threshold, p_string_value=None,
                                       p_float_value=None))
        prms.append(backtest_parameter(backtest, p_key="impl_closing_time",
                                       p_int_value=None, p_string_value=self.impl_closing_time,p_float_value=None))

        prms.append(backtest_parameter(backtest, p_key="closing_time", p_int_value=None, p_string_value=self.closing_time,
                                       p_float_value=None))
        prms.append(backtest_parameter(backtest, p_key="mid_price_calculation", p_int_value=None,
                                       p_string_value=self.mid_price_calculation, p_float_value=None))

        return prms


    #endregion

    #region Periodic Methods

    def persist_summary(self):

        while True:
            while not self.summaries_queue.empty():
                summary= self.summaries_queue.get()
                try:
                    self.market_making_summary_manager.parsist_market_making_summary(summary)
                except Exception as e:
                    self.log("Error persisting execution summary:{}".format(str(e)))
            time.sleep(0.001)

    def on_periodic_change(self,period ,backtest,p_start,p_end):
        summary = market_making_summary(p_type=period, p_backtest=backtest, p_date=self.now,
                                        p_start=p_start, p_end=p_end,
                                        p_profit=round(self.profit,2),
                                        p_net_shares=self.net_shares,
                                        p_net_buys=round(self.net_buys,2) if self.net_buys is not None else 0,
                                        p_net_sells=round(self.net_sells,2) if self.net_sells is not None else 0,
                                        p_imbalance_activated=self.imbalance_activated,
                                        p_imbalance_start=self.imbalance_start, p_imbalance_end=self.imbalance_end,
                                        p_nominal_volatility=self.calculate_prices_std_dev(),
                                        p_imbalance_bid=self.get_imbalance_bid(),
                                        p_imbalance_ask=self.get_imbalance_ask(),
                                        p_market_price=self.last_market_price,
                                        p_new_high_lows_type=self.new_high_lows_activated,
                                        p_new_high_lows_start=self.new_high_lows_start,
                                        p_new_high_lows_end=self.new_high_lows_end,
                                        p_reggr_slope=self.calculate_reggr_slope(),
                                        p_candle_breadth=self.calculate_breadth()
                                        )

        self.clean_imbalance_indicators()

        self.clean_momentum_indicators()

        self.summaries_queue.put(summary)


    def on_hourly_change(self, backtest,p_start,p_end):

        if self. periodic_persist==backtest_period.Hourly.value:
            self.on_periodic_change(backtest_period.Hourly ,backtest,p_start,p_end)

    def on_minute_change(self, backtest,p_start,p_end):
        if self.periodic_persist==backtest_period.Minute.value:
            self.on_periodic_change(backtest_period.Minute, backtest,p_start,p_end)

    def on_daily_change(self, backtest,p_start,p_end):
        if self.periodic_persist==backtest_period.Daily.value:
            self.on_periodic_change(backtest_period.Daily, backtest,p_start,p_end)

        self.candle_prices.clear()

    def get_cum_profit(self):
        return self.profit if self.profit is not None else 0

    def get_net_buys(self):
        return self.net_buys if self.net_buys is not None else 0

    def get_net_sells(self):
        return self.net_sells if self.net_sells is not None else 0

    def get_net_long_contracts(self):
        return self.net_long_contracts if self.net_long_contracts is not None else 0

    def get_net_short_contracts(self):
        return self.net_short_contracts if self.net_short_contracts is not None else 0

    def get_net_long_contracts_for_symbol(self,symbol):
        return self.get_net_long_contracts() #if it was a one security strategy

    def get_net_short_contracts_for_symbol(self,symbol):
        return self.get_net_short_contracts() #if it was a one security strategy

    def get_summary(self):
        return "<BACKTESTER SUMMARY! cum_profit={} day_profit={} cum_buys={} cum_sells={}".format(self.get_cum_profit(),
                self.get_cum_profit(),self.get_net_buys(), self.get_net_sells())


    #endregion

