class market_making_summary:
    def __init__(self,p_type,p_backtest,p_date,p_start, p_end,p_profit,p_net_shares,p_net_buys,p_net_sells,p_imbalance_activated,
                 p_imbalance_start,p_imbalance_end,p_nominal_volatility,p_imbalance_bid,p_imbalance_ask,p_market_price,
                 p_new_high_lows_type,p_new_high_lows_start,p_new_high_lows_end, p_reggr_slope,p_candle_breadth):

        self.type=p_type
        self.backtest=p_backtest
        self.date=p_date
        self.start=p_start
        self.end=p_end
        self.profit=p_profit
        self.net_shares=p_net_shares
        self.net_buys=p_net_buys
        self.net_sells=p_net_sells
        self.imbalance_activated = p_imbalance_activated
        self.imbalance_start = p_imbalance_start
        self.imbalance_end = p_imbalance_end
        self.nominal_volatility = p_nominal_volatility

        self.imbalance_bid=p_imbalance_bid
        self.imbalance_ask=p_imbalance_ask
        self.market_price=p_market_price

        self.new_high_lows_type=p_new_high_lows_type
        self.new_high_lows_start=p_new_high_lows_start
        self.new_high_lows_end=p_new_high_lows_end
        self.reggr_slope=p_reggr_slope
        self.candle_breadth=p_candle_breadth
