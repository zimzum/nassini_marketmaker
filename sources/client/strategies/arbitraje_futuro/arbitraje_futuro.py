#from arquants import Strategy
#from arquants import Order
from backtrader import Order
from enum import Enum
from math import isnan
from datetime import datetime, timedelta
import arqLogger

from sources.client.strategies.arbitraje_futuro.common.util.arbitraje_futuro_backtester_proxy import \
    arbitraje_futuro_backtester_proxy


class Stage(Enum):
    Stage1 = 1
    Stage2 = 2
    ContingencyBreakEven = 3
    ContingencyStopLoss = 4
    ContingencyTimeout = 5


class OrderSide(Enum):
    Bid = 1
    Offer = 2


MAX_DECIMALS = 4


MIN_TICK_VALUE = {
    'rofex': 5,
    'usd': 0.01,
    'ggal': 0.05
}


CONTRACT_SIZE = {
    'rofex': 1,
    'ggal': 100,
    'usd': 1000
}


CONTRACT_COMISSION = {
    'rofex': 0.00024,
    'ggal': 0.00024,
    'usd': 0.2
}


CONTRACT_PREFIX = {
    'RFX': 'rofex',
    'GGAL': 'ggal',
    'DO': 'usd'
}

MONTHS = ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic']


class OrderPack:
    stage = Stage.Stage1
    roll_over_order = ''
    short_order = ''
    long_order = ''
    side = OrderSide.Bid
    trade_no = 1
    long_md_px = 0
    short_md_px = 0

    def as_list(self):
        ret = []
        if self.roll_over_order != '':
            ret.append(self.roll_over_order)
        if self.long_order != '':
            ret.append(self.long_order)
        if self.short_order != '':
            ret.append(self.short_order)
        return ret


class ArbitrajeFuturo:
    rollover_md = None
    long_md = None
    short_md = None
    p_change = {}

    def __init__(self, log, cancel, buy, sell, alert, get_order_info, timer_contingency, stop_loss,
                 spread_contingency, order_timer_sec, max_profit, min_profit, max_order_size, tick, usd_ars, print_scr):
        self.logger: arqLogger.ArquantLogger = log
        self.cancel = cancel
        self.buy = buy
        self.sell = sell
        self.alert = alert
        self.get_order_info = get_order_info
        self.timer_contingency_sec = timer_contingency
        self.stop_loss = stop_loss
        self.spread_contingency = spread_contingency
        self.order_timer_sec = order_timer_sec
        self.max_profit = max_profit
        self.min_profit = min_profit
        self.max_order_size = max_order_size
        self.tick = tick
        self.usd_ars = usd_ars
        self._print = print_scr

        self.bid_rollover = OrderPack()
        self.bid_rollover.side = OrderSide.Bid
        self.offer_rollover = OrderPack()
        self.offer_rollover.side = OrderSide.Offer

    def print(self, msg, side = None):
        if side is None:
            self._print("%s: %s" % (self.rollover_md.tradecontract, msg))
        else:
            self._print("%s - %s: %s" % (self.rollover_md.tradecontract, repr(side), msg))

    def next(self):
        if self.p_change != {} and self.offer_rollover.stage == Stage.Stage1 and \
                self.bid_rollover.stage == Stage.Stage1:
            n = self.p_change
            if 'min_profit' in n:
                self.min_profit = n['min_profit']
            if 'max_profit' in n:
                self.max_profit = n['max_profit']
            if 'usd_ars' in n:
                self.usd_ars = n['usd_ars']
            if 'max_order_size' in n:
                self.max_order_size = n['max_order_size']
            if 'timer_contingency_sec' in n:
                self.timer_contingency_sec = n['timer_contingency_sec']
            if 'order_timer_sec' in n:
                self.order_timer_sec = n['order_timer_sec']
            if 'stop_loss' in n:
                self.stop_loss = n['stop_loss']
            if 'spread_contingency' in n:
                self.spread_contingency = n['spread_contingency']
            self.p_change = {}
            self.logger.log_strategy({"stage": repr(Stage.Stage1), "description": "Actualizacion de parametros",
                                      "strategy": self.rollover_md.tradecontract})

        if self.bid_rollover.stage == Stage.Stage1:
            self.logger.log_strategy({"side": repr(OrderSide.Bid), "stage": repr(self.bid_rollover.stage),
                                      "description": "Recalculo precio pase",
                                      "strategy": self.rollover_md.tradecontract})
            self.recalculate_rollover_price(OrderSide.Bid)
        elif self.bid_rollover.stage in [Stage.Stage2, Stage.ContingencyStopLoss,
                                         Stage.ContingencyBreakEven]:
            self.logger.log_strategy({"side": repr(OrderSide.Bid), "stage": repr(self.bid_rollover.stage),
                                      "description": "Cancela ordenes",
                                      "strategy": self.rollover_md.tradecontract})
            self.cancel_rollover_orders(OrderSide.Bid)

        if self.offer_rollover.stage == Stage.Stage1:
            self.logger.log_strategy({"side": repr(OrderSide.Offer), "stage": repr(self.offer_rollover.stage),
                                      "description": "Recalculo precio pase",
                                      "strategy": self.rollover_md.tradecontract})
            self.recalculate_rollover_price(OrderSide.Offer)
        elif self.offer_rollover.stage in [Stage.Stage2, Stage.ContingencyStopLoss,
                                           Stage.ContingencyBreakEven]:
            self.logger.log_strategy({"side": repr(OrderSide.Offer), "stage": repr(self.offer_rollover.stage),
                                      "description": "Cancela ordenes",
                                      "strategy": self.rollover_md.tradecontract})
            self.cancel_rollover_orders(OrderSide.Offer)

        if self.bid_rollover.stage in [Stage.ContingencyStopLoss, Stage.ContingencyBreakEven]:
            self.logger.log_strategy({"side": repr(OrderSide.Bid), "stage": repr(self.bid_rollover.stage),
                                      "description": "Contingencia, verificacion de spread",
                                      "strategy": self.rollover_md.tradecontract})
            self.check_spread(OrderSide.Bid)

        if self.offer_rollover.stage in [Stage.ContingencyStopLoss, Stage.ContingencyBreakEven]:
            self.logger.log_strategy({"side": repr(OrderSide.Offer), "stage": repr(self.offer_rollover.stage),
                                      "description": "Contingencia, verificacion de spread",
                                      "strategy": self.rollover_md.tradecontract})
            self.check_spread(OrderSide.Offer)

    def notify_order(self, order_id):
        order = self.get_order_info(order_id)
        if order is None:
            return
        self.logger.log_er_event(order=order, additional={"strategy": self.rollover_md.tradecontract})
        if order.status in [Order.Canceled]:
            return
        if order.status in [Order.Rejected]:
            self.alert("Orden %s rechazada. Checkear manualmente" % order_id)
            return
        side = OrderSide.Bid
        if order_id in self.offer_rollover.as_list():
            side = OrderSide.Offer
        self.logger.log_strategy({"description": 'Notificacion orden %s' % order_id, "side": repr(side),
                                  "strategy": self.rollover_md.tradecontract})
        self.notify_order_side(side)

    def notify_order_side(self, side):
        roll_over = self.bid_rollover
        if side == OrderSide.Offer:
            roll_over = self.offer_rollover
        self.logger.log_strategy({"description": 'Notificacion orden', "side": repr(side),
                                  'stage': repr(roll_over.stage),
                                  "strategy": self.rollover_md.tradecontract})
        roll_over_order = self.get_order_info(roll_over.roll_over_order)
        long_order = self.get_order_info(roll_over.long_order)
        short_order = self.get_order_info(roll_over.short_order)
        if roll_over.stage == Stage.Stage1:
            if is_valid(roll_over_order) and (
                    roll_over_order.status in [Order.Completed, Order.Partial]):
                self.logger.log_strategy({"description": 'Cambio a Stage 2', 'side': repr(side),
                                          'stage': repr(roll_over.stage),
                                          "strategy": self.rollover_md.tradecontract})
                self.change_to_stage2(side)
        elif roll_over.stage == Stage.Stage2:
            if is_valid(short_order) and is_valid(long_order):
                both_orders_filled = short_order.status == Order.Completed and \
                                     long_order.status == Order.Completed
                if both_orders_filled:
                    self.logger.log_strategy(
                        {"description": 'Cambio a Stage 1', 'side': repr(side), 'stage': repr(roll_over.stage),
                         "strategy": self.rollover_md.tradecontract})
                    self.print("Nuevo trade: %d" % (roll_over.trade_no), side)
                    self.change_to_stage1(side)
                else:
                    self.logger.log_strategy(
                        {"description": 'Cambio a Contingencia', 'side': repr(side), 'stage': repr(roll_over.stage),
                         "strategy": self.rollover_md.tradecontract})
                    self.start_contingency_breakeven(side)
        elif roll_over.stage in [Stage.ContingencyBreakEven, Stage.ContingencyStopLoss]:
            if is_valid(short_order) and is_valid(long_order):
                both_orders_filled = short_order.status == Order.Completed and \
                                     long_order.status == Order.Completed
                if both_orders_filled:
                    self.logger.log_strategy(
                        {"description": 'Cambio a Stage 1', 'side': repr(side), 'stage': repr(roll_over.stage),
                         "strategy": self.rollover_md.tradecontract})
                    self.print("Nuevo trade: %d" % (roll_over.trade_no), side)
                    self.change_to_stage1(side)

    def change_to_stage1(self, side):
        roll_over = self.bid_rollover
        if side == OrderSide.Offer:
            roll_over = self.offer_rollover

        roll_over.stage = Stage.Stage1
        roll_over.trade_no += 1
        roll_over.short_order = ''
        roll_over.long_order = ''
        roll_over.roll_over_order = ''

    def recalculate_rollover_bid(self):
        roll_over_px = float('nan')
        roll_over_sz = float('nan')
        roll_over = self.bid_rollover
        roll_over_order = self.get_order_info(roll_over.roll_over_order)
        self.logger.log_md_event(data_list=[self.short_md, self.long_md, self.rollover_md],
                                 entry_list=[['offer_px', 'offer_qty'], ['bid_px', 'bid_qty'],
                                             ['bid_px', 'bid_qty', 'bid_px_2', 'bid_qty_2']],
                                 additional={"rollover": repr(OrderSide.Bid), "strategy": self.rollover_md.tradecontract})
        has_marketdata = not isnan(self.long_md.bid_px[0]) and not isnan(self.short_md.offer_px[0])
        if has_marketdata:
            roll_over.short_md_px = self.short_md.offer_px[0]
            roll_over.long_md_px = self.long_md.bid_px[0]
            roll_over_px = self.long_md.bid_px[0] - self.short_md.offer_px[0] - self.max_profit
            c = comission_price(self.short_md.tradecontract, self.short_md.offer_px[0], self.long_md.bid_px[0],
                                roll_over_px, self.usd_ars)
            self.logger.log_strategy({"Calculo comision": c, "strategy": self.rollover_md.tradecontract})
            roll_over_px -= c
            roll_over_px = round_step(roll_over_px, self.tick)
            roll_over_bid_px = round_step(self.rollover_md.bid_px[0], self.tick)
            roll_over_sz = min(self.long_md.bid_qty[0], self.short_md.offer_qty[0], self.max_order_size)

            best_price = isnan(roll_over_bid_px) or (roll_over_bid_px < roll_over_px)
            if best_price:
                self.logger.log_strategy({"side": repr(OrderSide.Bid), "price": roll_over_px, "size": roll_over_sz,
                                          "description": "Poner orden optima",
                                          "strategy": self.rollover_md.tradecontract})
                return roll_over_px, roll_over_sz
            else:
                has_order = is_valid(roll_over_order)
                bid_to_beat = round_step(roll_over_bid_px + self.tick, self.tick)
                max_roll_over_px = round_step(roll_over_px + self.max_profit - self.min_profit, self.tick)
                is_on_top = (roll_over_order is not None) and (roll_over_order.price >= roll_over_bid_px)
                ob = {
                        "side": repr(OrderSide.Bid), "has_order": has_order, "max_ro_px": max_roll_over_px,
                        "bid_to_beat": bid_to_beat, "is_on_top": is_on_top,
                        "strategy": self.rollover_md.tradecontract, "roll_over_px": roll_over_px,
                        "roll_over_sz": roll_over_sz
                }
                if has_order:
                    ob.update({
                        "order_px": roll_over_order.price, "order_sz": roll_over_order.size,
                    })

                if has_order and is_on_top and max_roll_over_px >= roll_over_order.price:
                    can_be_improved = \
                        (round_step(roll_over_order.price - self.rollover_md.bid_px_2, self.tick)) > self.tick
                    if can_be_improved:
                        new_price = round_step(self.rollover_md.bid_px_2 + self.tick, self.tick)
                        ob.update({"description": "Superar oferta", "price": new_price})
                        self.logger.log_strategy(ob)
                        return new_price, roll_over_sz
                    else:
                        ob.update({"description": "Sin cambios, precio optimo", "price": roll_over_order.price})
                        self.logger.log_strategy(ob)
                        return roll_over_order.price, roll_over_sz
                elif max_roll_over_px >= bid_to_beat:
                    ob.update({"description": "Colocar orden", "price": bid_to_beat})
                    self.logger.log_strategy(ob)
                    return bid_to_beat, roll_over_sz
                else:
                    ob.update({"description": "No satisface profit", "price": float('nan')})
                    self.logger.log_strategy(ob)
                    return float('nan'), roll_over_sz
        else:
            self.logger.log_strategy({"side": repr(OrderSide.Bid), "price": roll_over_px, "size": roll_over_sz,
                                      "description": "No MD", "strategy": self.rollover_md.tradecontract})
            return roll_over_px, roll_over_sz

    def recalculate_rollover_offer(self):
        roll_over_px = float('nan')
        roll_over_sz = float('nan')
        roll_over = self.offer_rollover
        roll_over_order = self.get_order_info(roll_over.roll_over_order)
        self.logger.log_md_event(data_list=[self.long_md, self.short_md, self.rollover_md],
                                 entry_list=[['offer_px', 'offer_qty'], ['bid_px', 'bid_qty'],
                                             ['offer_px', 'offer_qty', 'offer_px_2', 'offer_px_2']],
                                 additional={"rollover": repr(OrderSide.Offer),
                                             "strategy": self.rollover_md.tradecontract})
        has_marketdata = not isnan(self.long_md.offer_px[0]) and not isnan(self.short_md.bid_px[0])
        if has_marketdata:
            roll_over.short_md_px = self.short_md.bid_px[0]
            roll_over.long_md_px = self.long_md.offer_px[0]
            roll_over_offer_px = round_step(self.rollover_md.offer_px[0], self.tick)
            roll_over_px = self.long_md.offer_px[0] - self.short_md.bid_px[0] + self.max_profit
            c = comission_price(self.short_md.tradecontract, self.short_md.bid_px[0], self.long_md.offer_px[0],
                                roll_over_px, self.usd_ars)
            self.logger.log_strategy({"Calculo comision": c, "strategy": self.rollover_md.tradecontract})
            roll_over_px += c
            roll_over_px = round_step(roll_over_px, self.tick)
            roll_over_sz = min(self.long_md.offer_qty[0],
                               self.short_md.bid_qty[0], self.max_order_size)
            best_price = isnan(roll_over_offer_px) or roll_over_offer_px > roll_over_px
            if best_price:
                self.logger.log_strategy({"side": repr(OrderSide.Offer), "price": roll_over_px, "size": roll_over_sz,
                                          "description": "Poner orden optima",
                                          "strategy": self.rollover_md.tradecontract})
                return roll_over_px, roll_over_sz
            else:
                has_order = is_valid(roll_over_order)
                offer_to_beat = round_step(roll_over_offer_px - self.tick, self.tick)
                min_roll_over_px = round_step(roll_over_px - self.max_profit + self.min_profit, self.tick)
                is_on_top = (roll_over_order is not None) and (roll_over_order.price <= roll_over_offer_px)

                ob = {
                        "side": repr(OrderSide.Offer), "has_order": has_order, "min_ro_px": min_roll_over_px,
                        "offer_to_beat": offer_to_beat, "is_on_top": is_on_top,
                        "strategy": self.rollover_md.tradecontract, "roll_over_px": roll_over_px,
                        "roll_over_sz": roll_over_sz
                }
                if has_order:
                    ob.update({
                        "order_px": roll_over_order.price, "order_sz": roll_over_order.size,
                    })
                if has_order and is_on_top and min_roll_over_px <= roll_over_order.price:
                    can_be_improved = \
                            (round_step(self.rollover_md.offer_px_2[0] - roll_over_order.price, self.tick)) > self.tick
                    if can_be_improved:
                        new_price = round_step(self.rollover_md.offer_px_2[0] - self.tick, self.tick)
                        ob.update({"description": "Superar oferta", "roll_over_px": new_price})
                        self.logger.log_strategy(ob)
                        return new_price, roll_over_sz
                    else:
                        ob.update({"price": roll_over_order.price, "description": "Sin cambios, precio optimo"})
                        self.logger.log_strategy(ob)
                        return roll_over_order.price, roll_over_sz
                elif min_roll_over_px <= offer_to_beat:
                    ob.update({"price": offer_to_beat, "description": "Colocar orden"})
                    self.logger.log_strategy(ob)
                    return offer_to_beat, roll_over_sz
                else:
                    ob.update({"description": "No satisface profit", "price": float("nan")})
                    self.logger.log_strategy(ob)
                    return float('nan'), roll_over_sz
        else:
            self.logger.log_strategy({"side": repr(OrderSide.Offer), "price": roll_over_px, "size": roll_over_sz,
                                      "description": "No MD", "strategy": self.rollover_md.tradecontract})
            return roll_over_px, roll_over_sz

    def is_price_change_allowed(self, side, roll_over_px, roll_over_sz):
        roll_over = self.offer_rollover
        if side == OrderSide.Bid:
            roll_over = self.bid_rollover
        roll_over_order = self.get_order_info(roll_over.roll_over_order)
        if not is_valid(roll_over_order):
            return True
        if abs(roll_over_order.size) != roll_over_sz:
            return True
        if side == OrderSide.Bid and (isnan(roll_over_px) or (roll_over_order.price < roll_over_px)):
            return True
        if side == OrderSide.Offer and (isnan(roll_over_px) or (roll_over_order.price > roll_over_px)):
            return True
        return False

    def recalculate_rollover_price(self, side):
        roll_over = self.bid_rollover
        if side == OrderSide.Offer:
            roll_over = self.offer_rollover
        roll_over_order = self.get_order_info(roll_over.roll_over_order)
        try:
            roll_over_px = float('nan')
            roll_over_sz = float('nan')

            if side == OrderSide.Bid:
                roll_over_px, roll_over_sz = self.recalculate_rollover_bid()
            elif side == OrderSide.Offer:
                roll_over_px, roll_over_sz = self.recalculate_rollover_offer()

            has_order = is_valid(roll_over_order)
            size_changed = False
            price_changed = False
            if has_order:
                size_changed = abs(roll_over_order.size) != roll_over_sz
                price_changed = round_step(roll_over_order.price, self.tick) != roll_over_px
                self.logger.log_er_event(roll_over_order, additional={"side": repr(side),
                                                                      "strategy": self.rollover_md.tradecontract})
            change_allowed = self.is_price_change_allowed(side, roll_over_px, roll_over_sz)
            bid_roll_order_changed = change_allowed and (not has_order or size_changed or price_changed)
            if bid_roll_order_changed:
                if has_order:
                    roll_over.roll_over_order = self.cancel(roll_over.roll_over_order)
                if not isnan(roll_over_sz) and not isnan(roll_over_px):
                    if side == OrderSide.Bid:
                        roll_over.roll_over_order = self.buy(data=self.rollover_md,
                                                             price=roll_over_px, size=roll_over_sz,
                                                             exectype=Order.Limit)
                        self.logger.log_new_order_response(order=self.get_order_info(roll_over.roll_over_order),
                                                           additional={"side": repr(side),
                                                                       "strategy": self.rollover_md.tradecontract})
                    elif side == OrderSide.Offer:
                        roll_over.roll_over_order = self.sell(data=self.rollover_md,
                                                              price=roll_over_px, size=roll_over_sz,
                                                              exectype=Order.Limit)
                        self.logger.log_new_order_response(order=self.get_order_info(roll_over.roll_over_order),
                                                           additional={"side": repr(side),
                                                                       "strategy": self.rollover_md.tradecontract})
        except Exception as e:
            self.logger.log_error_event("recalculate_rollover_price " + repr(e),
                                        additional={"error": repr(e), "strategy": self.rollover_md.tradecontract})
            if is_valid(roll_over_order):
                roll_over.roll_over_order = self.cancel(roll_over.roll_over_order)

    def cancel_rollover_orders(self, side):
        roll_over = self.bid_rollover
        if side == OrderSide.Offer:
            roll_over = self.offer_rollover

        roll_over_order = self.get_order_info(roll_over.roll_over_order)
        if is_valid(roll_over_order):
            if roll_over_order.status != Order.Completed:
                roll_over.roll_over_order = self.cancel(roll_over.roll_over_order)

    def check_spread(self, side):
        roll_over = self.bid_rollover
        if side == OrderSide.Offer:
            roll_over = self.offer_rollover
        long_order = self.get_order_info(roll_over.long_order)
        short_order = self.get_order_info(roll_over.short_order)
        market_roll_over = self.long_md.bid_px[0] - self.short_md.offer_px[0]
        if side == OrderSide.Offer:
            market_roll_over = self.long_md.offer_px[0] - self.short_md.bid_px[0]
        my_roll_over = long_order.price - short_order.price
        spread = abs(market_roll_over / my_roll_over)
        if spread > self.spread_contingency:
            msg = "%s, El spread actual superó el limite: %.2f" % (side, spread)
            self.alert(msg)
            self.logger.log_strategy(data={"side": repr(side), "description": msg,
                                           "strategy": self.rollover_md.tradecontract})

    def change_to_stage2(self, side):
        roll_over = self.bid_rollover
        if side == OrderSide.Offer:
            roll_over = self.offer_rollover
        roll_over_order = self.get_order_info(roll_over.roll_over_order)
        roll_over.stage = Stage.Stage2
        if side == OrderSide.Bid:
            roll_over.short_order = self.buy(data=self.short_md, price=roll_over.short_md_px,
                                             size=roll_over_order.executed.size, exectype=Order.Limit)
            roll_over.long_order = self.sell(data=self.long_md, price=roll_over.long_md_px,
                                             size=roll_over_order.executed.size, exectype=Order.Limit)
        elif side == OrderSide.Offer:
            roll_over.short_order = self.sell(data=self.short_md, price=roll_over.short_md_px,
                                              size=roll_over_order.executed.size, exectype=Order.Limit)
            roll_over.long_order = self.buy(data=self.long_md, price=roll_over.long_md_px,
                                            size=roll_over_order.executed.size, exectype=Order.Limit)
        if roll_over_order.status != Order.Completed:
            roll_over.roll_over_order = self.cancel(roll_over.roll_over_order)

    def on_contingency_timeout(self, side):
        self.logger.log_strategy(data={"side": repr(side), "description": "Timeout contingencia",
                                       "strategy": self.rollover_md.tradecontract})
        roll_over = self.bid_rollover
        if side == OrderSide.Offer:
            roll_over = self.offer_rollover
        short_order = self.get_order_info(roll_over.short_order)
        long_order = self.get_order_info(roll_over.long_order)

        if roll_over.stage == Stage.ContingencyBreakEven:
            self.logger.log_strategy(data={"side": repr(side), "description": "Contingencia - stoploss",
                                           "strategy": self.rollover_md.tradecontract})
            self.print("Contingencia - stoploss", side)
            roll_over.stage = Stage.ContingencyStopLoss

            short_order_px = round_step(short_order.executed.price * (1 + (self.stop_loss / 2)), self.tick)
            long_order_px = round_step(long_order.executed.price * (1 - (self.stop_loss / 2)), self.tick)
            short_order_sz = short_order.created.size - short_order.executed.size
            long_order_sz = long_order.created.size - long_order.executed.size
            roll_over.long_order = self.cancel(roll_over.long_order)
            roll_over.short_order = self.cancel(roll_over.short_order)

            roll_over.short_order = self.buy(data=self.short_md, price=short_order_px,
                                             size=short_order_sz,
                                             exectype=Order.Limit)
            roll_over.long_order = self.sell(data=self.long_md, price=long_order_px,
                                             size=long_order_sz,
                                             exectype=Order.Limit)
            self.logger.log_new_order_response(order=self.get_order_info(roll_over.short_order),
                                               additional={"side": repr(side), "description": "Contingencia stoploss",
                                                           "strategy": self.rollover_md.tradecontract})
            self.logger.log_new_order_response(order=self.get_order_info(roll_over.long_order),
                                               additional={"side": repr(side), "description": "Contingencia stoploss",
                                                           "strategy": self.rollover_md.tradecontract})
        elif roll_over.stage == Stage.ContingencyStopLoss:
            roll_over.stage = Stage.ContingencyTimeout
            roll_over.roll_over_order = self.cancel(roll_over.roll_over_order)
            roll_over.short_order = self.cancel(roll_over.short_order)
            roll_over.long_order = self.cancel(roll_over.long_order)
            self.logger.log_strategy(data={"side": repr(side),
                                           "description": "Contingencia - timeout. Estrategia detenida",
                                           "strategy": self.rollover_md.tradecontract})
            self.print("Contingencia - timeout. Estrategia detenida", side)

    def start_contingency_breakeven(self, side):
        roll_over = self.bid_rollover
        if side == OrderSide.Offer:
            roll_over = self.offer_rollover
        self.logger.log_strategy(data={"side": repr(side), "description": "Contingencia - breakeven",
                                       "strategy": self.rollover_md.tradecontract})
        self.print("Contingencia - breakeven", side)
        roll_over.stage = Stage.ContingencyBreakEven

    def process_parameter_change(self, new_data):
        new_data = dict(new_data)
        self.logger.log_strategy(data={"description": "Cambio de parametros", "new_data": repr(new_data)})
        if self.p_change != {}:
            self.p_change.update(new_data)
        else:
            self.p_change = new_data
        if 'max_profit' in new_data and 'min_profit' in new_data:
            if new_data['min_profit'] > new_data['max_profit']:
                self.print("Cambio de parametros de profit no efectuado. Max profit es menor a min profit")
                self.p_change.pop('min_profit')
                self.p_change.pop('max_profit')
        elif 'max_profit' in new_data:
            if self.min_profit > new_data['max_profit']:
                self.print("Cambio de parametros de profit no efectuado. Max profit es menor a min profit")
                self.p_change.pop('max_profit')
        elif 'min_profit' in new_data:
            if new_data['min_profit'] > self.max_profit:
                self.print("Cambio de parametros de profit no efectuado. Max profit es menor a min profit")
                self.p_change.pop('min_profit')
        if 'reset_estrategia' in new_data and self.offer_rollover.stage == Stage.ContingencyTimeout:
            self.print("Cambio de parametros. Pase offer rehabilitado")
            self.offer_rollover.stage = Stage.Stage1
        if 'reset_estrategia' in new_data and self.bid_rollover.stage == Stage.ContingencyTimeout:
            self.print("Cambio de parametros. Pase bid rehabilitado")
            self.bid_rollover.stage = Stage.Stage1


def contract_type(data):
    symbol = ''
    if type(data) is str:
        symbol = data
    else:
        symbol = data.tradecontract
    for key in CONTRACT_PREFIX:
        value = CONTRACT_PREFIX[key]
        if symbol.startswith(key):
            try:
                return {
                    'prefix': key,
                    'contract_type': value,
                    'tick': MIN_TICK_VALUE[value],
                    'contract_size': CONTRACT_SIZE[value],
                    'comission': CONTRACT_COMISSION[value]
                }
            except Exception as e:
                raise Exception("El contrato no esta soportado. Err: %s" % e)
    raise Exception('El contrato no esta soportado')


def get_contract_expiry(contract_name: str):
    month = -1
    for i in range(len(MONTHS)):
        if MONTHS[i] in contract_name:
            month = i
    if month == -1:
        raise Exception('Fecha no encontrada en %s' % contract_name)
    year = contract_name[contract_name.index(MONTHS[month]) + len(MONTHS[month]):]
    if not year.isdigit():
        year = year[:len(year) - 1]
    if not year.isdigit():
        raise Exception('Fecha no encontrada en %s' % contract_name)
    return datetime.strptime(year + '-' + str(month + 1), '%y-%m')


def is_valid(order):
    try:
        return (order is not None) and (order.status not in [Order.Canceled, Order.Cancelled, Order.Rejected])
    except:
        return False


def round_step(number, step):
    if isnan(number) or isnan(step):
        return float('nan')
    return round(round(float(number) / step) * step, MAX_DECIMALS)


def comission_price(symbol, price_front, price_further, price_rollover, usd_ars):
    t = contract_type(symbol)
    comission = t['comission']
    if t['contract_type'] in ['usd']:
        return 3 * comission * usd_ars / t['contract_size']
    else:
        return (price_front + price_further + price_rollover) * comission / t['contract_size']


class StrategyInfoItem:
    strategy: ArbitrajeFuturo = None
    rollover_data_idx = -1
    front_data_idx = -1
    further_data_idx = -1


class OrderItem:
    strategy_idx: int
    order: Order

class ArbitrajeMultiFuturos(arbitraje_futuro_backtester_proxy):
#class ArbitrajeMultiFuturos(Strategy):
    strategies: [StrategyInfoItem] = []
    orders = {}
    cancelled_orders = []
    data_idx: [int] = []
    strategies_running = 0

    def __init__(self, min_profit=10, max_profit=100, usd_ars=78.22, max_order_size=5, timer_contingency_sec=60, order_timer_sec=3, stop_loss=.05, spread_contingency=.1, reset_estrategia=0):
        if min_profit > max_profit:
            raise Exception("Max profit es menor a min profit. Relanzar con una configuracion correcta")
        self.logger = arqLogger.ArquantLogger(
            url="ws://54.226.117.73:3000/",
            strategy=self,
            strategy_id="Arbitraje Pases",
            param_list=locals(),
            client_id="ArbitrajePaseNasini")
        self.logger.connect()

        self.log("Execution ID: " + repr(self.logger.execution_id))
        # Verificacion de integridad (cantidad de instrumentos)
        if (self.count_sym() % 3) != 0:
            for i in range(self.count_sym()):
                self.log("Sym found: %s" % self.get_data(i).tradecontract)
            raise Exception('''Cantidad de instrumentos invalida (%s). 
                Debe ser multiplo de 3 (futuros (corto, largo) y pase)''' % self.count_sym())
        self.strategies_running = int(self.count_sym() / 3)
        self.min_profit = min_profit
        self.max_profit = max_profit
        self.timer_contingency_sec = timer_contingency_sec
        self.stop_loss = stop_loss
        self.spread_contingency = spread_contingency
        self.order_timer_sec = order_timer_sec
        self.max_order_size = max_order_size
        self.usd_ars = usd_ars


    def run_strategies(self):
        for i in range(0, self.strategies_running):
            strategy_item = StrategyInfoItem()
            syms = [getattr(self, 'data%s' % (3 * i)), getattr(self, 'data%s' % (3 * i + 1)),
                    getattr(self, 'data%s' % (3 * i + 2))]
            types = [contract_type(syms[0]), contract_type(syms[1]), contract_type(syms[2])]
            # Verificacion de integridad (tipos de instrumentos)
            if (types[0]['contract_type'] != types[1]['contract_type']) or \
                    (types[1]['contract_type'] != types[2]['contract_type']):
                raise Exception("El grupo de instrumentos %s %s %s es incompatible" %
                                (syms[0].tradecontract, syms[1].tradecontract, syms[2].tradecontract))
            # Organizacion de instrumentos (en futuro largo, corto y pase)
            futures_idx = []
            for j in range(0, 3):
                sym = syms[j]
                typ = types[j]
                self.log("Inicializando " + sym.tradecontract)
                if sym.tradecontract.endswith('A'):
                    raise Exception('Instrumento invalido %s. Estrategia no apta para mayorista' % sym.tradecontract)
                if sym.tradecontract.startswith(typ['prefix'] + 'P'):
                    strategy_item.rollover_data_idx = 3 * i + j
                    self.log("Identificado Pase: %s Indice %s" % (sym.tradecontract, 3 * i + j))
                else:
                    self.log("Identificado Futuro: %s Indice %s" % (sym.tradecontract, 3 * i + j))
                    futures_idx.append(3 * i + j)

            if get_contract_expiry(self.get_data(futures_idx[0]).tradecontract) > \
                    get_contract_expiry(self.get_data(futures_idx[1]).tradecontract):
                self.log("Identificado - Futuro cercano: %s Futuro lejano: %s" %
                         (self.get_data(futures_idx[1]).tradecontract, self.get_data(futures_idx[0]).tradecontract))
                strategy_item.front_data_idx = futures_idx[1]
                strategy_item.further_data_idx = futures_idx[0]
            else:
                self.log("Identificado - Futuro cercano: %s Futuro lejano: %s" %
                         (self.get_data(futures_idx[0]).tradecontract, self.get_data(futures_idx[1]).tradecontract))
                strategy_item.front_data_idx = futures_idx[0]
                strategy_item.further_data_idx = futures_idx[1]
            # Inicializacion
            min_p = round_step(float(self.min_profit) / types[0]['contract_size'], types[0]['tick'])
            max_p = round_step(float(self.max_profit) / types[0]['contract_size'], types[0]['tick'])
            if strategy_item.front_data_idx == -1 or strategy_item.further_data_idx == -1 or \
                    strategy_item.rollover_data_idx == -1:
                raise Exception('No se identificaron los instrumentos correctamente')

            strategy_item.strategy = ArbitrajeFuturo(self.logger, self.cancel_order, self.buy_order, self.sell_order,
                                                     self.alert, self.get_order_info, float(self.timer_contingency_sec),
                                                     float(self.stop_loss), float(self.spread_contingency),
                                                     float(self.order_timer_sec), max_p, min_p, int(self.max_order_size),
                                                     types[0]['tick'], float(self.usd_ars), self.log)
            self.log('''
                Estrategia pase creada: Timer orden: %s, Timer contingencia: %s, Stop loss: %s,  
                Spread contingencia: %s, Max profit: %s, Min profit: %s, Tamaño max orden: %s, USD/ARS: %s, 
                Tick: %s''' % (self.order_timer_sec, self.timer_contingency_sec, self.stop_loss, self.spread_contingency, max_p, min_p,
                               self.max_order_size, self.usd_ars, types[0]['tick']))
            self.strategies.append(strategy_item)

    def next(self):
        for i in range(len(self.strategies)):
            strategy = self.strategies[i]
            self.logger.log_strategy(data={"Next - estrategia": i})
            if strategy.rollover_data_idx == -1 or strategy.front_data_idx == -1 or strategy.further_data_idx == -1:
                self.logger.log_error_event(
                    'La estrategia %s tiene mal seteado los indices Pase: %s Corto: %s Largo: %s' %
                    (i, strategy.rollover_data_idx, strategy.front_data_idx, strategy.further_data_idx))
                continue
            strategy.strategy.rollover_md = self.get_data(strategy.rollover_data_idx)
            strategy.strategy.short_md = self.get_data(strategy.front_data_idx)
            strategy.strategy.long_md = self.get_data(strategy.further_data_idx)
            strategy.strategy.next()

    def notify_order(self, order):
        self.logger.log_er_event(order)
        if order.m_orderId in self.cancelled_orders:
            if order.status in [Order.Canceled, Order.Cancelled, Order.Rejected, Order.Completed]:
                self.cancelled_orders.remove(order.m_orderId)
            else:
                if order.status in [Order.Partial, Order.Accepted]:
                    self.cancel(order)
        else:
            if order.m_orderId not in self.orders:
                self.logger.log_error_event('Orden %s no pertenece a ninguna estrategia' % order.m_orderId)
                return
            else:
                o = self.orders[order.m_orderId]
                o.order = order
                self.strategies[o.strategy_idx].strategy.notify_order(order.m_orderId)

    def get_order_info(self, order_id):
        if order_id not in self.orders:
            if order_id != '':
                self.logger.log_error_event('La orden %s no existe' % order_id)
            return None
        else:
            return self.orders[order_id].order

    def get_data(self, idx):
        return getattr(self, 'data%s' % idx)

    def get_data_idx(self, sym_name):
        for i in range(self.count_sym()):
            if self.get_data(i).tradecontract == sym_name:
                return i
        return -1

    def cancel_order(self, order_id):
        if order_id not in self.orders:
            if order_id != '':
                self.logger.log_error_event("La orden %s no existe" % order_id)
        else:
            self.cancel(self.orders[order_id].order)
            self.cancelled_orders.append(order_id)
            self.logger.log_cancel_order_response(order=self.orders[order_id].order)
        return ''

    def buy_order(self, data, **kwargs):
        idx = self.get_data_idx(data.tradecontract)
        for i in range(len(self.strategies)):
            s = self.strategies[i]
            if s.rollover_data_idx == idx or s.front_data_idx == idx or idx == s.further_data_idx:
                idx = i
                break
        if idx == -1:
            self.logger.log_error_event('Una estrategia invalida quiere agregar una orden de compra')
            return ''
        order = self.buy(data=data, **kwargs)
        self.logger.log_new_order_response(order)
        order_item = OrderItem()
        order_item.strategy_idx = idx
        order_item.order = order
        self.orders[order.m_orderId] = order_item
        return order.m_orderId

    def sell_order(self, data, **kwargs):
        idx = self.get_data_idx(data.tradecontract)
        for i in range(len(self.strategies)):
            s = self.strategies[i]
            if s.rollover_data_idx == idx or s.front_data_idx == idx or idx == s.further_data_idx:
                idx = i
                break
        if idx == -1:
            self.logger.log_error_event('Una estrategia invalida quiere agregar una orden de venta')
            return ''
        order = self.sell(data=data, **kwargs)
        self.logger.log_new_order_response(order)
        order_item = OrderItem()
        order_item.strategy_idx = idx
        order_item.order = order
        self.orders[order.m_orderId] = order_item
        return order.m_orderId

    def start(self):
        self.log('Start strategy')
        self.run_strategies()

    def stop(self):
        self.log('Stop strategy')
        self.strategies.clear()

    def count_sym(self):
        i = 0
        while hasattr(self, 'data%s' % i):
            i += 1
        return i

    def process_parameter_change(self, event):
        new_param = {}
        self.logger.log_strategy(data={"Master: Parameter change"})
        try:
            for params in event.parameters:
                for name, value in params.items():
                    if name in ['min_profit', 'max_profit', 'usd_ars', 'max_order_size', 'timer_contingency_sec',
                                'order_timer_sec', 'stop_loss', 'spread_contingency']:
                        new_param[name] = float(value)
            for i in range(len(self.strategies)):
                s: StrategyInfoItem = self.strategies[i]
                c_info = contract_type(self.get_data(s.rollover_data_idx))
                new_param_s = new_param.copy()
                if 'min_profit' in new_param_s:
                    new_param_s['min_profit'] = round_step(new_param_s['min_profit'] / c_info['contract_size'],
                                                         c_info['tick'])
                if 'max_profit' in new_param_s:
                    new_param_s['max_profit'] = round_step(new_param_s['max_profit'] / c_info['contract_size'],
                                                         c_info['tick'])
                s.strategy.process_parameter_change(new_param_s)
            self.log("Nuevos parametros: " + repr(new_param))
            super().process_parameter_change(event)
        except ValueError:
            self.log('''Parámetros inválidos. 
            No se cambió ninguna configuración.
            Revise los datos y reintente.
           Considerar que el separador decimal es el . (punto)''')