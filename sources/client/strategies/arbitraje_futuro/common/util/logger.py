class logger:

    def __init__(self):
        self.execution_id="x"

    def connect(self):
        print("Connecting mock logger")

    def info(self,data):
        print(data)

    def log_strategy(self,data):
        print(data);

    def log_er_event(self,order,additional=None):
        print("test log_ExecReport_event. Order={} Status={} additional={}".format("<order...>",order.status
                                                                           ,"<additional...>"))

    def log_md_event(self,data_list,entry_list,cols,additional):
        print ("test log_md_event. data_list={} entry_list={}  cols={} additional={}"
               .format("<data_list...>","<entry list...>","<cols...>","<additional...>"))

    def log_md_event(self,data_list,entry_list,additional):
        print ("test log_md_event. data_list={} entry_list={}   additional={}"
               .format("<data_list...>", "<entry list...>", "<additional...>"))

    def  log_new_order_response(self,order, additional=None):
        print("test log_new_order_response: order={} additional={}".format(order,additional))

    def log_error_event(self,desc,additional=None):
        print ("@log_error_event: desc={} additional={}".format(desc,additional))

    def log_cancel_order_response(self,order):
        print("test log_cacel_order_response: order={} ".format(order))