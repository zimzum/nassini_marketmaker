from sources.client.framework.business_entities.backtest_parameter import *
from sources.client.strategies.market_maker.business_entities.market_making_summary import *
from sources.markets.common.dto.market_data_dto import *
from sources.client.strategies.arbitraje_futuro.common.util.logger import *
from sources.client.framework.util.backtester_proxy import *
from sources.client.framework.enums.backtest_period import *
import statistics
import threading
import queue
import time
from scipy import stats

class arbitraje_futuro_backtester_proxy(backtester_proxy):
    _symbols=""
    @staticmethod
    def arbitraje_futuro_backtester_proxy_pre_initialization(symbols):
        arbitraje_futuro_backtester_proxy._symbols=symbols

    def __init__(self):
        self.logger_started=False

    #region Private Methods

    def prepare_logs(self,name):
        self.logger = logger()
        self.logger_started=True

    #endregion

    #region Init Methods
    def on_init_backtester(self,p_configuration):

        self.ProxyName = "arbitraje_futuro_backtester_proxy"
        # Debemos tener 3 activos
        symbolsArr = arbitraje_futuro_backtester_proxy._symbols.split(",")

        i = 0
        for symbol in symbolsArr:
            md = market_data_dto(p_tradecontract=symbol)
            setattr(self, "data" + str(i), md)
            i += 1

        #Once at the begining of the backtester
        #To initialize logs and other context variables (that are not daily variables)
        self.periodic_persist = p_configuration.periodic_persist
        self.prepare_logs("ArbitrajeFuturo")

    def backtest_prestart(self,start_time):#On New Day
        self.log("Prestarting Arbitraje Futuro Proxy at {}".format(start_time))

    def get_config_params(self, backtest):
        prms = []

        #retrieve all the config params from strategy for persistance

        return prms

    #endregion

    #region Logging Methods

    def logs(self,str):
        self.logger_started = False
        self.ProxyName="ArbFuturosProxy"
        self.log(str)

    def alert(self,str):
        self.log(str)

    #endregion

    #region Periodic Methods

    def persist_summary(self):
        #TODO persist summaries thread
        pass

    def on_periodic_change(self,period ,backtest,p_start,p_end):
        #Create summaries with statistical parameters
        #persist and clean them
        pass



    def on_hourly_change(self, backtest,p_start,p_end):
        if self. periodic_persist==backtest_period.Hourly.value:
            self.on_periodic_change(backtest_period.Hourly ,backtest,p_start,p_end)

    def on_minute_change(self, backtest,p_start,p_end):
        if self.periodic_persist==backtest_period.Minute.value:
            self.on_periodic_change(backtest_period.Minute, backtest,p_start,p_end)

    def on_daily_change(self, backtest,p_start,p_end):
        if self.periodic_persist==backtest_period.Daily.value:
            self.on_periodic_change(backtest_period.Daily, backtest,p_start,p_end)


    def get_cum_profit(self):
        return  0

    def get_net_buys(self):
        return  0

    def get_net_sells(self):
        return  0

    def get_net_long_contracts(self):
        return  0

    def get_net_short_contracts(self):
        return 0

    def get_net_long_contracts_for_symbol(self,symbol):
        return self.get_net_long_contracts() #if it was a one security strategy

    def get_net_short_contracts_for_symbol(self,symbol):
        return self.get_net_short_contracts() #if it was a one security strategy

    def get_summary(self):
        return ""

    def process_parameter_change(self, event):
        pass

    #endregion

