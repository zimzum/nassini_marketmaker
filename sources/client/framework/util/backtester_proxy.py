from logging.handlers import TimedRotatingFileHandler

import backtrader as bt
from sources.client.framework.business_entities.backtest_parameter import *
import logging
import os

class backtester_proxy:
    def __init__(self):
        try:
            self.data = self.datas[0]
        except Exception as e:
            print("Could not find self.datas @strategy")
        self.FakeMarket =None
        self.logger = logging.getLogger("nasini_backtester")
        self.logger_started=False


    def prepare_logs(self,name):
        self.logger = logging.getLogger(name)

        log_path = os.path.join("logs", name)

        main_formatter = logging.Formatter(
            fmt='%(asctime)s [%(module)s %(levelname)s] %(message)s',
            datefmt='%Y-%m-%d %H:%M:%S')

        console_handler = logging.StreamHandler()
        file_handler = TimedRotatingFileHandler(
            filename=log_path, when='d', backupCount=20)

        for handler in [console_handler, file_handler]:
            handler.setFormatter(main_formatter)
            self.logger.addHandler(handler)
        self.logger.setLevel(20)  # 20 = normal
        self.logger_started=True

    def log(self,str):
        if  not self.logger_started:
            self.prepare_logs(self.ProxyName)
        self.logger.info(str)

    def buy(self,data,size,price,exectype,tradeid):
        return self.FakeMarket.buy(data,size,price,exectype,tradeid)

    def buy(self,data,**kwargs):
        return self.FakeMarket.buy(data,kwargs["size"],kwargs["price"],kwargs["exectype"],
                                   kwargs["tradeid"] if "tradeid" in kwargs else None)


    def sell(self,data,size,price,exectype,tradeid):
        return self.FakeMarket.sell(data,size,price,exectype,tradeid)

    def sell(self,data,**kwargs):
        return self.FakeMarket.sell(data,kwargs["size"],kwargs["price"],kwargs["exectype"],
                                    kwargs["tradeid"] if "tradeid" in kwargs else None)

    def replace(self,order,size,price,tradeid=None):
        return self.FakeMarket.replace(order,size,price,tradeid)

    def cancel(self,order):
        return self.FakeMarket.cancel(order)