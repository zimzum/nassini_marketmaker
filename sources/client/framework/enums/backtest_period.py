from enum import Enum
class backtest_period(Enum):
    Minute = "MINUTE"
    Hourly = "HOURLY"
    Daily = "DAILY"
