import pyodbc


class backtest_manager():

    #region Constructors

    def __init__(self, connString):
        self.i = 0
        self.connection = pyodbc.connect(connString)
    #endregion

    #region Public Methods

    def get_backtest_id(self, datetime, name):
        backtest_id = None
        with self.connection.cursor() as cursor:
            params = (datetime, name)
            cursor.execute("{CALL GetBacktests (?,?)}", params)

            for row in cursor:
                backtest_id = row[0]

        return backtest_id

    def parsist_backtest(self,backtest):

        with self.connection.cursor() as cursor:
            params = (backtest.name,
                      backtest.datetime
                      )
            cursor.execute("{CALL PersistBacktest (?,?)}", params)
            self.connection.commit()

    #endregion
