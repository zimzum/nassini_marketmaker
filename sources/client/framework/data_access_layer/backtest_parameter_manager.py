import pyodbc


class backtest_parameter_manager():

    #region Constructors

    def __init__(self, connString):
        self.i = 0
        self.connection = pyodbc.connect(connString)
    #endregion

    #region Public Methods

    def parsist_backtest_paramter(self,backtest_paramter):

        with self.connection.cursor() as cursor:
            params = (backtest_paramter.backtest.id,
                      backtest_paramter.key,
                      backtest_paramter.string_value,
                      backtest_paramter.int_value,
                      backtest_paramter.float_value,
                      )
            cursor.execute("{CALL PersistBacktestParamter (?,?,?,?,?)}", params)
            self.connection.commit()

    #endregion
