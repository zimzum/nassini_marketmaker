from sources.client.strategies.dummy_router.dummy_router import *
from sources.markets.fake_market.fake_market import *
from sources.markets.common.cofiguration.configuration import *

if __name__ == '__main__':


    #market_data_manager.impl_backtest=True

    configuration = configuration("../../configs/backtester.ini")

    dummy_router = dummy_router(destURL=configuration.dummy_url)

    fake_market = fake_market(dummy_router,p_configuration=configuration)

    fake_market.start()

    input()
